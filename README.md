# The Adventures of BigSock

The Adventure of BigSock is an action top-down roguelike rpg, with the rpg part still pending.

### Other files
* [Game Design Doc](https://gitlab.stud.idi.ntnu.no/gardaf/mr-bigsock/-/blob/main/Documentation/gamedesign.md)
* [Submission Doc](https://gitlab.stud.idi.ntnu.no/gardaf/mr-bigsock/-/blob/main/Documentation/Submission.md)

## The Goal

Note: This goal was quite lofty when taking into account the time of development, and we never expected to achieve it all, and it would only serve as our guideline.

The goal of the game was to create a different top-down action roguelike. This would be achieved with item management inspired by FTL, as well as more advanced combat than similar games. Additionally adding a very simple but compelling story that can easily vary based on context and choice of character. 

With the item management the objective was to make choices matter as well as add some amount of improvisation to every run. This was to be achieved by limiting hoarding with a simpler inventory and making the objective of gathering items both somewhat random as well as giving the player choice. 

When it comes to the combat, our goal was to make a system where simply spamming and running would not be sufficient. The hope was to make a system where the player would have to actively use dodge as well as shields to survive. To make the act of causing damage more interesting the player would have multiple different attacks for different purposes and for enemies to have obvious, but hard to hit weak spots. 

Finally, the story would be simple in nature, but could vary based on character chosen. The original story would be that someone has stolen Mr.BigSocks cat and he would have to delve down into a dungeon to get it back. Then for the next character in the game it could be their favourite flute or ring as an example. 

## Running the game

In order to run the game, dowload "The Adventures of BigSock.zip" and unzip the contents into a folder, then run the Mr.Bigsock.exe file

* Note: to show key-bindings press "L" while in game.

## Gameplay

The game is an action top-down roguelike. The roguelike being the most important part. Essentially the players start with some basic equipment and stats, and throughout the run upgrades and improves to match the scaling of difficulty. In the end there is a final boss, and by beating the boss the game is won. The beauty of this concepts is that it opens for a lot of replay ability with each run being different based on the players choices as well as some amount of rng. 

**Core Gameplay**

The objective of the game is to fight your way through all rooms and levels to reach the final boss. To do this the player has to fight enemies, get xp and items, use this to upgrade themselves and prepare for the final battle. 

**Mechanics** 

* The game world is procedurally generated, while this is still in an early stage of development for us, it means in practice that no run should be too similar. With each new run comes different enemies and by beating them there is a chest with random items that the player must choose from. 
* By fighting enemies, the players get XP points that can be used to upgrade stats in the player menu. There is quite a lot of different ones, considering the length of the game, however, would the development continue, this would make more sense. 
* Items that are gained by completing rooms and opening the chest that spawns in the center of the room by pressing E and choosing an item, to use them they have to be equipped into one of the two bottom sets of slots. (There currently are no slot dependency)
* To deal damage the player can charge attack with normal LMB and spam attack with RMB. There is also a stronger slower attack with Z. 
* The player can dodge with Shift, at which point they move fast in the direction they dodged and gain invincibility frames. 
* By killing all enemies in a room, the player can move onto the next room by pressing E at the door.  
* There is a boss at the end of the floor, by killing him the player can move on to the next floor. There are only two floors and by killing the next boss the game is won. 

## Stage of Development

*As mentioned, the goal of the project was quite lofty, this is the stage of development we reached. * 

**Player Controls**
We have created a fully working movement system as well as attack system. The system works, but for a full release the game would need both reworks as well as tweaking to find a medium where it is both fun, intuitive and makes sense. 

**UI**
We have UI elements for the most important parts of the game to convey to the player. This includes:
* Bars for health, stamina, mana and xp.
* Icons for abilities to give feedback to player about charge and cooldown. 
* Player menu with inventory and stat upgrades. There are pages for settings, but they are not yet implemented. Lastly there is a page to "die" as there is no saving.
* Items work in the inventory, but there is no slot dependency; meaning they can be used in any slot. usable items are implemented, but not added. 
* Chests with choice of items to choose. 

**World generation**
We have a procedural generated world, with rooms that the player must move trough as well as enemy spawns. 

**Player Character**
While there is only one character in the current game, we have a fully-fledged modular character system. The current player character is based on this system as well as the enemies. 

**AI**
Enemies do attack the player, but this is as of now very simplistic. 

## Known Issues 

* Possible to get shot out of the rooms. 
* Chest sometimes spawn on the second level when they are not supposed to.  
* You can charge attacks while the game is paused


## Further Work

There is a lot of stuff to add, improve and change to get a great game 

**Art** 
The game currently only uses premade assets from the asset store as well as some very simple placeholders. Having a concise art style for the game with purpose made assets could help improve the game vastly. 

**Gameplay** 
There are much that can be done to both improve and add to the gameplay. Some of the are: Improve movement, add a story, stores, more advanced AI and more advanced world gen.
