# Game Design Document


A game design document is living document which describes the intent of the game design. 
It has two goals, first to document the decisions that have been made about the game and communicate those concepts to the entire team. 
Thus, it needs to be detailed enough for programmers to refer to when they need clarification about an aspect of the game. 
It must be able to be updated as the game is to be built. 
The need to have a game design document increases with the size of the team and length of the project. 

For a student project the intent is to capture as much as possible of your design. 
The game design will be larger than what you can achieve in a semester, but you must then decide what you need to do first. 
This document should be in version control so that you can see it changing and growing. 
Given we are using git you could also use @name to assign parts of the design to individual members of the team.


## Overview
BigSock , BigSock

### Game Concept
-Top down, rougelike, fantasy theme, rpg narrative
-Advanced combat with dodge and different attacks
-moves up tower or down a dungeon
-Upgrades when progressing

### Genre
Roguelike

### Target Audience
Roguelike for darsouls enjoyers

### Game Flow Summary
How does the player move through the game?
similar to binding of isaac, moves trough dungeon floors
set amount of floor 
prepare for final boss


### Look and Feel
What is the basic look and feel of the game?  What is the visual style?
-Pixelart
-grimy
-dungeon

## Gameplay and Mechanics
What does the player do?
-roguelike

### Gameplay
What is the core of the players interaction with the game?
-wast movement
-aims with mice
-uses attacks, dodge, blocks to fight enemies

#### Game progression
How does the player progress through the experience and how do they know they are making progress?
-The player get stronger
-The player moves trough the floors
-if you lose you probably did something wrong

#### Mission/challenge Structure
Is there a heirachy to the challenges in the game?
-small, big, bosses - type enemies
-gets stronger troug hthe floors

#### Puzzle Structure
Are there puzzles, ie challenges that have a correct answer?
-not for now

#### Objectives
What is the player trying to achieve?
-Beat the final boss
-"get all the character/classes"
-get all the achievments

### Mechanics
What are the rules to the game, both implicit and explicit?  
This is the model of the universe that the game works under.  
Think of it as a simulation of a world. How do all the pieces interact?
-

#### Physics
How does the physical universe work?

#### Movement
How the player interacts with the game?
-wast
-click and play?
-dodge

#### Objects
What are the objects in the game?
How does the player interact with them?
-cash
-mana chrystals -rare
-equipment - gives boons
-skillbooks for class mods

#### Actions
What are the other interactions the player has with the game world?


#### Combat
If there is combat or conflict, how is this specifically modeled?
-darsouls like, two main attacks + dodge + boons + shield? + allies

#### Economy
What is the economy of the game? How does it work?
-player gets cash for killing enemies or finding loot 
-cash can be used in stores
-gambling?

#### Screne Flow
A graphical description of how each screne is related to every other and a description of the purpose of each screen.
-Main menu -> gameplay screen 
                            |-> inventory 
                            |-> options
                            |-> stores
               
### Game Options
What are the options and how do they affect gameplay and mechanics?
want:
-audi and grapix
-key binding
-difficulty

optional:


### Replay and Saving
-Can save mid run, but lose the save on death
-save rooms?
-can savescum trough the file explorer but that is for scrubs

### Cheats and Easter Eggs
-cats

## The Story, Setting, and Character
-mr BigSock is on his way to save his cat from the evil sorcerer longNose
-To do this he has to traverse a dungeon

### Story and Narrative
If there is a story component includes back story, plot elements, game progression, and cut scenes. 
Cut scenes descriptions include the actors, the setting, and the storyboard or script.

### Game World
The setting of the game
medival fantasy world

#### General look and feel of the World
Aesthetics
Dark and grimey in the dungeon
Themed floors

#### Areas
including the general description and physical characteristics as well as how it relates to the rest of the world 
(what levels use it, how it connects to other areas).

### Characters
Each character should include the back story, personality, appearance, animations, abilities, relevance to the story and relationship to other characters.
-BigSock
-LongNose


## Levels

### Playing Levels
Each level should include a synopsis, the required introductory material (and how it is provided), the objectives, 
and the details of what happens in the level.  
Depending on the game, this may include the physical description of the map, the critical path that the player needs to take, 
and what encounters are important or incidental.

### Training level
How is onboarding managed?

## Interface

### Visual System
If you have a HUD, what is on it?  What menus are you displaying? What is the camera model?

### Control System
How does the game player control the game?   What are the specific commands?

### Audio, Music, Sound Effects

### Help System

## Artificial Intelligence

### Opponent and Enemy AI
The active opponent that plays against the player and therefore requires strategic decision making.

### Non-combat and Friendly Characters

### Support AI

### Player and Collision Detection, Path-finding.

## Technical

### Target Hardware

### Development Hardware and Software (including game engine)

### Network requirements

## Game Art

### Key assets 
How are they being developed.  Intended style.

This is an extension of parts of [cs.unc.edu](http://wwwx.cs.unc.edu/Courses/comp585-s11/585GameDesignDocumentTemplate.docx)
