# The Adventures of BigSock Submission

## Members

* Julius Fredrik Einum 
* Robin Ruud Kristensen 
* Robin Sandvik 
* Alexander Gatland 
* Gard Furre

## Gameplay Video

[Gameplay Video](https://youtu.be/IQzbrdb04_E)

## Work Distribution Matrix

| Category| Julius |Robin R|Robin S|Alexander|Gard|
|----|----|----|----|----|----|
| Physics   | - | 4 | 4 | - | 4 |
| Inventory UI | 2 | - | 4 | - | - | 
| Chest system       | - | - | - | 1 | - | 
| GUI       | 2 | 5 | 4 | - | 5 | 
| Audio     | - | - | 5 | - | 2 | 
| Procedural generation | - | 1 | - | - | - | 
| Sprites   | 4 | 4 | - | 3 | 4 | 
| Items     | - | - | 2 | 5 | - | 
| Enemies   | - | - | 4 | 5 | 2 | 
| Abilities | 5 | - | 2 | - | - | 
| Bosses    | - | - | - | 1 | - | 
| Gameplay mechanics | 5 | - | 3 | - | - | 
| Design    | 3 | 5 | 4 | 5 | 5 | 

Explenation of what the numbers mean.
* All - 1
* Most - 2
* Half - 3
* Some - 4
* Touched - 5

## Group discussion

## The Game

A more thorough walktrough of the game can be found [Here](https://gitlab.stud.idi.ntnu.no/gardaf/mr-bigsock/-/blob/main/README.md) 

## Game Engine

### Our choice of engine.

For this project, we chose to go with Unity for our game. We went over what engine to use at the start of our project and decided to go with unity for a few reasons.
1) Some members had familiarity with C#.
2) Unity had good documentation.
3) Unity looked to be the best for 2D games.

### Experiences with the Unity editor
The unity editor was very practical, tho a bit complex at the start.
In particular, we found it very handy to be able to make prefabs, build elements out of components in a tree-like structure, and adjust field values for instances and prefabs.
It also came with a lot of useful things pre-made, which combined with the component structure made things easier to make and adapt to how we needed it.

### Experiences with C#
C# is an incredibly flexible language with many different ways of doing things.

While the flexibility is very nice once you get the hang of it, it does make things a little harder to learn, tho most of these practical features are optional, the way different people mix them make getting help and looking up guides/tutorials more difficult since you sometimes spend more time understanding how he did things than the actual solution.

However, once we got a better grasp of the language, we found a lot of these features very useful. 
The main examples are:
- Default arguments, null coalescence and null-safe calls made our code a lot simpler and more expressive.
- Event handlers and callbacks were very useful for our code, given the nature of our game, it was essential for building a system that allows players to hold items that trigger their own code under certain conditions.
- Inheritance, interfaces and properties were very useful for making abstract systems for handling things like abilities and items, allowing us to make much more flexible, expandable and simple systems for these things.
Some of these things proved essential to making our code as stable and easy to add to as it was. Due to these abstractions and encapsulations, making changes was often only in one place, making our development faster and less error prone.

### Experiences with Unity overall
Working in Unity with C# was a big change from working with higher-level languages (Like Python/JS) or lower-level languages (Like C/C++), and having all our code wrapped within a framework like unity took some time to get used to.

One large challenge was that our code had to operate within the confines of Unity, instead of following a flow we implement, which made organizing our code a lot harder.

Once we got the hang of Unity, we found it very useful.

While the system we had to operate within was somewhat limiting, it provided us with a structure to start with, which helped us focus our designs and get a common focus earlier on. 

The systems also turned out to be relatively flexible and we were able to make it fit our code pretty well.

Some of the biggest parts we found useful about Unity were:
- The script structure, with Start and Update, were very reasonable and was easy to get the hang of. It also provided an easy way to separate code into reasonable pieces.
- Supporting features like RigidBody and the Time class made the code a lot easier to manage and implement.
- In addition to classes related to scripts, Unity also provided interfaces for many other things that came in handy, like handling click and drag events, which we used a lot in our item UI.
- The component system made our code more flexible as we could find underlying components and child elements and make use of their events and callbacks where applicable. One example of this is our use of colliders to create triggers for various actions, like chasing or attacking the player.

### Conclusion
All in all, we're very happy with our choice to use Unity for the project.
It took us a while to get the hang of it, and we made a lot of mistakes and changes, but we were able to make the code very flexible, reducing the time and work needed to make these changes.

We've learned a lot from these experiences, not just about Unity and C#, but more general things, like good/bad coding practices, game development and working on group projects.



## Process and Comunication during development / (Teamwork)

* One meeting each week
* Each member worked on the project throughout the week
* If needed more meetings/group work time would be held

Every meeting would be held on Thursday in the timeframe of 14:00 to 16:00 were different issues would be discussed to all the group members. There was never a problem with attendance, but a few weeks we did not have a meeting because we felt like there would not come anything productive out of having a meeting at that time in the development process.

Each meeting agenda would be to look at what had been done since the previous meeting and then discuss what needs to be done to the next meeting. Normally that would be a discussion on features that we thought would improve the experience of playing the game or behind the scenes features that would help in developing the game.

Issue board was what we first said would be used to say what needs to be done and set up tasks that needed to be solved in the coming time. By making issues that someone could say they would work on or get given the group would then now who works on what on what is being worked on. That was what we intended to use at the start, but that is not how we ended up solving the issue of who works on what and what tasks need to be done. Instead, a TODO list that we agreed on in each meeting was how we would decide who does what.

Throughout the week the group members would work on their given task and try to get as far as they could with developing it or finishing the feature that they worked on. If a member was finished early with a feature, they would either help another member or start on one of the other features that was discussed in the previous meeting. It also happened that some members would sit down together to work on the same feature. 

If a problem occurred in the week or something needed to be discussed all group members or the individual that was needed to fix the problem would be contacted. Then normally the one that got a problem would get help to fix it and could finish the feature and start working on other features.


## Use of Version Controll
Version control is an essential part of game development, as it allows us to track changes to our code and collaborate more easily on a project. Our main way of version control during the development process was git. Using Git, we could create branches of our code, which are separate copies that can be worked on independently from the main repository. This allowed us to experiment with new features or bug fixes without affecting the main repository, and to easily merge our changes back when they were ready.

During the development, each member had their own branches which they worked on. Merging usually happened during team meeting where if bugs occurred, if the person stumbling upon the bugs needed help, they could ask for help, and we would handle it together. This made sure we most often had a working game in the main branch which everyone could pull from and continue developing. 

Overall, our use of Git and branching allowed us to efficiently manage the versions of our game. Enabling us to iterate on our game design and features quickly and effectively.

