# Individual Report Robin Sandvik


# Score weighting
|Description | my weight |
|----|----|
|Gameplay video | 10 |
|Code video | 10 |
|Good Code  | 30 |
|Bad Code | 30 |
|Development process | 10 |
|Reflection | 10 |

Ideally I'd be most weighted on the game itself, not very good at documentation/reports


# Code video

[Code video](individual-code-video-rs.mkv)

# Good Code

## Services for runtime access to resources

One of the things I think was done pretty well was the Service portion.
This portion is responsible for a handful of classes that provide runtime access to resources.

The code used to load these resources is partially based on code found online, the sources have been credited in the comments of the code. The design of the services themselves are my own.

![Overview of service classes](Images/rs_services_folder.JPG?raw=true)
<br/>
Here you see the 5 classes that were made, they provide access to their respective resource type.

![Overview of AbilityService](Images/rs_ability_service_overview.JPG?raw=true)
<br/>
Each class provides simple access to their respective resources so other parts can load them dynamically by unique identifiers.

![The loading method of AbilityService](Images/rs_ability_service_load.JPG?raw=true)
<br/>
The above code shows how [AbilityService.cs](../MrBigsock/Assets/Code/Services/AbilityService.cs) is loaded in. The same technique applies to [ItemService.cs](../MrBigsock/Assets/Code/Services/ItemService.cs).

When first used, the singleton will be created and the `_loadItems()` method will be called.

For these 2 services, since their resources are purely code, I employ some reflection to load all relevant classes (Abstract classes that inherit from the base class).
I then create instances of each and create a dictionary where they're mapped by their id.
When our code requests a resource by its id, I return a copy of the instance in that index.

LINQ was also used in the code to make it simpler and more readable.

Using these services is rather simple, an example would be: `AbilityService.SINGLETON.Get(104);`

![The loading method of AudioService](Images/rs_audio_service_load.JPG?raw=true)
<br/>
For [AudioService.cs](../MrBigsock/Assets/Code/Services/AudioService.cs), 
[SpriteService.cs](../MrBigsock/Assets/Code/Services/SpriteService.cs) and 
[PrefabService.cs](../MrBigsock/Assets/Code/Services/PrefabService.cs), the process is slightly different.
These resources have to be loaded directly from file using `Resources.Load()`.

Above is the `_loadItems()` method for `AudioService`.

Here I:
1) Use the `Directory` class to find all files within the `Assets/Resources/sound` folder. 
2) Filter out `.meta` files.
3) Extract the name (Remove the root path, file extension, and clean up the formatting a bit)
4) Load the resource from the file.
5) Add it to the dictionary.

For these services, instead of an arbitrary id we gave them, they are indexed by their relative path.

Examples of calls to these are:
- `SpriteService.SINGLETON.Get("item/coffee");`
- `AudioService.SINGLETON.Get(AUDIO_PATH);`


## The item system
- The different types, hierarchy

Items are a core part of our game. As a rouge-like, the items you get through the run can usually define whether you kill the boss without any effort, have to use skill and strategy to win, or stand virtually no chance to win.

Seeing how central items would be, I decided to make an abstract model early on.

Items are divided into 3 types:
- Active => Items that have an effect that must be actively triggered using one of the ability buttons.
- Passive => Items that provide a passive buff to the player's stats.
- Conditional => Items that have an effect that should trigger automatically when a certain event occurs.

The class hierarchy for items goes like this:
- ItemBase
	- ActiveItemBase 
	- PassiveItemBase
	- ConditionalItemBase

Conditional types currently implemented:
- OnDeath
- OnFire
- OnHeal
- OnHit
- OnKill
- OnTakeDamage

![IItem.cs](Images/rs_item_interface.JPG?raw=true)
<br/>
Here you see the interface shared by all items, [IItem.cs](../MrBigsock/Assets/Code/Item/Base/IItem.cs). With this design, the underlying classes can define their own ways of handeling things.

![ItemPlasticStraw.cs](Images/rs_item_plastic_straw.JPG?raw=true)
<br/>
Here you see [ItemPlasticStraw.cs](../MrBigsock/Assets/Code/Item/Items/ItemPlasticStraw.cs), an implementation of a conditional item.
This item triggers when the holder kills an enemy, granting a 100% chance to heal 1.5hp.

![ItemCoffee.cs](Images/rs_item_coffee.JPG?raw=true)
<br/>
Here you see [ItemCoffee.cs](../MrBigsock/Assets/Code/Item/Items/ItemCoffee.cs), an implementation of a passive item.
All a passive item needs is a `CharacterStats` object with the modifiers it should provide the holder as % of the holder's stats.

![IActiveItem.cs](Images/rs_active_item_interface.JPG?raw=true)
<br/>
Unfortunately, there are no implementations of active items as of yet.

The [IActiveItem.cs](../MrBigsock/Assets/Code/Item/Base/IActiveItem.cs) only requires an `IAbility` object.
All an active item would need is an ability.

### The inventory system

Players have an [Inventory.cs](../MrBigsock/Assets/Code/Item/Inventory.cs) to manage their items.

The inventory has 4 parts:
- Backpack => Holds items that aren't currently in use.
- Accessories => Holds items that are applied to the player's weapon.
- Equipment => Holds items that the player has on them.
- Tools => Holds active items the player has equipped.

Items in the Backpack do not provide their effect to the player.
Accessories and Equipment can be either passive or conditional, no distingshion between the two has been implemented yet.

![Move item method](Images/rs_inventory_move.JPG?raw=true)
<br/>
When items are moved between inventory sections, their effects are added/removed automatically by the `Inventory` object.

## The ability system

Similar to the item system, the ability system has also been abstracted.

The base class for all abilities is [BaseAbility](../MrBigsock/Assets/Code/Core/Abilities/Base/BaseAbility.cs), which all common aspects, like cooldown, charging, and mana/stamina/hp cost.

The ability implementation sets the relevant properties and defines its own `Activate()` method, which handles its effect.

![Dodge ability](Images/rs_ability_dodge.JPG?raw=true)
<br/>
Here you see [AbilityDodge](../MrBigsock/Assets/Code/Core/Abilities/AbilityDodge.cs), which gives the user a sudden boost forward and a very short invincibility.

## The skills system

The skills system is very simple. The player has a set of skills they can invest their points in, each skill affects a set of stats by a given amount pr. skill point.

![Skills class](Images/rs_skills.JPG?raw=true)
<br/>
The [Skills](../MrBigsock/Assets/Code/Core/Skills/Skills.cs) class is used to hold a number in each skill type. These can be used to hold how many points are invested, or how many can be invested at most.

The maximum points invested in each skill, and effects on a stat pr. point invested can be defined in the [PlayerController](../MrBigsock/Assets/Code/PlayerController.cs), which is done so that if we ever created a class system, like we originally wanted, we could change these for each class.

## The stats system

To tie the whole thing together, I designed a stats system for holding stats of both attacks and characters.

<br/>

The [CharacterStats](../MrBigsock/Assets/Code/Core/CharacterStats.cs) class is used to keep track of stats related to a given character.
These stats include:
- Max hp, mana and stamina
- Mana and stamina regeneration
- Movement speed
- Various attack related stats (The same ones found in `AttackStats`)

For the things that modify the stats of a character (Skills and passive items), the calculation goes like this: \
`Character * Skills * TotalPassives` \
For example, if a character has 2.0 damage, they get 20% more damage from skills, and they have 2 items that add 15% each, the calculation for their damage would be: \
`2.0 * 1.2 * 1.3 = 3.12`

<br/>

The [AttackStats](../MrBigsock/Assets/Code/Core/AttackStats.cs) class is used to keep track of the stats of an attack.
An attack ability will have one of these containing its base stats.
These stats include:
- Damage, Knockback, AttackSpeed
- Range, ProjectileSpeed
- CritChance, CritDamageModifier
- DamageVariance

When a character activates one of these attack abilities, their `CharacterStats` modify the base stats of the attack by multiplication. \
This means that if, for example, the attack has a base damage of 2.0, and the character has a damage stat of 1.2, the resulting damage of the attack will be 2.4.

## Generalization of entities
The controllers for different characters in the game were generalized early on to simplify the code.
Class hierarchy for characters:
- [Entity](../MrBigsock/Assets/Code/Entity.cs)
	- [Character](../MrBigsock/Assets/Code/Character.cs)
		- [PlayerController](../MrBigsock/Assets/Code/PlayerController.cs)
		- [EnemyController](../MrBigsock/Assets/Code/orc/EnemyController.cs)
			- [SlimeController](../MrBigsock/Assets/Code/slime/SlimeController.cs)
			- [SkeletonBossController](../MrBigsock/Assets/Code/Bosses/SkeletonBossController.cs)
			- [BringerOfDeathController](../MrBigsock/Assets/Code/Bosses/BringerOfDeathController.cs)

This helped us avoid repetition and redundant code.

## Event based handling

![Character events](Images/rs_character_events.JPG?raw=true)
<br/>
In [Character](../MrBigsock/Assets/Code/Character.cs), we implemented a bunch of events for general things that we might want to run code on.

![AddItemListener](Images/rs_item_listener.JPG?raw=true)
<br/>
This was essential for implementing conditional items, allowing me to hook them onto the relevant events when they're in use. This can be seen in `Character.AddItemListener()`.

# Bad Code

## Services 

While I think the service system I created was pretty good, it's far from perfect. The 3 biggest problems are as follows:
1) For loading resources from files, I used the `Directory` class to find all the files. When the game is built, all the resource files are baked into some special unity files, this causes an issue where `Directory` can't find them because they're not there. \
To address this, we have to manually copy the Resources folder into the built game, which is less than ideal, and causes it to get bigger.
2) For `PrefabService` I have not implemented any instance pooling, when requested, a new instance is made, and promptly discarded when no longer needed. This causes more overhead. \
Ideally, I would build instance pooling into the service so that outside code wouldn't have to worry about it.
3) When first called, the services load in all relevant resources at the same time, this comes with 2 downsides. Firstly, it can cause a temporary freeze of the game when first used. Secondly, it loads in all our files into memory reguardless of whether or not they'll ever be used. \
For this, it would be good to implement a more cache like approach, where things are loaded in the first time they're requested.


![The loading method of AudioService](Images/rs_audio_service_load.JPG?raw=true)

## Charge system

The charge system allows players to hold the keys down to charge a more powerful version of certain attacks, while some basics are there, it is missing some important parts.

The first issue is that there is no way to alter the cost or cooldown of an ability based on the charging, meaning a fully charged attack costs the same as one with minimum charge.

![Applying charge modifiers](Images/rs_charge_modifiers.JPG?raw=true)
<br/>
Secondly, as can be seen in this code from [BasicProjectile2](../MrBigsock/Assets/Code/Core/Abilities/BasicProjectile2.cs) charging modifiers are applied in a very rough manner. 

While this does the job, it's not a pretty or well implemented solution.
If we wanted to add another attack with charging, we'd need to make similar code there, creating repetition and redundancy.

## Other things that haven't been implemented enough.

AttackSpeed has not been properly implemented yet, and doesn't properly affect ability cooldown.

Active items do not currently affect anything. Most of the groundwork is there, the base class exists and the inventory handles them correctly for the most part.
However, they do not alter the player's abilities yet.



# Reflection

The project was really fun to work on, and I learned a bunch of different things.
Overall I learned a lot about Unity, C# and working in groups and game development in general.

Some of the bigger lessons I learned were:
1) Component based design. Never heard of it before, but it came in handy here to break things into scripts and components, it made the code quite a bit more flexible and general.
2) Branching is important. Throughout the project, we made separate branches to work from, this helped us avoid stepping over eachother for the most part, saving us a lot of time and errors.
3) Solid foundation. Early on in the project, we made plans and drafts for parts of the game that we didn't get to for quite a while, some we never even got to implement. Because of this, we were able to get a better mutual understanding of the end goal and we were able to make our code better geared towards things we added in later on, instead of having to constantly rewrite large parts to make room for new parts.
4) Browsing documentation. Finding what you're looking for in documentation can be hard, especially if you're not too familiar with the subject in the first place. Over the cource of the project, I managed to get a lot better at browsing the Unity and C# documentations.

All in all, I'm happy we chose to use Unity, it's a very adaptable and well designed engine. It was also very nice to get back to using C# again, it's a very nice to use language that has good looking solutions for most problems.


## Things we could have done better

The way we've organized our source code files is somewhat messy. \
For example, Abilities is inside Core, but Items is in the root code folder. \
Character, Entity and PlayerController are in the root folder, but EnemyController is in orc.

Generally, a decent bit of our code isn't that well optimized. 
Ignoring small calculations or extra variables, the some more significant parts are:
- No instance pooling.
- Services load everything at once.
- Too much use of methods like Update and OnColliderStay2D_Action for things we could handle in less frequently called methods.

## Conclusion

While there is room for improvements, I think we did a pretty good job given our experience level and resources.
We didn't have time to develop all the systems we wanted to, and a lot of things aren't used fully, but we got pretty far and the game feels close to being worth buying for a less than 20kr.

Additionally, what we have is pretty solid and is designed to be relatively easy to expand on. Working on adding the missing features and adding more content, like items and enemies, would be relatively simple.


























