using Bigsock;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace BigSock
{
    [CustomEditor(typeof(AbstractMapGen), true)]
    public class RandomMapGeneratorEditor : Editor
    {
        AbstractMapGen generator;

        private void Awake()
        {
            generator = (AbstractMapGen) target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if(GUILayout.Button("Create Map"))
            {
                generator.Generate();
            }

            if(GUILayout.Button("Clear Map"))
            {
                generator.ClearMap();
            }
        }
    }
}