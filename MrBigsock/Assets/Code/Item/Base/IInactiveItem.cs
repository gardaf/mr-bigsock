using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {

	/*
		An interface an item that isn't an active item.
	*/
	public interface IInactiveItem : IItem {

	}
}