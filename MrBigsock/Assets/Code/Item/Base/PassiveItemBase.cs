using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {

	/*
		A class that represents an item that provides passive stat boosts.
	*/
	public abstract class PassiveItemBase : ItemBase, IPassiveItem {
		/*
			The modifier of the item.
		*/
		public ICharacterStats Modifier { get; set; }



		/*
			The type of the item.
		*/
		public override string ItemType => "Passive";

		/*
			Returns a string used to inform the user about this item in greater detail.
		*/
		public override string GetToolTip() {
			var stats = Modifier.GetToolTip();
			var sb = new StringBuilder().AppendLine(base.GetToolTip());

			if(stats.Length > 0) sb.AppendLine($"\n{stats}");

			return sb.ToString();
		}

	}
}