using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {

	/*
		The type of trigger that activates the item's effect.
	*/
	public enum TriggerType {
		None,

		// Target
		Fire,
		Hit,
		Kill,

		// Actor
		TakeDamage,
		Heal,
		Death,

		PickedUpItem,
		GainedXP,
		LevelUp,

		// Stage
		EnterNewRoom,
		ClearedRoom,
		EnterNewStage,
	}
}