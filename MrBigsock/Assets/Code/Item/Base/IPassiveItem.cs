using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {

	/*
		An interface for items that are passive.
	*/
	public interface IPassiveItem : IInactiveItem {
		/*
			The modifier of the item.
		*/
		ICharacterStats Modifier { get; set; }

	}
}