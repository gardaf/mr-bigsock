using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {

	/*
		An interface for items that trigger on a particular condition.
	*/
	public interface IConditionalItem : IInactiveItem {

		/*
			The type of trigger this item uses.
		*/
		TriggerType Trigger { get; }
		
	}
}