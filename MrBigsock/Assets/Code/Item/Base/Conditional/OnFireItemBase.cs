using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {

	/*
		Base class for all items that trigger when the user attacks.
	*/
	public abstract class OnFireItemBase : ConditionalItemBase {
		/*
			The type of trigger this item uses.
		*/
		public override TriggerType Trigger => TriggerType.Fire;

		/*
			The handler to activate when the condition is triggered.
		*/
		public abstract void Handler(Character source, AttackStats attack); 

	}
} 