using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {

	/*
		Base class for all items that trigger when the user hits an enemy.
	*/
	public abstract class OnHitItemBase : ConditionalItemBase {
		/*
			The type of trigger this item uses.
		*/
		public override TriggerType Trigger => TriggerType.Hit;

		/*
			The handler to activate when the condition is triggered.
		*/
		public abstract void Handler(Character source, Character target, AttackStats attack); 

	}
} 