using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {

	/*
		Base class for all items that trigger when the user kills an enemy.
	*/
	public abstract class OnKillItemBase : ConditionalItemBase {
		/*
			The type of trigger this item uses.
		*/
		public override TriggerType Trigger => TriggerType.Kill;

		/*
			The handler to activate when the condition is triggered.
		*/
		public abstract void Handler(Character source, Character target, AttackStats attack); 

	}
} 