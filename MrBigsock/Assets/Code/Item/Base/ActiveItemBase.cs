using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {

	/*
		A class that represents an item that provides passive stat boosts.
	*/
	public abstract class ActiveItemBase : ItemBase, IActiveItem {
		
		/*
			The ability the item activates.
		*/
		public abstract IAbility Ability { get; }


		/*
			The type of the item.
		*/
		public override string ItemType => "Active";

		/*
			Returns a string used to inform the user about this item in greater detail.
		*/
		public override string GetToolTip() {
			return new StringBuilder()
				.AppendLine(base.GetToolTip())
				.AppendLine()
				.AppendLine(Ability.GetToolTip())
				.ToString();
		}

	}
}