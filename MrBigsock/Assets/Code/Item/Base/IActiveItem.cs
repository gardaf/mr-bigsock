using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {

	/*
		An interface for items that needs to be activated manually.
	*/
	public interface IActiveItem : IItem {
		/*
			The ability the item activates.
		*/
		IAbility Ability { get; }

	}
}