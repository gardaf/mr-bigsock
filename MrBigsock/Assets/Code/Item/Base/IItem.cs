using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {

	/*
		An interface representing a generic item.
	*/
	public interface IItem {
		/*
			The name of the item.
		*/
		string Name { get; }

		/*
			The description of the item.
		*/
		string Description { get; }

		/*
			The id of the item.
		*/
		ulong Id { get; }

		/*
			The icon of the item.
		*/
		Sprite Icon { get; }



		/*
			Returns a string used to inform the user about this item in greater detail.
		*/
		string GetToolTip();


	}
}