using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {

	/*
		A class that represents an item that an effect when a condition is meet.
	*/
	public abstract class ConditionalItemBase : ItemBase, IConditionalItem {

		public static readonly System.Random RND = new System.Random();

		/*
			The type of trigger this item uses.
		*/
		public abstract TriggerType Trigger { get; }


		/*
			The type of the item.
		*/
		public override string ItemType => "Conditional";

		/*
			Returns a string used to inform the user about this item in greater detail.
		*/
		public override string GetToolTip() {
			return $"{base.GetToolTip()}\n\nTrigger: {Trigger}";
		}

	}
} 