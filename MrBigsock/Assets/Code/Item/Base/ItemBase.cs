using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

using BigSock.Service;


namespace BigSock.Item {

	/*
		A class that represents the root of all items.
	*/
	public abstract class ItemBase : IItem {
		/*
			The name of the item.
		*/
		public abstract string Name { get; }

		/*
			The description of the item.
		*/
		public abstract string Description { get; }

		/*
			The id of the item.
		*/
		public abstract ulong Id { get; }

		/*
			The icon of the item.
		*/
		public Sprite Icon => SpriteService.SINGLETON.Get(IconName);
		
		/*
			The name of the icon this item uses.
				Override this to change what icon the item uses.
		*/
		public virtual string IconName { get; } = "item/tilesetnice";


		/*
			The type of the item.
		*/
		public abstract string ItemType { get; }



		public ItemBase() {
			//Icon = SpriteService.SINGLETON.Get(IconName);
		}



		/*
			Returns a string used to inform the user about this item in greater detail.
		*/
		public virtual string GetToolTip() {
			return $"{Name} ({ItemType})\n\n{Description}";
		}

	}
}