using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {
	
	/*
		A passive item that increases user's running speed by 50%.
	*/
	public class ItemRunningShoes : PassiveItemBase {	
		public override ulong Id => 101;
		public override string Name => "Running Shoes";
		public override string Description => "Increases movement speed by 50%";
		public override string IconName => "item/runningshoes";


		public ItemRunningShoes() {
			Modifier = new CharacterStats{
				MoveSpeed = 0.5f,
			};
		}

	}
}