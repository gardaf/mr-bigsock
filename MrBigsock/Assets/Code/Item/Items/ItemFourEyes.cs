using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {
	
	/*
		An item that adds a 30% chance to deal double damage on hit.
	*/
	public class ItemFourEyes : OnHitItemBase {	
		public override ulong Id => 201;
		public override string Name => "Four Eyes";
		public override string Description => "30% chance to deal double dammage. Has a 2 second cooldown.";
		public override string IconName => "item/foureyes";

		public static readonly double CHANCE = 0.3;
		public static readonly TimeSpan COOLDOWN = new TimeSpan(0, 0, 0, 2, 0);

		/*
			Stores the next time the character can recieve damage.
		*/
		public DateTime NextTimeCanTrigger { get; private set; } = DateTime.Now;
		
		public ItemFourEyes() { }

		public override void Handler(Character source, Character target, AttackStats attack) {
			// Check if the cooldown has happened yet.
			if(NextTimeCanTrigger > DateTime.Now) return;

			// Start new trigger time.
			NextTimeCanTrigger = DateTime.Now + COOLDOWN;

			// Check if it triggers.
			var roll = RND.NextDouble();
			if(roll <= CHANCE) {
				MonoBehaviour.print($"[ItemFourEyes.Handler()] Hit. ({roll:P1} <= {CHANCE:P1})");
				attack.Damage *= 2;
			} else {
				MonoBehaviour.print($"[ItemFourEyes.Handler()] Miss. ({roll:P1} > {CHANCE:P1})");
			}
		}

	}
}