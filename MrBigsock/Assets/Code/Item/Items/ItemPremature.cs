using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {
	
	/*
		A passive item that increases user's running speed by 50%.
	*/
	public class ItemPremature : PassiveItemBase {	
		public override ulong Id => 104;
		public override string Name => "Premature";
		public override string Description => "Increases projectile speed by 50%";
		public override string IconName => "item/premature";

		public ItemPremature() {
			Modifier = new CharacterStats{
				ProjectileSpeed = 0.5f,
			};
		}

	}
}