using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {
	
	/*
		An item that heals the user on kill.
	*/
	public class ItemPlasticStraw : OnKillItemBase {	
		public override ulong Id => 202;
		public override string Name => "Plastic Straw";
		public override string Description => "Heal the user on kill.";
		public override string IconName => "item/plasticstraw";

		public static readonly double CHANCE = 1.0;

		public override void Handler(Character source, Character target, AttackStats attack) {
			// Check if it triggers.
			var roll = RND.NextDouble();
			if(roll <= CHANCE) {
				MonoBehaviour.print($"[ItemPlasticStraw.Handler()] Hit. ({roll:P1} <= {CHANCE:P1})");
				source.TryHeal(1.5f);
			} else {
				MonoBehaviour.print($"[ItemPlasticStraw.Handler()] Miss. ({roll:P1} > {CHANCE:P1})");
			}
		}

	}
}