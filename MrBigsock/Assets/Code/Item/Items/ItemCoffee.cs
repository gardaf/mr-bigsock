using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {
	
	/*
		A passive item that increases user's max hp by 25%.
	*/
	public class ItemCoffee : PassiveItemBase {	
		public override ulong Id => 103;
		public override string Name => "Caffeinated Coffee";
		public override string Description => "Increases attack speed by 25%";
		public override string IconName => "item/coffee";

		public ItemCoffee() {
			Modifier = new CharacterStats{
				AttackSpeed = 0.25f,
			};
		}

	}
}