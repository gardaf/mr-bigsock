using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {
	
	/*
		A passive item that increases user's max hp by 20%.
	*/
	public class ItemLunch : PassiveItemBase {	
		public override ulong Id => 102;
		public override string Name => "Lunch";
		public override string Description => "Increases hp by 20%";
		public override string IconName => "item/lunch";

		public ItemLunch() {
			Modifier = new CharacterStats{
				MaxHP = 0.2f,
			};
		}

	}
}