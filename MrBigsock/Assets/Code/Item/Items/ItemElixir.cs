using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock.Item {
	
	/*
		A passive item that increases user's running speed by 50%.
	*/
	public class ItemElixir : PassiveItemBase {	
		public override ulong Id => 105;
		public override string Name => "Elixir of Speed";
		public override string Description => "10% increase to all speed and stamina related stats, but at a cost.";
		public override string IconName => "item/elixirofspeed";

		public ItemElixir() {
			Modifier = new CharacterStats{
				MaxStamina      =  0.1f,
				RegenStamina    =  0.1f,
				ProjectileSpeed =  0.1f,
				MoveSpeed       =  0.1f,
				AttackSpeed     =  0.1f,

				Damage          = -0.1f,
				Range           = -0.1f,
			};
		}

	}
}