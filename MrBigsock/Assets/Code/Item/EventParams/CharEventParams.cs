using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock {

	/*
		Class that represents the parameters of a generic character event.
	*/
	public class CharEventParams : ICharEventParams {
		/*
			The character that own's the event.
		*/
		public Character Source { get; set; }

		/*
			The character that triggered the event. (If any)
			ex.: OnKill -> the killed enemy. OnDamage -> Enemy that dealt the damage.
		*/
		public Character Target { get; set; }


	}
}