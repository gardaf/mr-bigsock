using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock {

	/*
		Interface that represents the parameters of a generic character event.
	*/
	public interface ICharEventParams {
		/*
			The character that own's the event.
		*/
		Character Source { get; }

		/*
			The character that triggered the event. (If any)
			ex.: OnKill -> the killed enemy. OnDamage -> Enemy that dealt the damage.
		*/
		Character Target { get; }


	}
}