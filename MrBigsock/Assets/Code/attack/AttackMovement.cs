using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.GraphicsBuffer;

using BigSock.Service;


namespace BigSock {

	public class AttackMovement : MonoBehaviour {
		public float speed = 10.0f;
		bool moved = false;
		//Vector3 direction;
		Vector2 startingPos;

		/*
			Damage of the character.
		*/
		//public float Damage => baseDamage;
		//public float baseDamage = 1;

		//public float KnockbackForce => knockbackForce;
		//public float knockbackForce = 1;

		/*
			The character that activated the attack.
		*/
		//public Character Actor { get; set; }

		/*
			The attack stats of the attack.
		*/
		public AttackStats Stats { get; set; }

		/*
			The direction of the attack.
		*/
		public Vector2 Direction { get; set; }


		// Start is called before the first frame update 
		void Start() {
		}

		// Update is called once per frame
		void Update() {
			if (!moved) {
				startingPos = transform.position;
				moved = true;
			}

			//transform.Translate(new Vector3(1 * horizontalInput, 1 * verticalInput, 0) * speed * Time.deltaTime);
			transform.Translate(Direction * Stats.ProjectileSpeed * Time.deltaTime);

			var traveled = Vector2.Distance(transform.position, startingPos);

			// If we traveled passed our range: destroy this obj.
			if (traveled > Stats.Range) {
				PrefabService.SINGLETON.Destroy(gameObject);
			}

		}


		void OnCollisionEnter2D(Collision2D collision) {
			//print($"[AttackMovement.OnCollisionEnter2D()] {collision.transform.position}");
			var target = collision.gameObject.GetComponent<Character>();

			if (target != null) {

				//var attack = new AttackStats{
				//	Damage = Damage,
				//	Knockback = KnockbackForce,
				//	//Range = 0,
				//	//AttackSpeed = AttackSpeed,
				//	Source = transform.position,
				//	Actor = Actor,
				//};
				Stats.Source = transform.position;

				// If we have an actor: Apply their stat mods & trigger their OnHit.
				//if(Actor != null) {
				//	attack.Damage *= Actor.Damage;
				//	attack.Knockback *= Actor.KnockbackForce;
				//	//Actor.TargetHit(target, attack);
				//}

				if (target.TakeDamage(Stats)) {

				}
			}
		}



	}
}