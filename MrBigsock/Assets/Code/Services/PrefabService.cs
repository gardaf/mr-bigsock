using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEditor;

using BigSock.Item;


namespace BigSock.Service {
	
	/*
		Service for handling prefabs.
	*/
	public partial class PrefabService {	
		/*
			The instance to use.
		*/
		public static readonly PrefabService SINGLETON = new PrefabService();

		/*
			Get a prefab of a name.
		*/
		public GameObject Get(string name) {
			if(_prefabs.TryGetValue(_sanitize(name), out var res)) return res;
			return null;
		}

		
		/*
			Create an instance of a prefab.
		*/
		public GameObject Instance(GameObject obj, Vector3? pos = null) {
			var res = MonoBehaviour.Instantiate(obj, pos ?? (Vector3) obj.transform.position, obj.transform.rotation);
			return res;
		}
		public GameObject Instance(string name, Vector3? pos = null)
			=> Instance(_prefabs[_sanitize(name)], pos);

		private GameObject Instance(GameObject obj, Transform parent) {
			var res = MonoBehaviour.Instantiate(obj, parent);
			return res;
		}
		public GameObject Instance(string name, Transform parent)
			=> Instance(_prefabs[_sanitize(name)], parent);


		public GameObject Instance(GameObject obj, Vector3 position, Quaternion rotation, Transform parent) {
			return MonoBehaviour.Instantiate(obj, position, rotation, parent);
		}
		public GameObject Instance(GameObject obj, Vector3 position, Quaternion rotation) {
			return MonoBehaviour.Instantiate(obj, position, rotation);
		}


		/*
			Destroy an instance.
		*/
		public void Destroy(GameObject obj)
			=> MonoBehaviour.Destroy(obj);
			
	}

	public partial class PrefabService {	
		private Dictionary<string, GameObject> _prefabs = new Dictionary<string, GameObject>();

		private System.Random _rnd = new System.Random();

		private PrefabService() {
			_loadItems();
		}

		/*
			Load the items into the dictionary.
			Based on: https://stackoverflow.com/a/67670629
		*/
		private void _loadItems() {
			var dict = new Dictionary<string, GameObject>();

			foreach (string file in Directory.EnumerateFiles("Assets\\Resources\\Prefabs", "*.*", SearchOption.AllDirectories)) {
				// Skip meta files.
				if(file.Contains(".meta")) continue;
				var name = _sanitize(file.Replace(".prefab", "").Replace("Assets\\Resources\\Prefabs\\", ""));
				GameObject go = Resources.Load<GameObject>( file.Replace("Assets\\Resources\\", "").Replace(".prefab", "") );
				//Debug.Log($"[AudioService._loadItems()] {name}");
				//if(go == null) Debug.Log($"[AudioService._loadItems()] ITEM IS NULL!!! {name}");
				dict[name] = go;
			}
			
			_prefabs = dict;
		}

	

		private string _sanitize(string name)
			=> name.Replace("/", "\\").Replace(" ", "").Replace("_", "").ToLower();

	}
}