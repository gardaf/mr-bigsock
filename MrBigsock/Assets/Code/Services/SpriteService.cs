using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEditor;

using BigSock.Item;


namespace BigSock.Service {
	
	/*
		Service for handling sprites.
	*/
	public partial class SpriteService {	
		/*
			The instance to use.
		*/
		public static readonly SpriteService SINGLETON = new SpriteService();

		/*
			Get a prefab of a name.
		*/
		public Sprite Get(string name) {
			if(_sprites.TryGetValue(_sanitize(name), out var res)) return res;
			return null;
		}
			
	}

	public partial class SpriteService {	
		private Dictionary<string, Sprite> _sprites = new Dictionary<string, Sprite>();

		private System.Random _rnd = new System.Random();

		private SpriteService() {
			_loadItems();
		}

		/*
			Load the items into the dictionary.
			Based on: https://stackoverflow.com/a/67670629
		*/
		private void _loadItems() {
			var dict = new Dictionary<string, Sprite>();

			foreach (string file in Directory.EnumerateFiles("Assets\\Resources\\Sprites", "*.*", SearchOption.AllDirectories)) {
				// Skip meta files.
				if(file.Contains(".meta")) continue;
				var name = _sanitize(file.Replace(".png", "").Replace(".jpg", "").Replace("Assets\\Resources\\Sprites\\", ""));
				Sprite go = Resources.Load<Sprite>( file.Replace("Assets\\Resources\\", "").Replace(".png", "").Replace(".jpg", "") );
				//Debug.Log($"[AudioService._loadItems()] {name}");
				//if(go == null) Debug.Log($"[AudioService._loadItems()] ITEM IS NULL!!! {name}");
				dict[name] = go;
			}
			
			_sprites = dict;
		}
		


		private string _sanitize(string name)
			=> name.Replace("/", "\\").Replace(" ", "").Replace("_", "").ToLower();

	}
}