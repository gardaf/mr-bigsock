using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using UnityEngine;
using UnityEngine.InputSystem;

using BigSock.Item;


namespace BigSock.Service {
	
	/*
		Service for handling items.
	*/
	public partial class ItemService {	
		/*
			The instance to use.
		*/
		public static readonly ItemService SINGLETON = new ItemService();

		/*
			Get an instance of the item of the given id.
		*/
		public IItem Get(ulong id) {
			if(_items.TryGetValue(id, out var res)) return _new(res);
			return null;
		}

		/*
			Get a random item from the item pool.
		*/
		public IItem GetRandom() {
			var num = _rnd.Next(_itemList.Count);
			return _new(_itemList[num]);
		}

		public List<IItem> Get3Random() {
			List<IItem> returnList = new List<IItem>();
			while (returnList.Count < 3) {
				IItem random = _itemList[_rnd.Next(_itemList.Count)];
				if(!returnList.Contains(random)) {
					returnList.Add(random);
				}
			}
			return returnList;
		}

	}

	public partial class ItemService {	
		private Dictionary<ulong, IItem> _items = new Dictionary<ulong, IItem>();
		private List<IItem> _itemList = new List<IItem>();

		private System.Random _rnd = new System.Random();

		private ItemService() {
			_loadItems();
		}

		/*
			Load the items into the dictionary.
			Reflection code: https://stackoverflow.com/a/6944605
		*/
		private void _loadItems() {
			// Get the classs that inherit the item base class.
			var types = Assembly
				.GetAssembly(typeof(ItemBase))
				.GetTypes()
				.Where(myType => myType.IsClass 
					&& !myType.IsAbstract 
					&& myType.IsSubclassOf(typeof(ItemBase)));

			// Create list of instances.
			_itemList = types
				.Select(t => (IItem) Activator.CreateInstance(t, new object[0]))
				.ToList();

			// Map to a dictionary by their ids.
			_items = _itemList
				.ToDictionary(t => t.Id);
		}

		/*
			Creates a new instance of the object.
		*/
		private IItem _new(IItem obj) 
			=> (IItem) Activator.CreateInstance(obj.GetType(), new object[0]);
		
	}
}