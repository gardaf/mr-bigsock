using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEditor;

using BigSock.Item;


namespace BigSock.Service {
	
	/*
		Service for handling audio.
	*/
	public partial class AudioService {	
		/*
			The instance to use.
		*/
		public static readonly AudioService SINGLETON = new AudioService();

		/*
			Get a audio of a name.
		*/
		public AudioClip Get(string name) {
			if(_items.TryGetValue(_sanitize(name), out var res)) return res;
			return null;
		}
			
	}

	public partial class AudioService {	
		private Dictionary<string, AudioClip> _items = new Dictionary<string, AudioClip>();

		private System.Random _rnd = new System.Random();

		private AudioService() {
			_loadItems();
		}

		/*
			Load the items into the dictionary.
			Based on: https://stackoverflow.com/a/67670629
		*/
		private void _loadItems() {
			var dict = new Dictionary<string, AudioClip>();

			foreach (string file in Directory.EnumerateFiles("Assets\\Resources\\sound", "*.*", SearchOption.AllDirectories)) {
				// Skip meta files.
				if(file.Contains(".meta")) continue;
				var name = _sanitize(file.Replace(".wav", "").Replace(".mp3", "").Replace("Assets\\Resources\\sound\\", ""));
				AudioClip go = Resources.Load<AudioClip>( file.Replace("Assets\\Resources\\", "").Replace(".wav", "").Replace(".mp3", "") );
				//Debug.Log($"[AudioService._loadItems()] {name}");
				//if(go == null) Debug.Log($"[AudioService._loadItems()] ITEM IS NULL!!! {name}");
				dict[name] = go;
			}
			
			_items = dict;
		}


		private string _sanitize(string name)
			=> name.Replace("/", "\\").Replace(" ", "").Replace("_", "").ToLower();

	}
}