using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bigsock {
	[CreateAssetMenu(fileName = "NeigbourParameters_", menuName = "PCG/NeigbourData")]
	public class NeigbourSO : ScriptableObject {
		[Range(25f, 100f)]
		public int width;
		[Range(10f, 100f)]
		public int height;
		public int smoothCount;
		public bool empty, edgesAreWalls;
	}
}