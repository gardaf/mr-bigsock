using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Bigsock {
	[CreateAssetMenu(fileName = "TileSetParam_", menuName = "PCG/TileSetData")]
	public class TileSetSO : ScriptableObject {
		public TileBase wallLeftTile, wallRightTile;
		public TileBase[] wallBottomTile, wallTopTile, wallCornerTile, floorTile, door, wallStatue;
		[Range(10, 50)]
		public int floorRandom = 10, wallRandom = 10;
	}
}
