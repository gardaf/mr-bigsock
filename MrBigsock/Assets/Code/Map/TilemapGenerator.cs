using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Bigsock {
	public class TilemapGenerator : MonoBehaviour {
		[SerializeField] Tilemap[] FloorTilemap, WallBottom, WallTop, WallSide;
		[SerializeField] TileSetSO tileSetSO;
		[SerializeField] PolygonCollider2D polygonCollider;
		[SerializeField] GameObject Door;
		[SerializeField] GameObject[] Enemy;
		[SerializeField] GameObject[] Boss;
		[SerializeField] GameObject Stairs;
		[SerializeField] GameObject Chest;
		private int z_value = 0;
		private static int roomID = 0;

		static List<Vector3Int> DoorLocations = new List<Vector3Int>();
		static List<List<Vector2>> MapBoundaryList = new List<List<Vector2>>();


		private int bossCounter = 0;
		/*
		Makes an array with numbers, where each number decides what type of tile will be on what spot on the floor*/
		public int[,] GenerateArray(int width, int height, bool empty) {
			int[,] map = new int[width, height];
			for (int x = 0; x < map.GetUpperBound(0); x++) {
				for (int y = 0; y < map.GetUpperBound(1); y++) {
					int fill = Random.Range(0, 100);
					int num;
					//Random number from how many tiles possible for floor, not basic
					if (fill < tileSetSO.floorRandom) num = Random.Range(1, tileSetSO.floorTile.Length - 1);
					else num = 0;   //Basic floor

					map[x, y] = num;
				}
			}
			return map;
		}

		/*
		Draws out the floor and walls of a room into the scene */
		public void RenderMap(int[,] map, int roomNr, bool LastRoom) {
			int doorLocation = Random.Range(0, map.GetUpperBound(0) - 4);
			bool doorInRoom = false;
			int doorTop = Random.Range(0, 2);   // 0 = bottom, 1 = top
			int topWallVariable;

			//Clear the map
			Clear(roomNr, false);

			//Loop thorugh width : floor
			for (int x = 0; x < map.GetUpperBound(0); x++) {
				for (int y = 0; y < map.GetUpperBound(1); y++) {
					FloorTilemap[roomNr].SetTile(new Vector3Int(x, y, z_value), tileSetSO.floorTile[map[x, y]]); // 1 = tile, 0 = no tile
				}
			}

			//Walls
			for (int x = 0; x <= map.GetUpperBound(0) - 1; x++) {
				//Left wall
				if (x == 0) {
					WallSide[roomNr].SetTile(new Vector3Int(x, map.GetLowerBound(1), z_value), tileSetSO.wallLeftTile);
					WallBottom[roomNr].SetTile(new Vector3Int(x, map.GetLowerBound(1), z_value), tileSetSO.wallBottomTile[0]);
					for (int y = 1; y < (map.GetUpperBound(1) + 1); y++){
						WallSide[roomNr].SetTile(new Vector3Int(x, y, z_value), tileSetSO.wallLeftTile);
					}
					WallTop[roomNr].SetTile(new Vector3Int(x, map.GetUpperBound(1) + 1, z_value), tileSetSO.wallTopTile[0]);
				}
				//Right wall
				if (x == (map.GetUpperBound(0) - 1)) {
					WallSide[roomNr].SetTile(new Vector3Int(x, map.GetLowerBound(1), z_value), tileSetSO.wallRightTile);
					WallBottom[roomNr].SetTile(new Vector3Int(x, map.GetLowerBound(1), z_value), tileSetSO.wallCornerTile[1]);
					for (int y = 0; y < (map.GetUpperBound(1) + 1); y++){
						WallSide[roomNr].SetTile(new Vector3Int(x, y, z_value), tileSetSO.wallRightTile);
					}
					WallTop[roomNr].SetTile(new Vector3Int(x, (map.GetUpperBound(1) + 1), z_value), tileSetSO.wallCornerTile[0]);
				}

				//Check for start of wall
				if (x != map.GetLowerBound(0) && x != (map.GetUpperBound(0) - 1)) {
					//Bottom wall
					WallBottom[roomNr].SetTile(new Vector3Int(x, map.GetLowerBound(1), z_value), tileSetSO.wallBottomTile[0]);
					//Top wall
					WallTop[roomNr].SetTile(new Vector3Int(x, map.GetUpperBound(1) + 1, z_value), tileSetSO.wallTopTile[0]);
				}

				//Bottom wall middle part
				if (Random.Range(0, 100) < tileSetSO.wallRandom){
					WallBottom[roomNr].SetTile(new Vector3Int(x, map.GetLowerBound(1) - 1, z_value), tileSetSO.wallBottomTile[Random.Range(2,
							tileSetSO.wallBottomTile.GetUpperBound(0))]);
				}
				else{
					WallBottom[roomNr].SetTile(new Vector3Int(x, map.GetLowerBound(1) - 1, z_value), tileSetSO.wallBottomTile[1]);
				}

				//Top wall middle part
				if (Random.Range(0, 100) < tileSetSO.wallRandom){
					//Check that it is not near a corner or the start of the door location
					if (x != map.GetUpperBound(0) - 1 && x != map.GetLowerBound(0) && x != doorLocation){
						topWallVariable = Random.Range(2, tileSetSO.wallTopTile.GetUpperBound(0));
						if (topWallVariable == 2){
							ChangeFloorTile(map, x, map.GetUpperBound(1) - 1, roomNr);
							ChangeTopTile(map, x, map.GetUpperBound(1) + 1, roomNr);
						}
					}
					//Everythin that is not the statue
					else{
						topWallVariable = Random.Range(3, tileSetSO.wallTopTile.GetUpperBound(0));
					}

					WallTop[roomNr].SetTile(new Vector3Int(x, map.GetUpperBound(1), z_value), tileSetSO.wallTopTile[topWallVariable]);
				}
				else{
					WallTop[roomNr].SetTile(new Vector3Int(x, map.GetUpperBound(1), z_value), tileSetSO.wallTopTile[1]);    //Middle part of top wall
				}

				int fast = 0;
				//Check for where the door will be located : when there is no door
				if (x == doorLocation && !doorInRoom && !LastRoom) {
					bool doorPlaced = false;
					for (int x1 = 0; x1 < 4; x1++) {
						//Makes the door on the bottom wall
						if (doorTop == 0) {
							//Door
							if (!doorPlaced) {
								WallBottom[roomNr].SetTile(new Vector3Int(x + x1, map.GetLowerBound(1), z_value), tileSetSO.door[x1]);
								WallBottom[roomNr].SetTile(new Vector3Int(x + x1, map.GetLowerBound(1) - 1, z_value), tileSetSO.door[x1 + 4]);
								//Door prefab
								Instantiate(Door, new Vector3(WallBottom[roomNr].transform.position.x + x + 2, WallBottom[roomNr].transform.position.y,
										z_value), Quaternion.identity);
								DoorLocations.Add(new Vector3Int((int)WallBottom[roomNr].transform.position.x + x + 2,
										(int)WallBottom[roomNr].transform.position.y, z_value));
								FloorTilemap[roomNr].SetTile(new Vector3Int(x + x1 + 1, map.GetLowerBound(1) - 1, z_value), tileSetSO.floorTile[0]);
								FloorTilemap[roomNr].SetTile(new Vector3Int(x + x1 + 2, map.GetLowerBound(1) - 1, z_value), tileSetSO.floorTile[0]);
								doorPlaced = true;
							}
							if (x1 == 3) {
								WallBottom[roomNr].SetTile(new Vector3Int(x + x1, map.GetLowerBound(1), z_value), tileSetSO.door[x1]);
								WallBottom[roomNr].SetTile(new Vector3Int(x + x1, map.GetLowerBound(1) - 1, z_value), tileSetSO.door[x1 + 4]);
							}

							if (x1 > 0 && x1 < 3) WallBottom[roomNr].SetTile(new Vector3Int(x + x1, map.GetLowerBound(1) + 1, z_value),
									 tileSetSO.door[x1 + 7]); //Makes top of door

							//Top Wall opposite of the door : Wall
							WallTop[roomNr].SetTile(new Vector3Int(x + x1, map.GetUpperBound(1) + 1, z_value), tileSetSO.wallTopTile[0]);
							WallTop[roomNr].SetTile(new Vector3Int(x + x1, map.GetUpperBound(1), z_value), tileSetSO.wallTopTile[1]);
						}
						//Makes the door on the top wall
						else {
							if (!doorPlaced) {
								WallTop[roomNr].SetTile(new Vector3Int(x + x1, map.GetUpperBound(1) + 1, z_value), tileSetSO.door[x1]);
								WallTop[roomNr].SetTile(new Vector3Int(x + x1, map.GetUpperBound(1), z_value), tileSetSO.door[x1 + 4]);
								//Door prefab
								Instantiate(Door, new Vector3(WallTop[roomNr].transform.position.x + x + 2,
										WallTop[roomNr].transform.position.y + map.GetUpperBound(1) + 1, z_value), Quaternion.identity);
								DoorLocations.Add(new Vector3Int((int)WallTop[roomNr].transform.position.x + x + 2,
										(int)WallTop[roomNr].transform.position.y, z_value));
								FloorTilemap[roomNr].SetTile(new Vector3Int(x + x1 + 1, map.GetUpperBound(1), z_value), tileSetSO.floorTile[0]);
								FloorTilemap[roomNr].SetTile(new Vector3Int(x + x1 + 2, map.GetUpperBound(1), z_value), tileSetSO.floorTile[0]);
								doorPlaced = true;
							}
							if (x1 == 3) {
								WallTop[roomNr].SetTile(new Vector3Int(x + x1, map.GetUpperBound(1) + 1, z_value), tileSetSO.door[x1]);
								WallTop[roomNr].SetTile(new Vector3Int(x + x1, map.GetUpperBound(1), z_value), tileSetSO.door[x1 + 4]);
							}

							if (x1 > 0 && x1 < 3) WallTop[roomNr].SetTile(new Vector3Int(x + x1, map.GetUpperBound(1) + 2, z_value),
									tileSetSO.door[x1 + 7]); //Makes top of door

							//Bottom Wall opposite of the door : Wall
							WallBottom[roomNr].SetTile(new Vector3Int(x + x1, map.GetLowerBound(0), z_value), tileSetSO.wallBottomTile[0]);
							WallBottom[roomNr].SetTile(new Vector3Int(x + x1, map.GetLowerBound(0) - 1, z_value), tileSetSO.wallBottomTile[1]);
						}
					}
					fast++;
					doorInRoom = true;
					x += 3; //Jump x over the door location
				} else if (fast == 0 && LastRoom) {
					DoorLocations.Add(new Vector3Int((int)WallTop[roomNr].transform.position.x + x + 2,
													(int)WallTop[roomNr].transform.position.y, z_value));
					fast++;
				}
			}
			spawnChest(map, roomNr);
			SetMapBoundary(map, roomNr);
		}


		/*
		Gives out the corner points of the room to know how to set the boundries in the world */
		public void SetMapBoundary(int[,] map, int roomNr) {
			List<Vector2> MapBoudary = new List<Vector2>();
			MapBoudary.Add(new Vector2(FloorTilemap[roomNr].transform.position.x + map.GetLowerBound(0),
					FloorTilemap[roomNr].transform.position.y + map.GetLowerBound(1) - 1));
			MapBoudary.Add(new Vector2(FloorTilemap[roomNr].transform.position.x + map.GetLowerBound(0),
					FloorTilemap[roomNr].transform.position.y + map.GetUpperBound(1) + 1.2f));
			MapBoudary.Add(new Vector2(FloorTilemap[roomNr].transform.position.x + map.GetUpperBound(0),
					FloorTilemap[roomNr].transform.position.y + map.GetUpperBound(1) + 1.2f));
			MapBoudary.Add(new Vector2(FloorTilemap[roomNr].transform.position.x + map.GetUpperBound(0),
					FloorTilemap[roomNr].transform.position.y + map.GetLowerBound(1) - 1));
			MapBoundaryList.Add(MapBoudary);
		}

		/*
		Returns the spesific MapBoundry from a list*/
		public static List<Vector2> GetRoomBoundary(int i) {
			return MapBoundaryList[i];
		}

		/*
		Returns the ID of the next room*/
		public static int NextRoom() {
			return roomID++;
		}

		public static void SetRoomIDZero() {
			roomID = 1;
		}

		public static void resetMaps() {
			DoorLocations.Clear();
			MapBoundaryList.Clear();
		}

		/*
		Returns the ID of the previous room*/
		public static int LastRoom() {
			return roomID--;
		}

		/*
		Spawns enemies inside the boundry of a spesific room by giving it the space the floor ocopay
		*/
		public void SpawnEnemies(int[,] map, int enemies, int roomNr) {
			foreach (var item in Enemy) {
				item.tag = roomNr.ToString();
			}

			for (int i = 0; i < enemies; i++) {
				int enemyRandom = Random.Range(0, Enemy.Length);
				int randomLocation_x = Random.Range(1, map.GetUpperBound(0) - 1);
				if (randomLocation_x == DoorLocations[roomNr].x){
					randomLocation_x += 1;
				}
				int randomLocation_y = Random.Range(map.GetLowerBound(1) + 2, map.GetUpperBound(1) - 1);
				if (randomLocation_x == DoorLocations[roomNr].y){
					randomLocation_y += 1;
				}
				Instantiate(Enemy[enemyRandom], new Vector3Int((int)FloorTilemap[roomNr].transform.position.x + randomLocation_x,
						(int)FloorTilemap[roomNr].transform.position.y + randomLocation_y, 0), Quaternion.identity);
			}
		}

		//Initializes a gameobject of the enemy type boss
		public void SpawnBoss(int[,] bossRoom, int roomNr) {
			Boss[bossCounter].tag = "Boss";
			Instantiate(Boss[bossCounter], new Vector3Int((int)FloorTilemap[roomNr].transform.position.x + bossRoom.GetUpperBound(0) / 2,
							(int)FloorTilemap[roomNr].transform.position.y + bossRoom.GetUpperBound(1) / 2, 0), Quaternion.identity);
			bossCounter++;
		}

		/*
		Sets new points for where the polygon collider for a room is goint to be set*/
		public void polyCollider(int[,] map, int roomNr) {
			polygonCollider.pathCount = 1;

			List<Vector2> vectors = new List<Vector2>();

			vectors.Add(new Vector2(FloorTilemap[roomNr].transform.position.x + map.GetLowerBound(0),
					FloorTilemap[roomNr].transform.position.y + map.GetLowerBound(1) - 1));
			vectors.Add(new Vector2(FloorTilemap[roomNr].transform.position.x + map.GetLowerBound(0),
					FloorTilemap[roomNr].transform.position.y + map.GetUpperBound(1) + 1.2f));
			vectors.Add(new Vector2(FloorTilemap[roomNr].transform.position.x + map.GetUpperBound(0),
					FloorTilemap[roomNr].transform.position.y + map.GetUpperBound(1) + 1.2f));
			vectors.Add(new Vector2(FloorTilemap[roomNr].transform.position.x + map.GetUpperBound(0),
					FloorTilemap[roomNr].transform.position.y + map.GetLowerBound(1) - 1));

			polygonCollider.SetPath(0, vectors);
		}

		/*
		 gets location of a spesific door*/
		public static Vector3Int DoorLocaitonTransport(int door) {
			return new Vector3Int(DoorLocations[door].x + 1, DoorLocations[door].y + 3, DoorLocations[door].z);
		}

		/*
		Changes the wall on the top of the map to a different tile*/
		private void ChangeTopTile(int[,] map, int x, int y, int roomNr) {
			WallTop[roomNr].SetTile(new Vector3Int(x, y, z_value), null);
			WallTop[roomNr].SetTile(new Vector3Int(x, y, z_value), tileSetSO.wallStatue[0]);
		}

		/*
		 Changes tiles for places there is a statue*/
		public void ChangeFloorTile(int[,] map, int x, int y, int roomNr) {
			FloorTilemap[roomNr].SetTile(new Vector3Int(x, y, z_value), null);
			FloorTilemap[roomNr].SetTile(new Vector3Int(x, y, z_value), tileSetSO.wallStatue[1]);
		}

		//Takes in our map and tilemap, setting null tiles where needed
		public static void UpdateMap(int[,] map, Tilemap tilemap) {
			for (int x = 0; x < map.GetUpperBound(0); x++) {
				for (int y = 0; y < map.GetUpperBound(1); y++) {
					if (map[x, y] == 0){
						tilemap.SetTile(new Vector3Int(x, y, 0), null);
					}
				}
			}
		}

		//Removes current paint on tiles
		public void Clear(int roomNr, bool all) {
			if (all) {
				for (int i = 0; i < roomNr; i++) {
					FloorTilemap[i].ClearAllTiles();
					WallBottom[i].ClearAllTiles();
					WallSide[i].ClearAllTiles();
					WallTop[i].ClearAllTiles();
				}
			} else {
				FloorTilemap[roomNr].ClearAllTiles();
				WallBottom[roomNr].ClearAllTiles();
				WallSide[roomNr].ClearAllTiles();
				WallTop[roomNr].ClearAllTiles();
			}
		}

		/*
		 Initializes a gameobject of stairs*/
		public void SpawnStairs(int[,] bossRoom, int roomNr) {
			Instantiate(Stairs, new Vector3(FloorTilemap[roomNr].transform.position.x + bossRoom.GetUpperBound(0) / 2 + 0.5f,
							FloorTilemap[roomNr].transform.position.y + bossRoom.GetUpperBound(1) - 3 + 0.5f, 0), Quaternion.identity);
		}

		//Spawns a chest in the middle of the room
		public void spawnChest(int[,] room, int roomNr) {
			Chest.tag = "Chest";
			Instantiate(Chest, new Vector3(FloorTilemap[roomNr].transform.position.x + room.GetUpperBound(0) / 2,
							FloorTilemap[roomNr].transform.position.y + room.GetUpperBound(1) / 2, 0), Quaternion.identity);
		}
	}
}