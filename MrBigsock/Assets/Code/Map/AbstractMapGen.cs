using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Bigsock {
	public abstract class AbstractMapGen : MonoBehaviour {
		[SerializeField]
		protected TilemapGenerator tilemapGenerator = null;
		[SerializeField]
		protected int RoomCount;

		//Depricated
		public void GenerateMap() {
			//tilemapVisualizer.Clear();
			RunProceduralGeneration();
		}

		public void Generate() {
			//tilemapGenerator.Clear();
			RunProceduralGeneration();
		}

		public void ClearMap() {
			tilemapGenerator.Clear(RoomCount, true);
			//tilemapVisualizer.Clear();
		}

		public abstract void RunProceduralGeneration();
	}
}