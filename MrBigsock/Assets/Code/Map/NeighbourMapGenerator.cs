using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

namespace Bigsock {
	public class NeighbourMapGenerator : AbstractMapGen {
		[SerializeField]
		protected NeigbourSO[] mapParam;
		[SerializeField] NeigbourSO BossRoom;

		private static List<int[,]> roomList = new List<int[,]>();

		/*
		 Generates an array of how each room will look like and draws it onto the scene.
		Fixes collider for camera also.*/
		public override void RunProceduralGeneration() {
			roomList.Clear();

			for (int i = 0; i <= RoomCount - 1; i++) {
				int randomMap = Random.Range(0, mapParam.Length);
				int[,] map = tilemapGenerator.GenerateArray(mapParam[randomMap].width, mapParam[randomMap].height, mapParam[randomMap].empty);
				roomList.Add(map);
				tilemapGenerator.RenderMap(roomList[i], i, false);
				tilemapGenerator.SpawnEnemies(map, 4 + i, i);

			}
			int[,] bossMap = tilemapGenerator.GenerateArray(BossRoom.width, BossRoom.height, BossRoom.empty);
			roomList.Add(bossMap);
			tilemapGenerator.RenderMap(roomList[roomList.Count - 1], roomList.Count - 1, true);
			tilemapGenerator.SpawnBoss(bossMap, roomList.Count - 1);
			tilemapGenerator.SpawnStairs(bossMap, roomList.Count - 1);

			tilemapGenerator.polyCollider(roomList[0], 0);
		}

		/*
		 Returns a list of values for what the floor tiles will be*/
		public static int[,] GetRoomList(int i) {
			return roomList[i];
		}

		/*
		 Returns the number of rooms there is in the scene*/
		public static int GetRoomListCount() {
			return roomList.Count;
		}

		/*
		 Clears the list for objects*/
		public static void ClearRoomList() {
			roomList.Clear();
		}
	}
}