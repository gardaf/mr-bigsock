using System.Collections;
using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.InputSystem;

using BigSock.Service;

namespace BigSock {

	/*
		A basic dodge move that gives a push and a short invincibility.
	*/
	public class AbilityDodge : BaseAbility {
		public static readonly float BASE_FORCE = 1f; 
		public static readonly TimeSpan IFRAME_DURATION = new TimeSpan(0, 0, 0, 0, 500);
		
		public override ulong Id => 201;
		public override string Name => "Dodge";
		public override string Description => "A basic dodge move.";
		public override string IconName => "item/foureyes";

		public AbilityDodge() {
			StaminaCost = 10;
			Cooldown = new TimeSpan(0, 0, 0, 1, 0);
		}

		/*
			Activates the ability.
		*/
		protected override bool Activate(IAbilityParam par) {
			var actor = par.Actor; 
			var direction = par.MovementDir;

			if(direction == null) return false;
			
			// Apply the push and iframes.
			actor.KnockBack(BASE_FORCE * actor.Stats.MoveSpeed, direction.Value);
			actor.AddStatusEffect(StatusEffectType.Invincible, IFRAME_DURATION);

			return true;
		}

	}

}