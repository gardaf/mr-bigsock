using System.Collections;
using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.InputSystem;

using BigSock.Service;

namespace BigSock {

	/*
		Basic projectile attack for the player..
	*/
	public class BasicArrow : BaseAttack {
		//protected static readonly GameObject PROJECTILE_BASE = new AttackMovement();
		public const string PROJECTILE_NAME = "bullets/enemy_arrow";

		
		public override ulong Id => 103;
		public override string Name => "Basic Player Projectile Attack";
		public override string Description => "A basic projectile shooting attack the player has.";

		public BasicArrow() {
			AttackStats = new AttackStats{
				Damage = 1f,
				Knockback = 1f,
				Range = 10f,
				ProjectileSpeed = 10f,
				AttackSpeed = 1f,
				CritChance = 0.1f,
				CritDamageModifier = 2f,
			};
		}



		/*
			Activates the ability.
			Returns true if the ability was successfully activated. 
				- Even if nothing was hit, used to indicate that cooldowns should be updated.
			This should be overridden in sub-classes for the actual abilities.
		*/
		protected override bool Activate(IAbilityParam par) {
			var actor = par.Actor; 
			var target = par.TargetPos;

			if(target == null)
				return false;

			var attack = (AttackStats) AttackStats.Calculate(actor.Stats);
			attack.Actor = actor;

			var bullet = PrefabService.SINGLETON.Instance(PROJECTILE_NAME, actor.transform.position);

			//bullet.transform.rotation = Quaternion.LookRotation(temp.normalized);
			//bullet.transform.rotation = Quaternion.LookRotation(temp1 ,Vector3.up);

			var bulletScript = bullet.GetComponent<AttackMovement>();
			bulletScript.Stats = attack;
			bulletScript.Direction = (target.Value - (Vector2) actor.transform.position).normalized;


			//MonoBehaviour.Instantiate(PROJECTILE_BASE, (Vector3) actor.transform.position, PROJECTILE_BASE.transform.rotation);
			return true;
		}

	}

}