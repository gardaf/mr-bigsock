using System.Collections;
using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.InputSystem;

using BigSock.Service;

namespace BigSock {

	/*
		Basic projectile attack for the player..
	*/
	public class BiggerSlowerProjectile : BaseAttack {
		//protected static readonly GameObject PROJECTILE_BASE = new AttackMovement();
		public const string PROJECTILE_NAME = "bullets/bigslowbullet";

		
		public override ulong Id => 102;
		public override string Name => "Big Ball";
		public override string Description => "It's big and slow, but it packs a punch.";
		public override string IconName => "item/premature";

		public BiggerSlowerProjectile() {
			AttackStats = new AttackStats{
				Damage = 4f,
				Knockback = 5f,
				Range = 5f,
				ProjectileSpeed = 3f,
				AttackSpeed = 1f,
				CritChance = 0.25f,
				CritDamageModifier = 3f,
				DamageVariance = 0.5f,
			};

			Cooldown = new TimeSpan(0, 0, 0, 2, 0);
			ManaCost = 5;
			FireType = FireType.Charge;
			MinCharge = 1f;
			MaxCharge = 3f;

		}



		/*
			Activates the ability.
			Returns true if the ability was successfully activated. 
				- Even if nothing was hit, used to indicate that cooldowns should be updated.
			This should be overridden in sub-classes for the actual abilities.
		*/
		protected override bool Activate(IAbilityParam par) {
			var actor = par.Actor; 
			var target = par.TargetPos;

			if(target == null) return false;

			Debug.Log($"[BiggerSlowerProjectile.Activate()] Charge: {par.ChargeTime:N1} ({par.ChargeTimePercent:P0})");

			var attack = (AttackStats) AttackStats.Calculate(actor.Stats);
			attack.Actor = actor;

			var bullet = PrefabService.SINGLETON.Instance(PROJECTILE_NAME, actor.transform.position);
			var bulletScript = bullet.GetComponent<AttackMovement>();
			bulletScript.Stats = attack;
			bulletScript.Direction = (target.Value - (Vector2) actor.transform.position).normalized;

			return true;
		}

	}

}