using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.InputSystem;

using BigSock.Service;

namespace BigSock {

	/*
		Basic projectile attack for the player..
	*/
	public class BasicProjectile1 : BaseAttack {
		//protected static readonly GameObject PROJECTILE_BASE = new AttackMovement();
		public const string PROJECTILE_NAME = "bullets/basicsquarebullet";
		public const string AUDIO_PATH = "The Essential Retro Video Game Sound Effects Collection [512 sounds] By Juhani Junkala/Weapons/Melee/sfx_wpn_punch3";
		

		
		public override ulong Id => 101;
		public override string Name => "Basic Player Projectile Attack";
		public override string Description => "A basic projectile shooting attack the player has.";
		public override string IconName => "item/coffee";

		public BasicProjectile1() {
			AttackStats = new AttackStats{
				Damage = 1f,
				Knockback = 1f,
				Range = 10f,
				ProjectileSpeed = 10f,
				AttackSpeed = 1f,
				CritChance = 0.1f,
				CritDamageModifier = 2f,
			};
			ManaCost = 1;
			FireType = FireType.FullAuto;
		}



		/*
			Activates the ability.
			Returns true if the ability was successfully activated. 
				- Even if nothing was hit, used to indicate that cooldowns should be updated.
			This should be overridden in sub-classes for the actual abilities.
		*/
		protected override bool Activate(IAbilityParam par) {
			var actor = par.Actor; 
			var target = par.TargetPos;

			if(target == null) return false;

			var attack = (AttackStats) AttackStats.Calculate(actor.Stats);
			attack.Actor = actor;

			var bullet = PrefabService.SINGLETON.Instance(PROJECTILE_NAME, actor.transform.position);
			var bulletScript = bullet.GetComponent<AttackMovement>();
			bulletScript.Stats = attack;
			bulletScript.Direction = (target.Value - (Vector2) actor.transform.position).normalized;

			// Play sound effect
			var source = actor.source.FirstOrDefault();
			var audioClip = AudioService.SINGLETON.Get(AUDIO_PATH);
			if (source != null && audioClip != null) {
				source.clip = audioClip;
				source.Play();
			} else {
				if(source == null)    Debug.Log($"[BasicProjectile1.Activate()] audio source was null.");
				if(audioClip == null) Debug.Log($"[BasicProjectile1.Activate()] audio clip was null.");
			}


			return true;
		}

	}

}