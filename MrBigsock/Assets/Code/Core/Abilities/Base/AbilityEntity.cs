﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;



namespace BigSock {
	/*
		An object representing an ability held by a player.
	*/
	public class AbilityEntity {

		// The ability held in this slot.
		public IAbility Ability { get; }

		// The index of the ability
		public int Index { get; }

		// The time this ability started charging.
		public float ChargeStarted { get; set; }

		// The keys bound to this ability.
		public List<KeyCode> Keys { get; }


		// Indicates whether or not the ability is charging.
		public bool IsCharging => ChargeStarted > 0;

		// The time this ability has been charging.
		public float ChargeTime => Time.time - ChargeStarted;

		// The percent of maximum time the ability has charged for.
		public float ChargePercent => (Ability.FireType == FireType.Charge && IsCharging)
			? Math.Clamp(ChargeTime / Ability.MaxCharge, 0, 1)
			: 0;


		// How far into the cooldown the attack is.
		public float CooldownPercent
			=> (float)Math.Clamp((DateTime.Now - Ability.NextTimeCanUse + Ability.Cooldown) / Ability.Cooldown, 0, 1);


		public AbilityEntity(IAbility ability, int index, List<KeyCode> keys = null) {
			if (ability == null) throw new ArgumentNullException(nameof(ability));

			Ability = ability;
			Index = index;
			Keys = keys ?? new List<KeyCode>();
		}
	}
}
