using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEngine.InputSystem;

using BigSock.Service;


namespace BigSock {

	/*
		Base class for abilities.
	*/
	public abstract class BaseAbility : IAbility {
		/*
			The cooldown of the ability in seconds.
		*/
		public TimeSpan Cooldown { get; set; } = new TimeSpan(0, 0, 0, 0, 250); // 1/4 seconds.

		/*
			The name of the ability.
		*/
		public abstract string Name { get; }

		/*
			The description of the ability.
		*/
		public abstract string Description { get; }

		/*
			The id of the ability.
		*/
		public abstract ulong Id { get; }


		/*
			The icon of the ability.
		*/
		public Sprite Icon => SpriteService.SINGLETON.Get(IconName);

		/*
			The name of the icon this ability uses.
				Override this to change what icon the item uses.
		*/
		public virtual string IconName { get; } = "item/runningshoes";


		/*
			The next time the ability has cooled down.
		*/
		public DateTime NextTimeCanUse { get; protected set; } = DateTime.Now;

		/*
			Whether the ability is ready.
		*/
		public bool Ready => NextTimeCanUse <= DateTime.Now;



		/*
			The mana cost of the ability.
		*/
		public float ManaCost { get; protected set; }

		/*
			The stamina cost of the ability.
		*/
		public float StaminaCost { get; protected set; }

		/*
			The hp cost of the ability.
		*/
		public float HPCost { get; protected set; }


		/*
			--------------------
			Charging related properties.
			--------------------
		*/

		/*
			What kind of firetype this ability uses.
		*/
		public FireType FireType { get; protected set; }

		/*
			The minimum time in seconds the ability needs to be charged.
		*/
		public float MinCharge { get; protected set; } = 0.1f;

		/*
			The maximum time in seconds the ability before the ability is fully charged.
		*/
		public float MaxCharge { get; protected set; } = 0.2f;



		/*
			Try to use the ability.
			Handles cooldown and ability cost here.
			Returns true if the ability was successfully used.
		*/
		public bool Use(IAbilityParam par) {
			var actor = par.Actor;

			// Check the charging
			if (FireType == FireType.Charge) {
				// Check that we charged the minimum time.
				if (par.ChargeTime < MinCharge) return false;

				// Calculate how much optional charging we did.
				par.ChargeTimePercent = Math.Clamp((par.ChargeTime - MinCharge) / (MaxCharge - MinCharge), 0f, 1f);
			}

			// Check that the ability is cooled down.
			if (Ready) {
				//> Handle checking costs here.
				if (ManaCost > 0f && actor.Mana < ManaCost) return false;
				if (StaminaCost > 0f && actor.Stamina < StaminaCost) return false;
				if (HPCost > 0f && actor.HP < HPCost) return false;

				// Activate the ability.
				var res = Activate(par);

				// If it succeeded, update cooldown and pay ability cost.
				if (res) {
					NextTimeCanUse = DateTime.Now + Cooldown;
					//> Handle paying the cost (HP, mana, stamina) here.
					if (ManaCost > 0f) actor.Mana -= ManaCost;
					if (StaminaCost > 0f) actor.Stamina -= StaminaCost;
					if (HPCost > 0f) actor.HP -= HPCost;
				}

				return res;
			}
			return false;
		}



		/*
			Activates the ability.
			Returns true if the ability was successfully activated. 
				- Even if nothing was hit, used to indicate that cooldowns should be updated.
			This should be overridden in sub-classes for the actual abilities.
		*/
		protected abstract bool Activate(IAbilityParam par);




		/*
			Returns text that represents info about the ability.
		*/
		public string GetToolTip() {
			var sb = new StringBuilder()
				.AppendLine(Name)
				.AppendLine()
				.AppendLine(Description)
				.AppendLine($"Cooldown: {Cooldown}");

			if (ManaCost != 0) sb.AppendLine($"Mana Cost: {ManaCost}");
			if (StaminaCost != 0) sb.AppendLine($"Stamina Cost: {StaminaCost}");
			if (HPCost != 0) sb.AppendLine($"HP Cost: {HPCost}");

			return sb.ToString();
		}

	}

	/*
	Notes:
		- Subclasses should override Activate() to implement their actual functionality.
		- Use() should not be overridden unless absolutely neccesary.
		- Activate() can be used to hold special conditions, if it returns false, no cooldowns or costs will be enforced.
			+ An ability that can only be activated if there is a valid target, Activate() can return false if no targets found.
		- Planning to expand code later:
			- Allow passing a target character instead of a position.
			- Return a result object instead of just bool to allow for more fine grained controll, like resetting cooldown without mana cost.
			- Adding cost management, use count limits (pr lvl or in total)
	*/
}