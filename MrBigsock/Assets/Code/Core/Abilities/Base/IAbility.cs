using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock {

	/*
		Interface that represents an ability.
	*/
	public interface IAbility {
		/*
			The cooldown of the ability.
		*/
		TimeSpan Cooldown { get; }

		/*
			The name of the ability.
		*/
		string Name { get; }

		/*
			The description of the ability.
		*/
		string Description { get; }

		/*
			The id of the ability.
		*/
		ulong Id { get; }

		/*
			The icon of the ability.
		*/
		Sprite Icon { get; }

		/*
			The next time the ability has cooled down.
		*/
		DateTime NextTimeCanUse { get; }

		/*
			Whether the ability is ready.
		*/
		bool Ready { get; }



		/*
			The mana cost of the ability.
		*/
		float ManaCost { get; }

		/*
			The stamina cost of the ability.
		*/
		float StaminaCost { get; }

		/*
			The hp cost of the ability.
		*/
		float HPCost { get; }



		/*
			--------------------
			Charging related properties.
			--------------------
		*/

		/*
			What kind of firetype this ability uses.
		*/
		FireType FireType { get; }

		/*
			The minimum time in seconds the ability needs to be charged.
		*/
		float MinCharge { get; }

		/*
			The maximum time in seconds the ability before the ability is fully charged.
		*/
		float MaxCharge { get; }



		/*
			-----------------------------
			Add in something for costs.
			- Uses
			- Mana
			- Stamina
			- HP
			-----------------------------
		*/



		/*
			Try to use the ability.
		*/
		bool Use(IAbilityParam par);

		/*
			Returns text that represents info about the ability.
		*/
		string GetToolTip();

	}
}