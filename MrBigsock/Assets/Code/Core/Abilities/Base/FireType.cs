using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock {

	/*
		How an ability should be fired off.
	*/
	public enum FireType {
		Standard,  // Fires on press. Holding down does nothing.
		FullAuto,  // Can hold down to auto-fire
		Charge,    // Ability can be held down to charge.
	}

}
