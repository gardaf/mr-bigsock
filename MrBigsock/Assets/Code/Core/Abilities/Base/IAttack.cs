using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;



namespace BigSock {

	/*
		Interface that represents an attack.
	*/
	public interface IAttack : IAbility {
		/*
			The attack stats of the ability.
		*/
		IAttackStats AttackStats { get; }




	}
}