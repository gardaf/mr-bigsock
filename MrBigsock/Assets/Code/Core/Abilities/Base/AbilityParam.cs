using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock {

	/*
		The parameters we send to all abilities.
	*/
	public class AbilityParam : IAbilityParam {

		/*
			The position the actor is currently aiming at.
				Ex.: For player, it's the cursor position.
		*/
		public Vector2? TargetPos { get; set; }

		/*
			The direction the actor is moving.
		*/
		public Vector2? MovementDir { get; set; }

		/*
			The character that activated the ability.
		*/
		public Character Actor { get; set; }

		/*
			How long the ability was charged.
		*/
		public float ChargeTime { get; set; }

		/*
			How much of the allowed charge was done in percent.
			Ex.: if min is 1s, max is 3s, and the ability was charged for 2s, 
				then 0.5 since it was charged for half the time between min and max.
		*/
		public float ChargeTimePercent { get; set; }
	}

}