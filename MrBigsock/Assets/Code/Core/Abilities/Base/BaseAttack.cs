using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock {

	/*
		Base class for attacks.
	*/
	public abstract class BaseAttack : BaseAbility, IAttack {

		/*
			The attack stats of the ability.
		*/
		public IAttackStats AttackStats { get; protected set; }

	}

}