using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock {

	/*
		Interface represents the stats of an attack.
	*/
	public interface IAttackStats {
		/*
			The damage of the attack.
		*/
		float Damage { get; }

		/*
			The knockback of the attack.
		*/
		float Knockback { get; }

		/*
			The range of the attack.
		*/
		float Range { get; }

		/*
			The projectile speed of the attack.
		*/
		float ProjectileSpeed { get; }

		/*
			The attack speed of the attack.
		*/
		float AttackSpeed { get; }

		/*
			The crit chance of the character.
		*/
		float CritChance { get; }
		
		/*
			The how much to modify damage by when critting.
		*/
		float CritDamageModifier { get; }
		
		/*
			How much the damage can vary in percent.
		*/
		float DamageVariance { get; }


		
		/*
			The source of the attack.
		*/
		Vector2 Source { get; }

		/*
			The character that activated the attack.
		*/
		Character Actor { get; }



		/*
			Indicates if the attack stats have been calculated.
		*/
		bool IsCalculated { get; }

		/*
			Indicates if the attack was a critical hit.
		*/
		bool IsCrit { get; }

		/*
			Calculates the final attack stats.
				(Takes crit and damage spread and calculates the final values)
		*/
		IAttackStats Calculate(ICharacterStats charStats = null);
	}


	/*
		Holds extension methods for attack stat objects.
	*/
	public static class AttackStatsExtension {


		/*
			Applies the character's stats to the attack.
		*/
		public static IAttackStats Apply(this IAttackStats a, ICharacterStats b) {
			return new AttackStats{
				Damage = a.Damage * b.Damage,
				Knockback = a.Knockback * b.Knockback,
				Range = a.Range * b.Range,
				ProjectileSpeed = a.ProjectileSpeed * b.ProjectileSpeed,
				AttackSpeed = a.AttackSpeed * b.AttackSpeed,
				CritChance = a.CritChance * b.CritChance,
				CritDamageModifier = a.CritDamageModifier * b.CritDamageModifier,
				
				Source = a.Source,
				Actor = a.Actor,

				DamageVariance = a.DamageVariance,
				IsCalculated = a.IsCalculated,
				IsCrit = a.IsCrit,
			};
		}
	}
}