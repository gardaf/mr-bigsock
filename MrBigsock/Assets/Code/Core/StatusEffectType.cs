using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;


namespace BigSock {
	/*
		The different types of status effects.
	*/
	public enum StatusEffectType {
		Invincible  = 0,
		Invisible   = 1,
		Stun        = 2,
	}
}