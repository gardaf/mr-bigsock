using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock {

	/*
		The skills of a character.
			(Also used to hold the maximum or what we've selected in skill menu)
	*/
	public class Skills {
		public int HP { get; set; }
		public int SP { get; set; }
		public int MP { get; set; }
		public int Damage { get; set; }
		public int Speed { get; set; }
		public int Luck { get; set; }
		public int Concentration { get; set; }

		public Skills Clone() {
			return new Skills{
				HP = HP,
				SP = SP,
				MP = MP,
				Damage = Damage,
				Speed = Speed,
				Luck = Luck,
				Concentration = Concentration,
			};
		}
	}


	/*
		The different skills.
	*/
	public enum Skill {
		HP,
		SP,
		MP,
		Damage,
		Speed,
		Luck,
		Concentration,
	}

	/*
		Holds extension methods for skills objects.
	*/
	public static class SkillsExtension {
			
		/*
			Increases one skill object by the amount of the other.
		*/
		public static Skills Increase(this Skills a, Skills b) {
			a.HP += b.HP;
			a.SP += b.SP;
			a.MP += b.MP;
			a.Damage += b.Damage;
			a.Speed += b.Speed;
			a.Luck += b.Luck;
			a.Concentration += b.Concentration;
			return a;
		}

		
		/*
			Applies skill increase/decrease effects to the stats of a character.
		*/
		public static CharacterStats Apply(this CharacterStats stats, CharacterStats increase, Skill skill, int amount) {
			switch(skill) {
				case Skill.HP:
					stats.MaxHP              += amount * increase.MaxHP;
					break;
				case Skill.SP:
					stats.MaxStamina         += amount * increase.MaxStamina;
					stats.RegenStamina       += amount * increase.RegenStamina;
					break;
				case Skill.MP:
					stats.MaxMana            += amount * increase.MaxMana;
					stats.RegenMana          += amount * increase.RegenMana;
					break;
				case Skill.Damage:
					stats.Damage             += amount * increase.Damage;
					stats.Knockback          += amount * increase.Knockback;
					break;
				case Skill.Speed:
					stats.MoveSpeed          += amount * increase.MoveSpeed;
					stats.AttackSpeed        += amount * increase.AttackSpeed;
					break;
				case Skill.Luck:
					stats.CritChance         += amount * increase.CritChance;
					stats.CritDamageModifier += amount * increase.CritDamageModifier;
					break;
				case Skill.Concentration:
					stats.Range              += amount * increase.Range;
					stats.ProjectileSpeed    += amount * increase.ProjectileSpeed;
					stats.Accuracy           += amount * increase.Accuracy;
					break;
				default:
					throw new NotImplementedException();
			}
			return stats;
		}
		
	}
}
/*
	##################################@@@@WWW@@#####@W@W@WWW@########################################################
	################################WxMWWWMMMMMMMWWMWWW@@WW@@@W@@####################################################
	##############################@MxMWWWMMMW@@@@WWWWMW@@@@@@@@@@WW@@################################################
	##############################MMMWWWMWW@@@@@W@@WWWW@@@@#@###@@@@@@W@#############################################
	############################@xxMMWMWW@@@@@@@@@#@@@W@@@#######@@@@#@@WW@##########################################
	###########################WMMxWWMMWW@@@@@@@@@#@@@@@@#########@@@##@@@WWW@#######################################
	#########################@MMMMMWMWWWW@@@@@#@@@@##@@@@############@@@@@@@##@@@####################################
	#########################WMMWWWWWW@WW@@@@@##@@##@@@@@###########@@@@@######@@WW@#################################
	########################WMWWWW@WW@@WW@@@@#######@@@@@@##@@@@@@@@@@@@######@##@@WM@###############################
	####################WxMMMWWWW@@W@@@WWW@@@#@#####@@@@@#@@###@@@@@@@@@@#########@@@@@@@############################
	##################@MWWWWWWWWW@@@@@@@@@@@#######@@@@@@######@@@WWW@@@################@@@##########################
	#################@W@@@@W@WWW@@@@@@@@@##########@@@@#######@@@@@@@@@@@@@@##@@@#########@@#########################
	################@@@@@@@WW@@@@@#@@@@##############@########@@@@@@@@@@###@#@@@@@#######@##@########################
	###############@W@@WWW@@@@@@@@#@@@@#############@########@@@@@@@@@@##@@##@@@@@@@#####@@@@@@######################
	##############@W@WWW@W@@@@@@@@#@@@######################@@#@@####@@@@#####@@@@#########@@#W######################
	#############WW@@@W@@@@@@@@#@@@@@##########################@#@@@@@@#####################@#@@#####################
	############WMWWWW@WWW@@@@@##@@@############################@@@###########################@W@####################
	###########WMWWWW@@@@@@@######@######################@##@@@@@#########@####################@W####################
	##########WMWWWWWWW@@@@@@########################@@#####@@@@##@@@@###@@#########@@@######@##@W###################
	#########@xMWWWWW@W@@@@@@@############################@@@########@@@@@###@#########@@####@@@@WW##################
	#########WMWWW@@@@@@@@@@@@@@##############@###@@@######@@@@@@@@#@@@@@@@@###@########@@@#@@@@@@WW#################
	#########WWW@@@@@@@@@W@@@@@@@################@####@@@@@@@@@@@@@@@@####@@@####@@#########@@@@@@@WW################
	########@WW@@@@@@@@@W@@@@@###############@####@@##@####@@#@@@@@@@@@#####@@####@@###########@@@@WWW###############
	########@WW@@@@@@@@WWW@@@##############################@@@@@@@@#@###############@###########@@@@WM@##############
	#######@WW@@@@@@@@@WWW@@@###############################@@@@@####################@##############@@W##############
	#######MW@@@@@WW@@WWWW@@@@#########################@@@@@@@@@####@@@###############@###############@@#############
	######@WW@@@@@@WW@@W@WW@@@@######################@@@@@@##################@###@#####@#############@@W#############
	######@WWWWWW@@@WWWWWWWW@@@#####################@@@@@@########@@@@########@@#@####@##@#############@@############
	######WWWWW@WW@@WMWWWWW@@@@####################@@@@#########################@##@@###################@@###########
	######MWWWW@@@@@@WWWWW@@@@@##################@@############################@@#######################@@@##########
	######W@WWWW@@@@WWWW@@@@@@@###############@@@@@#############################@@###@###################@@##########
	######M@@@WWW@@WWWW@@@@W@@@##@####@####@@@@@@#######@#####@#@################@###@####################@@#########
	######W@@@WW@@WWW@@@@@@W@@@##@@@@@@#@@@@@@@@########@@####@@@@@@@@############@@#######################W#########
	#####WW@@@@@W@WW@@@@@@W@@@##@@@@@@@@@W@@@@@@@#######@#####@@@@@@@@@############@########################@########
	#####@W@@@@@WWW@@@@@@@@WW@@@WW@@@@@@@@@@@@@@@@###@@@@@@@@@@@@@@@@@@@@@#@#####@@#@##@####################@########
	#####@W@@@@@WWW@@@@@@@@@W@@WMWW@@@@W@@WWW@@@@@@@@@WWWW@@@@@@@@@@#@@@@@@@@#####@#@@@@####################@@#######
	#####WW@@@@@WWW@@@##@@WWW@@MMWWW@WWWWW@W@WW@@@@@@WWWW@@@@@@@@@@@@####@###@#####@#@@######################@#######
	#####W@W@@W@WWW@@@#@@WMMMWWMMMMMWWWWMMWWWWWWW@WW@WWW@@@@@@@@##########@##########@@@#####################@#######
	#####W@W@@@WW@@@@@#@@MxxMWMWMMxMWMxMMMMMMWWWWWWWWWWW@WWW@@@@###########@###########@@#####################@######
	#####WWW@@@W@@###@@@WMxnxWMWxxnxMMxMMMxxxMMMMWWW@W@@@@@@@@@@###@########@####@######@#####################@######
	####@WWW@@@W@@#####@MnnznMMMxzzznxxxnnnnxxxxMWWWWWW@#@@@@@@@@####@######@###########@@####################@######
	####WWW@W@@W@@#####WnznzznMxMnnzznzznnnxnnnnMWWW@W@@@@@@#@@@@@#@#@@@##@#@@############@###################@######
	####WWW@@@@W@@@###@Mnznz#zMxxxz+###+#zzzzznnMWMMWWW@@@@@@@@@@@@@@@@@##@@@@@###########@@###################@#####
	###@WWMW@@@W@@@@#@@Mzzz#+#nnnxnz########zzznxxxxMWW@@@@@@@@@@@@@@@@@@@#@@@@################################@#####
	###W@@WWW@@W@@@@@@@xnzz###+nnnxnz#######zznznnxnxMW@@@@W@@W@@@@@@@@@@@@@@@@#################################@####
	##W@@@@WWW@WW@@@@@Wxnz++##+#nzznzz#####znz#znnxxnxxW@@WWWWWW@@@@@@@@@@@@@#@@################################@####
	##@@@@@WWWWWWW@@@WWMxz++++++nz##zzz#####zzzzzzznnnxMW@WWWWWWWW@@@@@@@@@@@###@###############################@@###
	##W@@@@@WWW@MWWWWWWMMxz+**+##nz#+#z###+++++#####znnxMWWWWWWWWW@@WW@@@@@@@@###@@##############################W###
	#@@@@@@@@@@WWWMWWMMMxxz#*i*+#zzz#+###+++++++++++#zznxMMMWWWWWW@@WW@W@@@@@@################################@##W###
	#@#@@@@@@@@WWMMMWWWMxnn#+ii**##nz#####+++**++++++#zznxMMMWWWWMW@W@@W@@@@@@@@@################################W###
	@@@@@@@#@@@@WWWWWWWMMMMz+**ii*++znnnzz##++++****++#znnxxWWWW@WW@W@WWWW@@@@@@@################################@###
	@@@@@@@@@@@@WWWWWWMMMxxxnz+*iii*++#zznnzz#++****+++#zznnxWWW@@W@W@WWWWW@@@@@@@###############################@###
	W@@@@@@@@@@@W@WWWWWMxxzz#+**iiiii**+#znnz#+*******++##z#zxWW@@W@@@WWWWWW@@@@@@###############################@@##
	W@@@@@@@@@@WW@@@WWWxnnzn##+*ii;;ii*i*++##+***iii****+####zMW@@@@@@@WWWWW@@@@@@@@##############################W##
	W@@#@@@@@@WWW@@@WWWxzz+##++*i;;;;;ii********i*iiii***+##+#nMW@@@@@@WWWWWW@W@@@@@@#############################W##
	W@@@@@@@@@@W@@@@WWWxz#++****i;;;;;;iiiiiiiiiiiiiiii***+#++#nxW@@@@@WWWWWW@WW@@@@@#############################@##
	M@@@@@@@@@WW@@@@WWMx##+*iiiii;;;;;;;iiiiiiiiiiiiiiiii**++###xMW@@@@@WWWWMWWW@@@@@#############################@@#
	@@@@@@@@@@WW@@@@MWxn##*iiiii;;;;;;;iiiiiiiiiiiiiiiiiiii*+++#nMM@@@@@WWWWWWWWW@@@###############################@#
	@@@@@@@@@WW@@@@WWWxn#*iiii;;;;;;;;;;;;;;iiiiiiiiiii;iiii*+++#nMW@@@@WWWWWWWWW@@@@@#############################@#
	@@@@@@@@@@@@@@@WWMn#+iii;;;;;;;;;;;;;;;;;;i;;;;;;ii;;;iii*+++#nM@@@@@WWWWWWMW@@@@@@@#############################
	#@@@@@@@@@@@@@WWWMz+*ii;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iii**++##xW@@@@@@WWWWMMW@@@@@@@############################
	#W@@@@@@@@@@@@WMMx#+iii;;;;;;;;;;;:;;;;;;;;;;;;;;;;;;;iiii*++++zxW@@@@WWWWWWMMW@@@W@@@###########################
	#W@@@@@@@@@@WWMMx#+*iii;;;;;;;;;;;:;;;;;;;;;;;;;;;;;;;;iii***+++#nxW@@WWWWWWWWWWWWWW@@@########################W#
	#@@@@@@@@@@@WWMxz++iiii;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iiii***++##zzMMMWWMMWWWMMWWWWW@@@#######################@#
	#@@@@@@@@@WWWMxz++*iii;;;;;;;;;;;;:::;::;;;;;;;;;;;;;;iiii***+**+#z#znxMWWWMWWMMWWWWWW@@@######################@#
	##@@@@@@@@WWWMz#+*iiiii;;;;;;;;;::;:::::::;;;;;;;;;;;;;iiiii*****++##znnxMWWWWWMWWWWWWW@@@#@###################@#
	#@@@@@@@@@@WWx#+*iiiiiii;;;:;:;;::;:::::;:;;;;;;;;;;;;;iiiii*iii***++##zznxMMWWWWMMMMWW@@##@###################W#
	#@@@@@@@@@@W@x#+iiiiiii;;;;;;;;;;;:;:;;:;:;;;;;;;;;;;;iiiiiiiiii***++++##zznxxxxMMMMMWW@@@@####################@#
	#W@@@@@@@@@W@x+*iiiiii;;;;;;;::;;;;;;;;;;;;;;;;;;;;;;iiiiiiiiiiii****+++##zznxxnxxxMMMW@@@#####################@#
	#@W@@@@@@@@W@n*iiiiiii;;;;;;;;;;;;;;;;;:;;;;;;;;:;;;;;iiiiiiiiiiii***++++##zznnnnnnxxMMW@@#######################
	#@M@#@@#@@WWWn*iiiiii;;i;;;;;;;;;;;;;;;;;;;;;i;;;;;;;iiiiiiiiiiiii**+++++###znnnnnnnxxMW@@@######################
	#@M@@@#@@@WWWz*iiiiiiiiiii;;ii;iiii;;;;;;;;;;i;;;;i;iiiii*iii***i**++#++#####znnnnznxxMM@@#######################
	#MWW@@##@@WWWz*i*iii*******i*++++++**iiii;;;iii;;;iiii*****+++##++++#z#######znnnnnznxxMW@@###################@W#
	#xW@@###@@@WW#*iiii**i***+*+#zzznnz##++***i;;i;;;;i*i*#####zzznnz##+#z#zzzzzzznnnnnnznxxW@@##################@@W#
	#MW@@@###@@@W#i*iii*****++++#znnnnnz#####*i;;i;;;i*++##znnnnnxxnnnzzzz##zzzzzznnnnnnznxxM@@########@#####@@#@@@@#
	#WWW@@##@@@@W+i*iii*****++###znnnxnnz#z##+*;;i;;;i+zz#zznxxxMMxxnnnzzz##zzzznnnnnnnnnnxxxW@#######@@@###@@@@@#@M#
	@W@@@@####@@M*iiiii**iii*++znxMMMMxxxxz##+*;;;;;;*+znnnnnnxMMMxnzzz#######zzznnnznnnnnnxxMW@#####@@@#@@@@@@@@#@W@
	@@@@W@@@#@@@x*iiiiiiiii*+##xMMxMxxxnznz#+**i;;;;i*#nnnnnnnxxxnznzzzzz###+##zzzzzznnnnnnxxxMW@###@@@###@@@@@###@@W
	@W@@@@@@@@@@z*iiiiiiii+#+zzzn#*xMnn###+++*ii;;;;i*#nnnnnznnzznnxMxxMxnzzz###zzzzznznnnnnxxxMW@#########@@@@@@@@@W
	#@@@@@@@@@@M+iiiiiiii*##n+;+nzz@x#z+#z*i*ii;:;;;i+#nnnnnz##zn#*nz*xMMMMnznz#z####znnnnnnnxxMW@#############@@@@@@
	#W@@@@@@@@Wx*iiii;;i*+#+*i;inz#z##z++#+iiii;:;;;i+znnnnz+#zz+;;nzz@Mxnxxxznz#z###zzznnnnnnxxMW##############@@@@@
	#W@@@@@@WWWn*i*i;;;ii***ii;;+z#z##+*+*+i;ii;:::;i+znnnn##z#+i::#z#zznnnzxxnz#z####zzznnnnxxxMM##############@@@@@
	#WWW@@@WWWWz*iii;;;ii*iii;;;;i*******i;iii;;:::;i+znnnz*z+++*i;izzzzzz###nnz#######zzznnnxxxMW@##############@@@@
	#WWWWWMMMWMz*ii;;;;;;;;;;;;;;;iiii***iiiii;;;::;i+znnnz+*iiii;ii*+###+++#nnz#########zznnnxxMW@################@#
	#WWWWWnnMMxz*ii;;;;;;;;;;;;;;;;;i***ii**ii;;;;;;i+#nnzz#+***iiiii*****+##z##++++++###zzznnxxMW@###@#@@@@@@@@@@WW#
	#@MWWM#nMMxn*ii;;;;;;;;;;;;;;;iii*iii*iiii;;;;;i*+#nnzz#+****ii;iiii**+###+****++++##zzznnxxMW@##@@@@@@WWWW@@WWM#
	##WWWM*#nnxn*i;;;;;;;;;;;;;;;;;;;iiiiiiiii;;;;;i*+#zzzzz#*****iiii**+++##+*****++++##zznnnxxM@@#@@@@@WWWMMMWWWW@#
	##@MWM+#nnnn*ii;;;:::;;;;;;;;::;;;;;;iiiii;;;;ii*+zzzzz##+*iiiiiii***++++*********++#zznnnxxMW@#@@WWMMMMWMxMWWW@#
	###WWW#+znnn+i;;;;;:;;;;;::::::::;;;;;;iii;;;;ii*+zzzzz##+i*iiiiiiii**************++#zznnxxxMW@@@@WWMMMMMMnx@WW##
	###WWWzi#znn+ii;;;;;;:;;::::::::::;;:;;;ii;;;;i**#zzzzz##+****iii;iiiii******i****++#zznnnxxMW@@@@WWWMMMMMnn@#@##
	##@W@Wni##zn+ii;;;:;;::::::;;:::::::;;;ii;;;;;ii+#zzzzzz#+**iiii;;;;;ii*******i***++#znnnnxxMW@#@WWWWMMMMMnn@@@##
	##@@@@M*++zx#iiii;;:::::;::::::;::::;;;;;;;;;;ii+#zzzzz##++*iiiii;;;iiiii********+++#znnnnxxMW@#@@WWMMMMMxxz@W###
	###W@@W+*i*x#iii;;:::::;;;::::::::::;;;;;;;;;;;i*#zzzzz#z+**iiii;;;iiiiiiiii*****+++#znnnnxxMW@#@@WMMxxMMxxz@@###
	###WWWW*i;inz*i;;;::::::::::::::::::;;;;;;;;;;i*+#zzzzzz#+*iii;i;;iiiiiiiiiiii***++##znnnnxxMW@@@@WxnnxxMxnzW####
	####W@@+;:ixz*ii;;;:;:::::::::::::::;;;;;;;;;ii*+#zzzzz##+*iii;;ii;iiiiiiiii**i**++##znxnnxxMW@@@@WMnznnxxnzM####
	#####M@+;:ixn*ii;;;:;:::::::::::::::;;;;;;;;;;i*+#zzzzz##+*iii;;;;;iiiiiiiiii****++##znxxxxxMW@@@@WxnnnnxMnzW####
	#####@W+;:;#x+ii;;;:;:;::::::::::::;;;;;;;;;;;;*+##zzzzz#+*iii;;;;;iiiii**iii***+++#znnxxxxxMW@WWWWxnzznxMzz@####
	#######+;::in+ii;;;:;;;:::::::::::;;;;;;;;;;;;i**+#zzzzz#+*i;;;:;;;;;iiii*ii****++##znnxxxxMW@@MxWWMnzzznxzx#####
	#######+;::;*#ii;;;:::::::::::::::;;;;;;;;;;;;i**+#zznzz#+*i;;;:;;;;;iiiiii****+++#zznnxxxxMWW@MMWWMnzzznxzM#####
	#######*;::ii*ii;;;;:::::::::::::::;;;;;;;;;;;ii*+#znnnz#+*i;;;:;;;;;;iiiiii****++#zznnxxxMWW@@MMMWMxz##zxzW#####
	#######*;:;i*i*ii;;;:::::::::::::::;;;;;;;;;;;ii*+#znnnnz+*i;;;;::;;;;;iiiii***++##zznnxxxMW@@WxMMWMxnzznnzW#####
	#######+;::i***ii;;;;:::::::::::::;;;;;;;;:;;;i**+#znzznz#*i;;;:::;;;;;iiii***+++##zznnxxxMW@WxMMMMMxnzznnz@#####
	########;;:;ii*ii;;;;::::::::::::::;;;;;;:::;;**++#zz##znz*i;;;:::;;;;;iiii***++##zznnnxxxxMWxxMMMMMxnzznzn######
	#######n;;:;;;*ii;;;;::::::::::::::;;;;;::,::;i*++##z#+#zn+ii;;::::;;;;iiii***+###zzznnxxxxMMxxMMMMMxz#znzx######
	#######x;;::;:iii;;;;:::::::;;::::::;;;;:::::;i*+++###+##z#*i;;;;:;;;;;iiii**++###zzznxxxxxMxxMMMMMMxz#znz@######
	#######Wi;;:::*iii;;;::::::::::;;:::;;;;;:::;i**++##+++##zz+i;;;;;;;;;;iiii**++#z#zznnxxxxMMxnMMMMMxnzznzn@######
	#######@*;;;::*iii;;;:::::::::;;::::;i*i;;;:;i*+###++++##nn+ii;;;;;;;;iiiii*++#####znnnxxxxMxxxxxxxnznxzzx#######
	########z;;:::iiii;;;;:::::::::::::::;**ii;;i*+##znxnz##znz+*i;;;;;;;;iiii**++####zznnnnxxMMxxnnnnxxnxnzzM#######
	########M;:;::iiii;;;;;;:::::;:::::::;iiiiii*+#zzznnnzzznn#+*i;;;;;;;;iiii***+####znnnnxxxxMxxnznnnxxnzzn@#######
	########@i;;;:iiii;;;;;;::::::;;::;::;;;;iii*+#zzzzzzznnz#++*i;;;;;;;iii****+#+###znnnxnxxxMxxxnzzznnzzzM########
	#########+;;;:iiii;;;;;;;:::::;;:::;;;;i;;ii*++###zzzzzz#+****ii;;;;iiii***++#++##zznnxxxxxMxxxnnzznzzzn#########
	#########Mi;::**ii;;;;;;;:;;:;;;;;;;;;;;;;;i***++#######+***i*ii;;;;iiii***++#++##zznnxxxxxMxnnnzzz#zzn@#########
	##########n;;;i*iii;;;;;:;;;;;;;;;;;;;;;i;;;iiii*++####++**ii***ii;iiiii**++##++##zznnnnxxxMxnnzzzzzznW##########
	############;iiiiiii;;;;;;::;;;;i;;;i;;;;;;;iii;i***+++++******+*iiiiiii**++##++##zznnnxxxxMxnzzzzzzzW###########
	###########@+;iiiii;;;;;;;:;;i;;;;;iii;;;;;;iii;;ii**+++++******+*i;ii*i*++###++##zznnnnxxxMxnzz##zzM############
	#############@ziiiiii;;;;;:;ii;;i;i;i;;;;;;;ii;;;ii**+##++++***+++*iii***++###+++#zznnnxxxMWMnz##zzx#############
	##############xiiiiii;;;;;:;ii;;;;;;;;;;;;:;;;i;;;ii**+##++++*+++***ii****+###++##zznnnxxxx@@xzzzxWW#############
	##############Wiiiiii;;;;;;;iii;i;;;;;;::::;;;;;;;iii**++##+++++***iiii***+#zz#+##zznnnxxxM@##@W@@@@#############
	##############@*iiii;;;;;;;;;iiii;;;;::::::;;:::;;;ii**++####+++**iiiii***+#zz#+##zzznnnxxM#######W@#############
	################i*iiii;;;;;;;ii*+*iiiiiiii*i;;;;;;;ii***+++####+*iii*ii*+++#zz++##zznnnnxxM######@@##############
	###############xiiiiiii;;;;;;;;;i+#zz########++++#+++++++##znxn#*iiiii**++##z#++#zznzzznxx@#####W@###############
	###############Wiiii;ii;;;;;;;;;;;*++*++###zzzzzzzzznnxxMxxxn#+*ii;iii*++##zz##+#zzzzznnxx@###@W@################
	################+iii;iii;;;;;;;;;;;ii*i**i*******+*++##zzz#+++*ii;;iii*+###zz#+##zznzznnxM##@@W@#################
	################xiii;;iii;;:;;;;;;;;;iiiii;;;;;;i***++##++++++*iiiiii**+#zzzz++#zzznzznnxMW@@@###################
	################@*ii;;;ii;;;;;;;;;;;:;;;i;;;;;;;ii***+**+++#+**iiiiiii*+#znz#++#zzzzzznxxM#######################
	#################niiii;ii;;;;;;;;;;;;:::;;;;;;;;;ii*****+##+******i*i*+#zzz##++#zzzzzznxx########################
	#################@*iii;;ii;;;;i;;;;;;;;;:::;;;;;;;iii**+++++**********+#zzz#++##zzzzznxx@########################
	##################xii;;;ii;;;;i;;;;;;;;;;;;;;;;iiii**+++++***+********##zz##++#zznzzznxM#########################
	##################@*i;i;;iiii;;;;;;;;;;iiiiiii******+++*******+******+##zz##+##zzzzznxM##########################
	###################xii;;iiiii;;;;;;;;;;;iii****+*************+++***++##zz#++++#zzznnnx@##########################
	####################nii;iiiii;;;;;;;;;;;;;ii***ii**i**ii***+*+*+++*++#zz##+++#zzznnnx@###########################
	####################@*i;;iiii;;;;ii;;;;;;iiiiiiii*iiiiii***++++++**+##zz#+++#zznnnnxW############################
	#####################xii;;;iii;;;;ii;;;;;;iiiiiiiiiiiiiii**++++#+**+#####++##zznnxxM#############################
	#######################ii;;;i;;;;;i;;;;;;;;i;;i;iiiiiiii***++++#+**+#z##+####znnxxM##############################
	######################@*i;iii;;;;;i;;;;;;;;;;;;;;iiiiiiii****+++++++zz#+###zznnxxM###############################
	#######################x*iiii;;;;;;;;;;;;;;;;;;;;iiii;i***i**++++++#zz###z#zznxMM@###############################
	########################n*ii;;;;;;;;;;;;;;;;;;;;;iiiiii******+++++##zz###zznnxxM@################################
	#########################ziiii;;;;;;;;;;;:;;;;;;;;;;iiii***+*+++++##zz#zznnnxxx@#################################
	###########################ii;;;;;;;;;;::::;;:;;;;iiiii******++++##zzzzzznnxxM@##################################
	##########################@+iii;::;:::::::::::;;;i;iiii***+*++++##zzzzzznnxxM@###################################
	###########################@+ii;::;;;:::::::;:;;;;;;iiii****++++#zzzznnnnxxM#####################################
	############################M*i;;::::::::::;;;;;;;;;ii******+++##zznnnnnxxM######################################
	#############################M*i;:::::::::;;;;;;;;;;;i******++zzznnnnnnxxW#######################################
	##############################Mii;;::::::;;;i;;;;;;;;iiii*+*+##znnnnnnxM@########################################
	###############################Mi;;;::;;;;;;;i;;;;;;ii*ii**++#zznnnnnx@##########################################
	################################Mi;;;;:;;;;;;;;;;;;;ii****++#zznnnnxW############################################
	#################################zi;;;;;i;;;i;;;;;iiiii**+##zznnnM@##############################################
	##################################x*i;iiiiiiiiiiiiii***+++#zznMW#################################################
	###################################@z***i******i***++++###znW####################################################
	#####################################@WMxxxxnz#++++zznxxMM@######################################################

	It just works.

*/