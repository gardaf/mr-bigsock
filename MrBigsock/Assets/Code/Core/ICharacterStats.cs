using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock {

	/*
		Interface represents the stats of a character.
	*/
	public interface ICharacterStats {
		/*
			The max hp of the character.
		*/
		float MaxHP { get; }

		/*
			The max mana of the character.
		*/
		float MaxMana { get; }

		/*
			The max stamina of the character.
		*/
		float MaxStamina { get; }

		/*
			The regeneration rate of the character's mana.
		*/
		float RegenMana { get; }

		/*
			The regeneration rate of the character's stamina.
		*/
		float RegenStamina { get; }


		/*
			The damage of the character.
		*/
		float Damage { get; }

		/*
			The movement speed of the character.
		*/
		float MoveSpeed { get; }

		/*
			The knockback of the character.
		*/
		float Knockback { get; }

		/*
			The range of the character.
		*/
		float Range { get; }

		/*
			The attack speed of the character.
		*/
		float AttackSpeed { get; }

		/*
			The crit chance of the character.
		*/
		float CritChance { get; }
		
		/*
			The how much to modify damage by when critting.
		*/
		float CritDamageModifier { get; }
		
		/*
			The projectile speed of the attack.
		*/
		float ProjectileSpeed { get; }
		
		/*
			How much the projectile will stray off course.
		*/
		float Accuracy { get; }


		/*
			Returns text representing the modifications it does in percent.
		*/
		string GetToolTip();

	}

	/*
		Holds extension methods for character stat objects.
	*/
	public static class CharacterStatsExtension {
		/*
			Identity object for character stats.
		*/
		public static readonly ICharacterStats IDENTITY = new CharacterStats{
			MaxHP = 1,
			MaxMana = 1,
			MaxStamina = 1,
			RegenMana = 1,
			RegenStamina = 1,

			Damage = 1,
			MoveSpeed = 1,
			Knockback = 1,
			Range = 1,
			AttackSpeed = 1,
			CritChance = 1,
			CritDamageModifier = 1,
			ProjectileSpeed = 1,
			Accuracy = 1,
		};

		/*
			Adds the values of 2 character stats together.
		*/
		public static ICharacterStats Add(this ICharacterStats a, ICharacterStats b) {
			return new CharacterStats{
				MaxHP = a.MaxHP + b.MaxHP,
				MaxMana = a.MaxMana + b.MaxMana,
				MaxStamina = a.MaxStamina + b.MaxStamina,
				RegenMana = a.RegenMana + b.RegenMana,
				RegenStamina = a.RegenStamina + b.RegenStamina,

				Damage = a.Damage + b.Damage,
				MoveSpeed = a.MoveSpeed + b.MoveSpeed,
				Knockback = a.Knockback + b.Knockback,
				Range = a.Range + b.Range,
				AttackSpeed = a.AttackSpeed + b.AttackSpeed,
				CritChance = a.CritChance + b.CritChance,
				CritDamageModifier = a.CritDamageModifier + b.CritDamageModifier,
				ProjectileSpeed = a.ProjectileSpeed + b.ProjectileSpeed,
				Accuracy = a.Accuracy + b.Accuracy,
			};
		}
		
		/*
			Removes the values of one stat object from the other.
			Effectively a - b.
		*/
		public static ICharacterStats Remove(this ICharacterStats a, ICharacterStats b) {
			return new CharacterStats{
				MaxHP = a.MaxHP - b.MaxHP,
				MaxMana = a.MaxMana - b.MaxMana,
				MaxStamina = a.MaxStamina - b.MaxStamina,
				RegenMana = a.RegenMana - b.RegenMana,
				RegenStamina = a.RegenStamina - b.RegenStamina,

				Damage = a.Damage - b.Damage,
				MoveSpeed = a.MoveSpeed - b.MoveSpeed,
				Knockback = a.Knockback - b.Knockback,
				Range = a.Range - b.Range,
				AttackSpeed = a.AttackSpeed - b.AttackSpeed,
				CritChance = a.CritChance - b.CritChance,
				CritDamageModifier = a.CritDamageModifier - b.CritDamageModifier,
				ProjectileSpeed = a.ProjectileSpeed - b.ProjectileSpeed,
				Accuracy = a.Accuracy - b.Accuracy,
			};
		}

		/*
			Multiplies the values of 2 character stats together.
		*/
		public static ICharacterStats Multiply(this ICharacterStats a, ICharacterStats b) {
			return new CharacterStats{
				MaxHP = a.MaxHP * b.MaxHP,
				MaxMana = a.MaxMana * b.MaxMana,
				MaxStamina = a.MaxStamina * b.MaxStamina,
				RegenMana = a.RegenMana * b.RegenMana,
				RegenStamina = a.RegenStamina * b.RegenStamina,

				Damage = a.Damage * b.Damage,
				MoveSpeed = a.MoveSpeed * b.MoveSpeed,
				Knockback = a.Knockback * b.Knockback,
				Range = a.Range * b.Range,
				AttackSpeed = a.AttackSpeed * b.AttackSpeed,
				CritChance = a.CritChance * b.CritChance,
				CritDamageModifier = a.CritDamageModifier * b.CritDamageModifier,
				ProjectileSpeed = a.ProjectileSpeed * b.ProjectileSpeed,
				Accuracy = a.Accuracy * b.Accuracy,

			};
		}

		
		/*
			Modifies the first stat object by the second one.
			Ex.: if the second is 20% hp, the result will be the first one, but with 20% more hp.
		*/
		public static ICharacterStats Modify(this ICharacterStats a, ICharacterStats b) {
			return a.Multiply(b.Add(IDENTITY));
		}

	}
}