using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEngine.InputSystem;


namespace BigSock {

	/*
		class represents the stats of a character.
	*/
	public class CharacterStats : ICharacterStats {
		/*
			The hp of the character.
		*/
		public float MaxHP { get; set; }



		/*
			The max mana of the character.
		*/
		public float MaxMana { get; set; }

		/*
			The regeneration rate of the character's mana.
		*/
		public float RegenMana { get; set; }



		/*
			The max stamina of the character.
		*/
		public float MaxStamina { get; set; }

		/*
			The regeneration rate of the character's stamina.
		*/
		public float RegenStamina { get; set; }



		/*
			The damage of the character.
		*/
		public float Damage { get; set; }

		/*
			The knockback of the character.
		*/
		public float Knockback { get; set; }



		/*
			The movement speed of the character.
		*/
		public float MoveSpeed { get; set; }

		/*
			The attack speed of the character.
		*/
		public float AttackSpeed { get; set; }



		/*
			The crit chance of the character.
		*/
		public float CritChance { get; set; }
		
		/*
			The how much to modify damage by when critting.
		*/
		public float CritDamageModifier { get; set; }
		
		

		/*
			The projectile speed of the attack.
		*/
		public float ProjectileSpeed { get; set; }

		/*
			The range of the character.
		*/
		public float Range { get; set; }

		/*
			The range of the character.
		*/
		public float Accuracy { get; set; }


		
		/*
			Returns text representing the modifications it does in percent.
		*/
		public string GetToolTip() {
			var sb = new StringBuilder();
			if(MaxHP != 0)              sb.AppendLine($"Max HP: {MaxHP:P0}");

			if(MaxMana != 0)            sb.AppendLine($"Max Mana: {MaxMana:P0}");
			if(RegenMana != 0)          sb.AppendLine($"Regen Mana: {RegenMana:P0}");

			if(MaxStamina != 0)         sb.AppendLine($"Max Stamina: {MaxStamina:P0}");
			if(RegenStamina != 0)       sb.AppendLine($"Regen Stamina: {RegenStamina:P0}");

			if(Damage != 0)             sb.AppendLine($"Damage: {Damage:P0}");
			if(Knockback != 0)          sb.AppendLine($"Knockback: {Knockback:P0}");

			if(Range != 0)              sb.AppendLine($"Range: {Range:P0}");
			if(ProjectileSpeed != 0)    sb.AppendLine($"Projectile Speed: {ProjectileSpeed:P0}");
			if(Accuracy != 0)           sb.AppendLine($"Accuracy: {Accuracy:P0}");

			if(MoveSpeed != 0)          sb.AppendLine($"Move Speed: {MoveSpeed:P0}");
			if(AttackSpeed != 0)        sb.AppendLine($"Attack Speed: {AttackSpeed:P0}");

			if(CritChance != 0)         sb.AppendLine($"Crit Chance: {CritChance:P0}");
			if(CritDamageModifier != 0) sb.AppendLine($"Crit Damage Modifier: {CritDamageModifier:P0}");
			
			return sb.ToString();
		}
	}
}