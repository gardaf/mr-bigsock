using System.Collections.Generic;
using BigSock.Interact;
using Cinemachine;
using UnityEngine;

namespace Bigsock {
	public class FollowPlayer : MonoBehaviour {
		[SerializeField] NeighbourMapGenerator neighborMapGenerator;
		[SerializeField] CinemachineConfiner2D cameraMap;
		[SerializeField] GameObject victoryScreen;

		//Unity varibles
		private GameObject[] boss;
		private GameObject player;
		private GameObject[] enemies;
		private GameObject[] Chest;
		private List<GameObject> chests;
		private GameObject stair;

		//Normal variables
		private int levels = 0;
		private static int LEVEL_CAP = 2;
		private int roomNr = 0;
		private bool stairs_down;
		private int victoryHold = 0;

		void Start() {
			chests = new List<GameObject>();
			player = GameObject.Find("BigSock");
			InitializeLevel();
		}

		void LateUpdate() {
			boss = GameObject.FindGameObjectsWithTag("Boss");
			enemies = GameObject.FindGameObjectsWithTag((roomNr).ToString());

			if (boss.Length == 0 && levels < LEVEL_CAP) {
				if (!stair.activeInHierarchy) {
					levels++;
				}
				stair.SetActive(true);
				stairs_down = stair.GetComponent<Stairs>().stairs_touch;

			}
			if (levels == LEVEL_CAP && victoryHold == 0) {
				VictoryScreen();
				victoryHold++;
			}

			if (stairs_down) {
				ClearScene();
				player.transform.position = new Vector3(9, 5, 0);
				InitializeLevel();
			}

			cameraMap.InvalidateCache();

			if (enemies.Length == 0) {
				chests[roomNr].SetActive(true);
				if (roomNr + 1 < NeighbourMapGenerator.GetRoomListCount() - 1) {
					roomNr++;
				}
			}
			if (boss.Length == 0) {
				chests[1 + roomNr].SetActive(true);
			}
		}
		private void OnDestroy() {
			ClearScene();
		}

		/*
		*Deletes all game objects from the scene that is not the player
		*and also resets all arrays that have been made*/
		private void ClearScene() {
			roomNr = 0;
			foreach (GameObject c in chests) {
				DestroyImmediate(c);
			}
			chests.Clear();
			DestroyImmediate(stair);
			NeighbourMapGenerator.ClearRoomList();
			TilemapGenerator.SetRoomIDZero();
			TilemapGenerator.resetMaps();
		}

		/*
		*Initializes the victory screen*/
		private void VictoryScreen() {
			Time.timeScale = 0;
			GameObject canvas = GameObject.Find("Canvas");
			if (canvas != null) {
				Instantiate(victoryScreen, canvas.transform);
			}
		}

		/*
		 *Creates a level and adds objects to the different arrays */
		private void InitializeLevel() {
			neighborMapGenerator.RunProceduralGeneration();
			Chest = GameObject.FindGameObjectsWithTag("Chest");
			stair = GameObject.Find("Stairs(Clone)");
			stairs_down = stair.GetComponent<Stairs>().stairs_touch;
			foreach (GameObject c in Chest) {
				if (c != null) {
					c.SetActive(false);
					chests.Add(c);
				}
			}
			stairs_down = stair.GetComponent<Stairs>().stairs_touch;
			stair.SetActive(false);
		}
	}
}
