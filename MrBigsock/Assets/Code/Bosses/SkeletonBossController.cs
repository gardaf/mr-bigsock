using UnityEngine;
using System;

namespace BigSock {
	public partial class SkeletonBossController : EnemyController {

		// Creates emty collider which will be used for charge
		protected EmptyCollider chargeCollider;
		// SkeletonBossState, default value set to idle
		public SkeletonBossState State { get; protected set; } = SkeletonBossState.Idle;

		// Checks if skeleton boss is in charge
		protected bool isInCharge = false;

		// Creates datetime for the cooldowns for skeletonboss
		protected DateTime timer = DateTime.Now;
		protected DateTime nextChargeTime = DateTime.Now;

		// Cooldowns for the charging
		protected static readonly TimeSpan CHARGE_COOLDOWN = new TimeSpan(0, 0, 0, 5, 0);
		protected static readonly TimeSpan CHARGE_WAIT_TIME = new TimeSpan(0, 0, 0, 0, 25);
		protected static readonly TimeSpan SECOND_CHARGE_WAIT_TIME = new TimeSpan(0, 0, 0, 0, 800);
		protected static readonly TimeSpan FINISH_CHARGE_WAIT_TIME = new TimeSpan(0, 0, 0, 0, 450);

		// Sets the leapforce of the skeleton boss
		public double LeapForce => MovementSpeed * 8;


		protected override void Start() {
			base.Start();
			// Sets the chargeCollider
			chargeCollider = transform.Find("ChargeCollider").GetComponent<EmptyCollider>();
			chargeCollider.OnColliderEnter2D_Action += Charge_OnColliderEnter2D;
			chargeCollider.OnColliderStay2D_Action += Charge_OnColliderStay2D;
			chargeCollider.OnColliderExit2D_Action += Charge_OnColliderExit2D;
		}


		protected override void Update() {
			Regenerate();

			// Checks if player is within range, and state is idle or walking, then moves against player
			if (target != null && (State == SkeletonBossState.Idle || State == SkeletonBossState.Walking)) {
				// Checks if player is in melee or not
				if(!isInMelee){
					var movement = (new Vector2(target.position.x, target.position.y) - rb.position).normalized;
					TryMove(movement);
					RotateAnimation(movement);
				}
			// Checks if skeletonboss is in charge, and the cooldown is done
			} else if (State == SkeletonBossState.Charge && DateTime.Now >= timer) {
				var pos = target.position;
				var temp = (pos - rb.transform.position);
				temp.z = 0;
				var direction = temp.normalized;
				// Calls the charge method
				Charge(direction, LeapForce);
				timer = DateTime.Now + SECOND_CHARGE_WAIT_TIME;
				State = SkeletonBossState.SecondCharge;
			} else if (State == SkeletonBossState.SecondCharge && DateTime.Now >= timer) {
				var pos = target.position;
				var temp = (pos - rb.transform.position);
				temp.z = 0;
				var direction = temp.normalized;
				// Calls the charge method
				Charge(direction, LeapForce * 2);
				timer = DateTime.Now + FINISH_CHARGE_WAIT_TIME;
				State = SkeletonBossState.FinishAttack;
			// Finishes the attack and sets state and animation back to walking
			} else if (State == SkeletonBossState.FinishAttack && DateTime.Now >= timer) {
				State = SkeletonBossState.Walking;
				m_Animator.SetTrigger("walk");
			}
		}

		// Charge method, takes the direction of charge, and the leapforce
		protected virtual bool Charge(Vector2 direction, double leapForce) {
			// Checks if direction is valid
			if (direction != Vector2.zero) {
				// Uses physics to add impulse to the boss in the given direction
				rb.AddForce((float)leapForce * direction, ForceMode2D.Impulse);
				return true;
			}
			return false;
		}


	}

	/*
			Attack
	*/
	public partial class SkeletonBossController {

		// Charge on collider enter
		protected virtual void Charge_OnColliderEnter2D(Collider2D other) {
			if (other.gameObject.tag == "Player") isInCharge = !isInCharge;
		}

		// Charge on collider stay
		protected virtual void Charge_OnColliderStay2D(Collider2D other) {
			// Gets the player gameobject
			var player = other.gameObject.GetComponent<PlayerController>();
			if (player != null) {
				// Checks if charge cooldown is done
				if (DateTime.Now >= nextChargeTime) {
					// starts the attack animation
					m_Animator.SetTrigger("attack");
					timer = DateTime.Now + CHARGE_WAIT_TIME;
					State = SkeletonBossState.Charge;
					// Sets the cooldown for next charge
					nextChargeTime = DateTime.Now + CHARGE_COOLDOWN;
				}
			}
		}

		// Charge on collider exit
		protected virtual void Charge_OnColliderExit2D(Collider2D other) {
			if (other.gameObject.tag == "Player") isInCharge = !isInCharge;
		}

	}


	public partial class SkeletonBossController {

		// Move on collider enter
		protected override void Move_OnColliderEnter2D(Collider2D other) {
			// Checks if the player is in the movecollider, and if the boss state is not in any attack
			if (other.gameObject.tag == "Player" && State != SkeletonBossState.Charge && State != SkeletonBossState.SecondCharge && State != SkeletonBossState.FinishAttack) {
				// Sets the animation to walk
				m_Animator.SetTrigger("walk");
				target = other.transform;
			}
		}

		// Move on collider exit
		protected override void Move_OnColliderExit2D(Collider2D other) {
			// Checks if the player exited the collider
			if (other.gameObject.tag == "Player") {
				// Sets animation to idle
				m_Animator.SetTrigger("idle");
				target = other.transform;
				target = null;
			}
		}

	}

	public partial class SkeletonBossController {


	}

	// The skeletonbosses states
	public enum SkeletonBossState {
		Idle, Walking, Attacking, Charge, SecondCharge, FinishAttack, Death
	}
}

