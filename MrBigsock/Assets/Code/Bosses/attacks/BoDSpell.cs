using System;
using UnityEngine;




namespace BigSock {
	public class BoDSpell : MonoBehaviour {

		// The duration of the spell
		protected static readonly TimeSpan DURATION = new TimeSpan(0, 0, 0, 1, 500);

		// datetimeobject for checking when the object is to be destroyed
		protected DateTime destroyObject = DateTime.Now + DURATION;

		private void Update() {
			// Checks if the duration of the spell is out
			if (DateTime.Now > destroyObject) {
				// Destroys the spell object
				Destroy(this.gameObject);
			}
		}
	}
}

