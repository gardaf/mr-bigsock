using UnityEngine;
using System;

using BigSock.Service;

namespace BigSock {

	public partial class BringerOfDeathController : EnemyController {

		// Cast spell timers
		protected static readonly TimeSpan SPELL_CAST_COOLDOWN = new TimeSpan(0, 0, 0, 4, 0);
		protected static readonly TimeSpan CAST_START = new TimeSpan(0, 0, 0, 0, 500);
		protected static readonly TimeSpan CAST_LOCATE = new TimeSpan(0, 0, 0, 0, 150);
		protected static readonly TimeSpan CAST_FINISH = new TimeSpan(0, 0, 0, 0, 750);

		// Location for where spell should be cast
		protected Vector3 spellLocation;

		// Creates datetime objects for the casting of spells
		protected DateTime casting_time = DateTime.Now;
		protected DateTime spell_cooldown = DateTime.Now + SPELL_CAST_COOLDOWN;

		// Teleport cooldowns
		protected static readonly TimeSpan TELEPORT_COOLDOWN = new TimeSpan(0, 0, 0, 6, 0);
		protected static readonly TimeSpan TELEPORT_START = new TimeSpan(0, 0, 0, 0, 500);
		protected static readonly TimeSpan TELEPORT_LOCATE = new TimeSpan(0, 0, 0, 0, 50);
		protected static readonly TimeSpan TELEPORT_FINISH = new TimeSpan(0, 0, 0, 0, 850);

		// Location for where to teleport
		protected Vector3 teleportLocation;

		// Creates datetime objects for the teleporting
		protected DateTime teleport_time = DateTime.Now;
		protected DateTime teleport_cooldown = DateTime.Now + TELEPORT_COOLDOWN;

		// BoDstate, set to idle as default
		public BoDState State { get; protected set; } = BoDState.Idle;

		// The collider for the spell
		protected EmptyCollider spellCollider;

		// Spell location in files
		public const string PROJECTILE_NAME = "Bosses/Attacks/Bringer_of_Death_Spell";

		// Decides the bosses actions
		protected override void Update() {
			Regenerate();
			// Checks if spell is off cooldown, and baseMaxHP is above 65% or below 30%
			// BoD is then forced into the next four if statements which does the casting of the abilities at the correct time cooldowns
			if (target != null && DateTime.Now > spell_cooldown && (this.baseHP >= baseMaxHP * 0.65 || this.baseHP < baseMaxHP * 0.3)) {
				casting_time = DateTime.Now + CAST_START;
				State = BoDState.CastLocate;
				// Starts casting animation
				m_Animator.SetTrigger("cast");
				spell_cooldown = DateTime.Now + SPELL_CAST_COOLDOWN;
			} else if (target != null && State == BoDState.CastLocate && DateTime.Now > casting_time) {
				casting_time = DateTime.Now + CAST_LOCATE;
				State = BoDState.Casting;
				// sets spell location
				spellLocation = target.transform.position;
			} else if (State == BoDState.Casting && DateTime.Now > casting_time) {
				casting_time = DateTime.Now + CAST_FINISH;
				State = BoDState.CastFinish;
				// Calls the cast spell method
				CastSpell(spellLocation);
			} else if (State == BoDState.CastFinish && DateTime.Now > casting_time) {
				// Casting finished, and animation and state set back to walk
				m_Animator.SetTrigger("walk");
				State = BoDState.Walk;

			// Checks if teleport is off cooldown, and baseMaxHP is below 65%
			// BoD is then forced into the next four if statements which does the teleportation of BoD
			} else if (target != null && DateTime.Now > teleport_cooldown && this.baseHP <= baseMaxHP * 0.65) {
				teleport_time = DateTime.Now + TELEPORT_START;
				State = BoDState.TeleportLocate;
				// Using death animation as teleport animation
				m_Animator.SetTrigger("death");
				teleport_cooldown = DateTime.Now + TELEPORT_COOLDOWN;
			} else if (target != null && State == BoDState.TeleportLocate && DateTime.Now > teleport_time) {
				teleport_time = DateTime.Now + TELEPORT_LOCATE;
				State = BoDState.Teleporting;
				// Sets teleport location
				teleportLocation = target.transform.position;
			} else if (State == BoDState.Teleporting && DateTime.Now > teleport_time) {
				teleport_time = DateTime.Now + TELEPORT_FINISH;
				State = BoDState.TeleportFinish;
				// Calls the teleport method
				TeleportAttack(teleportLocation);
			} else if (State == BoDState.TeleportFinish && DateTime.Now > teleport_time) {
				m_Animator.SetTrigger("walk");
				State = BoDState.Walk;

			// If BigSock is within range and the state is either idle or walk, BoD starts moving against the player
			} else if (target != null && (State == BoDState.Walk || State == BoDState.Idle)) {
				var movement = (new Vector2(target.position.x, target.position.y) - rb.position).normalized;
				TryMove(movement);
				RotateAnimation(-movement);
			}
		}
	}

	public partial class BringerOfDeathController : EnemyController {
		
		// Casts the spell, with spellLocation set
		protected void CastSpell(Vector3 spellLocation) {
			// Creates instance of the spell at the given location
			var spell = PrefabService.SINGLETON.Instance(PROJECTILE_NAME, spellLocation);
			// Gets the collider for the spell
			spellCollider = spell.transform.GetChild(0).GetComponent<EmptyCollider>();
			// Sets the collider for the spell to have Spell_onColliderEnter2D
			spellCollider.OnColliderEnter2D_Action += Spell_onColliderEnter2D;
		}

		// Collider for the spell cast by BoD
		protected void Spell_onColliderEnter2D(Collider2D other) {
			// Gets player object
			var player = other.gameObject.GetComponent<PlayerController>();
			if (player != null) {
				// Create attack object.
				var attack = (AttackStats)new AttackStats {
					Damage = 1,
					Knockback = 0,
					Range = 0,
					AttackSpeed = 0,
					Source = transform.position,
					Actor = this,
				}.Calculate(Stats);

				// Gets the player to take damage
				if (player.TakeDamage(attack)) {
				}
			}
		}

		// The teleport attack method
		protected void TeleportAttack(Vector3 teleportLocation) {
			// Checks which direction BoD teleported, and faces the direction he teleported to
			if (transform.position.x > teleportLocation.x) {
				transform.position = teleportLocation + new Vector3(2, 0, 0);
			} else {
				transform.position = teleportLocation + new Vector3(-2, 0, 0);
			}
			// Plays the attack animation
			m_Animator.SetTrigger("attack");
		}
	}

	// States for BoD
	public enum BoDState {
		Idle, Walk, Cast, Casting, CastLocate, CastFinish, Teleport, Teleporting, TeleportLocate, TeleportFinish, Attack
	}
}