using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace BigSock {

	public class DestroyObject : MonoBehaviour {

		private void OnCollisionEnter2D(Collision2D collision) {
			Destroy(this.gameObject);
		}

		// Start is called before the first frame update
		void Start() {

		}

		// Update is called once per frame
		void Update() {
		}
	}
}
