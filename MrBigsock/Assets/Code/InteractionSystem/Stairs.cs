using Bigsock;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace BigSock.Interact {
	public class Stairs : MonoBehaviour, IInteractable {
		[SerializeField] private string _prompt;

		public string InteractionPrompt => _prompt;
		public bool stairs_touch = false;

		public bool Interact(Interactor interactor) {
			if (!stairs_touch) {
				stairs_touch = true;
				return true;
			}
			return false;
		}
	}
}