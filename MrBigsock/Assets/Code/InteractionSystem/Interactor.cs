using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace BigSock.Interact {
	public class Interactor : MonoBehaviour {
		[SerializeField] private Transform _interactionPoint;
		[SerializeField] private float _interactionpointRadius = 0.5f;
		[SerializeField] private LayerMask _interactableMask;

		private readonly Collider2D[] _colliders = new Collider2D[3];
		[SerializeField] private int _numFound;

		private void Update() {
			_numFound = Physics2D.OverlapCircleNonAlloc(_interactionPoint.position, _interactionpointRadius, _colliders, _interactableMask);

			if (_numFound > 0) {
				var interactable = _colliders[0].GetComponent<IInteractable>();

				if (interactable != null && Keyboard.current.eKey.wasPressedThisFrame) {
					interactable.Interact(this);
				}
			}
		}

		private void OnDrawGizmos() {
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(_interactionPoint.position, _interactionpointRadius);
		}
	}
}