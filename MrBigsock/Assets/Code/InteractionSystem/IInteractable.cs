using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigSock.Interact {
	public interface IInteractable {
		public string InteractionPrompt { get; }
		public bool Interact(Interactor interactor) {
			throw new System.NotImplementedException();
		}

	}
}
