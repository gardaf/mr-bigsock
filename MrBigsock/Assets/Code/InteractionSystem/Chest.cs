using System.Collections.Generic;
using UnityEngine;

using BigSock.UI;
using BigSock.Service;
using BigSock.Item;

namespace BigSock.Interact {
	public class Chest : MonoBehaviour, IInteractable {
		[SerializeField] private string _prompt;
		[SerializeField] private Animator animator;
		[SerializeField] private string goldenChestOpen = "OpeningGoldChest";
		[SerializeField] private string goldenChestClose = "ClosingGoldChest";
		[SerializeField] private string idleGoldChest = "Idle";

		public SpriteRenderer spriteRenderer;
		public Sprite closedSprite;
		public Sprite openedSprite;
		public List<IItem> items;

		void Start() {
			// Gets animator, to play animations
			animator = gameObject.GetComponent<Animator>();
			// Calls method from ItemService which sends 3 random items
			items = ItemService.SINGLETON.Get3Random();
		}

		// Getter for the item list in chest
		public List<IItem> GetChestItems() {
			return items;
		}

		// ItemNotPicked, called from the chestDisplay class
		public void ItemNotPicked() {
			// Plays animation for closing of chest
			GetComponent<Animator>().Play(goldenChestClose);
			// Sets the isChestOpened to false
			isChestOpened = false;
		}

		// ItemPicked, called from the chestDisplay class
		public void ItemPicked() {
			// Destroy this gameobject
			Destroy(this.gameObject);
		}

		public string InteractionPrompt => _prompt;
		private bool isChestOpened = false;

		// Triggers when player interacts with chest
		public bool Interact(Interactor interactor) {
			// Checks if chest is opened
			if (isChestOpened == false) {
				isChestOpened = true;
				// Uses prefabservice to load an instance of the ChestContents Prefab
				var chestContents = PrefabService.SINGLETON.Instance("UI/ChestContents", GameObject.FindWithTag("MainCamera").transform.position + new Vector3(1000, 600, 0));
				// Makes sure this chest is set as the chest object, so it can be gotten outside class
				chestContents.gameObject.GetComponent<ChestDisplay>().setChestObject(this.gameObject);
				// Sets canvas to parent
				chestContents.transform.SetParent(GameObject.Find("Canvas").transform);
				// Plays opening animation of chest
				GetComponent<Animator>().Play(goldenChestOpen);
				return true;
			} else return false;
		}
	}
}
