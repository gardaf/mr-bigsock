using Bigsock;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigSock.Interact {
	public class Door : MonoBehaviour, IInteractable {
		[SerializeField] private string _prompt;
		public SpriteRenderer spriteRenderer;
		public Sprite newSprite;
		private GameObject[] enemies;
		private GameObject player;
		private GameObject cameraPlayer;
		private int i = TilemapGenerator.NextRoom();
		private GameObject boundary;
		GameObject door;

		public string InteractionPrompt => _prompt;

		public bool Interact(Interactor interactor) {
			if (i > 0){
				enemies = GameObject.FindGameObjectsWithTag((i - 1).ToString());
			}
			else if (i == 0){
				enemies = GameObject.FindGameObjectsWithTag((i).ToString());
			}
			if (enemies.Length == 0) {
				Debug.Log("Opening door!");
				spriteRenderer.sprite = newSprite;
				player = GameObject.Find("BigSock");
				boundary = GameObject.Find("CameraBoundry");
				cameraPlayer = GameObject.Find("Main Camera");
				player.transform.position = TilemapGenerator.DoorLocaitonTransport(i);
				cameraPlayer.transform.position = player.transform.position;
				boundary.GetComponent<PolygonCollider2D>().SetPath(0, TilemapGenerator.GetRoomBoundary(i));
				door = GameObject.Find("ClosedDoor(Clone)");
				Destroy(door);
				return true;
			}
			return false;
		}
	}
}
