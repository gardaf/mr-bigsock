using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


using BigSock.Item;


namespace BigSock.UI {

	/**
	 * Used for drag and dropp of items. 
	 */ 
	public class ItemPref : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IPointerClickHandler {
		public IItem item;
		public ItemSlot itemSlot;
		private RectTransform rectTransform;
		private CanvasGroup canvasGroup;


		void Start() {
			rectTransform = GetComponent<RectTransform>();
			canvasGroup = GetComponent<CanvasGroup>();

			// Change the sprite if the item has one.
			var sprite = item?.Icon;
			if (sprite != null)
				GetComponent<UnityEngine.UI.Image>().overrideSprite = sprite;
		}

		private void Awake() {
		}


		/*
			Moves the item to be centered on it's ItemSlot
		*/
		public void ResetPosition() {
			if (itemSlot != null) transform.position = itemSlot.transform.position;
		}

		/*
			Place this item in the given slot.
				Only does GUI stuff here, inventory code is handled elsewhere.
		*/
		public void Place(ItemSlot slot) {
			// Cannot pass it null.
			if (slot == null) throw new ArgumentNullException(nameof(slot));

			// Inform our current slot that we've moved.
			if (itemSlot != null) itemSlot.item = null;

			itemSlot = slot;
			slot.item = this;
			ResetPosition();
		}

		/*
			When the item is getting dragged, make it semi-transparent.
		*/
		public void OnBeginDrag(PointerEventData eventData) {
			canvasGroup.alpha = .6f;
			canvasGroup.blocksRaycasts = false;
		}

		/*
			Update its position while it's getting dragged.
		*/
		public void OnDrag(PointerEventData eventData) {
			rectTransform.anchoredPosition += eventData.delta;
		}

		/*
			When the item has been let go, make it opaque and center it on its slot.
		*/
		public void OnEndDrag(PointerEventData eventData) {
			ResetPosition();

			canvasGroup.alpha = 1f;
			canvasGroup.blocksRaycasts = true;
		}

		public void OnPointerDown(PointerEventData eventData) {
		}


		/*
			When the user clicks on this box, display the tooltip.
				(OnPointerClick only triggers if you press and release on the same object)
		*/
		public void OnPointerClick(PointerEventData eventData) {
			if (itemSlot != null && item != null)
				itemSlot.DisplayToolTip();
		}

	}
}

