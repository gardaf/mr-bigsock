using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace BigSock.UI {

	public class HpBarEnemy : MonoBehaviour {

		public Slider slider;



		protected void Start() {
			slider = GetComponent<Slider>();
		}

		public void SetHPEnemy(float hp) {
			slider.value = hp;
		}

		public void SetMaxHpEnemy(float hp) {
			slider.maxValue = hp;
		}
	}
}