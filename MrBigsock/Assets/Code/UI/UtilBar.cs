using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigSock.UI {

	/**
	 * Game object to show player their HP, Mana, Stamina and XP
	 */ 
	public class UtilBar : MonoBehaviour {

		public Slider hpSlider;
		public Slider manaSlider;
		public Slider staminaSlider;
		public Slider xpSlider;
		public Gradient gradient;
		public Image hpFill;

		/**
		 * WithHealth allows for setting Health
		 */ 
		public UtilBar WithHealth(int? value = null, int? maxValue = null) {
			if (value != null) hpSlider.value = value.Value;

			if (maxValue != null) hpSlider.maxValue = maxValue.Value;

			hpFill.color = gradient.Evaluate(hpSlider.normalizedValue);

			return this;
		}

		/**
		 * WithMana allows for setting Mana
		 */
		public UtilBar WithMana(int? value = null, int? maxValue = null) {
			if (value != null) manaSlider.value = value.Value;

			if (maxValue != null) manaSlider.maxValue = maxValue.Value;
			return this;
		}

		/**
		 * WithStamina allows for setting Stamina
		 */
		public UtilBar WithStamina(int? value = null, int? maxValue = null) {
			if (value != null) staminaSlider.value = value.Value;

			if (maxValue != null) staminaSlider.maxValue = maxValue.Value;
			return this;
		}

		/**
		 * WithXP allows for setting XP
		 */
		public UtilBar WithXP(int? value = null, int? maxValue = null) {
			if (value != null) xpSlider.value = value.Value;

			if (maxValue != null) xpSlider.maxValue = maxValue.Value;
			return this;
		}
	}
}
