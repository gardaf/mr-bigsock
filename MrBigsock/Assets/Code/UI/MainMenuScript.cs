using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BigSock.UI {

	/**
	 * For controlling the main menu. 
	 */ 
	public class MainMenuScript : MonoBehaviour {

		public string firstLevel;


		/**
		 * Loads first scene and ensures time scale is correct. 
		 */ 
		public void StartGame() {
			SceneManager.LoadScene(firstLevel);
			Time.timeScale = 1;
		}

		//Not implemented
		public void OpenOptions() {

		}

		//Not implemented
		public void CloseOptions() {

		}

		/**
		 * Quits game. 
		 */ 
		public void QuitGame() {
			Application.Quit();
		}
	}
}

