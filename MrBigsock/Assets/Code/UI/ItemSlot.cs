using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


using BigSock.Item;


namespace BigSock.UI {

	/**
	 * Used for one item slot in the inventory.
	 */ 
	public class ItemSlot : MonoBehaviour, IDropHandler {
		public InventoryType inventoryType;
		public int position;
		public ItemPref item;
		public Inventory inventory;

		// Indicates if the special update code has ran yet.
		bool firstRan = false;

		public void Start() {
			// Set the change indicator to false.
			transform.hasChanged = false;
		}

		public void Update() {
			/*
				If it hasn't ran before, and the position has changed, update the ItemPref.
				- This is to move the item to the item slot when it starts
				- This has to be this way because the item slot is hoisted in the corner until this point.
			*/
			if (!firstRan && transform.hasChanged) {
				transform.hasChanged = false;
				firstRan = true;

				item?.ResetPosition();
			}
		}

		/*
			When the user drops an item on this slot, try to move them
		*/
		public void OnDrop(PointerEventData eventData) {
			// Get the item prefab that was dropped.
			var itemPref = eventData?.pointerDrag?.GetComponent<ItemPref>();

			// Cancel if we didn't move an item prefab, or it was empty.
			if (itemPref?.item == null) return;

			// Try to move the item in the inventory.
			var didMove = inventory.MoveItem(
				itemPref.item, itemPref.itemSlot.inventoryType,
				itemPref.itemSlot.position, inventoryType,
				position
			);

			// If we successfully moved the item in the inventory: update the gui.
			if (didMove) itemPref.Place(this);

			/*
			Notes:
			- This doesn't handle what to do if this slot already had an item.
				- This isn't a problem currently, since the inventory system will refuse to move an item into an occupied slot.
				- If we want to add features to swap 2 items, this will need to be addressed.
			*/
		}

		/*
			Display the tooltip for our item onto the screen.
		*/
		public void DisplayToolTip() {
			// We must have an item.
			if (item?.item == null) return;

			// Find the inventory pannel we belong to.
			var inventoryPanel = transform.parent?.parent?.GetComponent<InventoryPanel>();
			if (inventoryPanel == null) return;

			// Set the tooltip.
			inventoryPanel.SetToolTip(item.item.GetToolTip());
		}

	}
}
