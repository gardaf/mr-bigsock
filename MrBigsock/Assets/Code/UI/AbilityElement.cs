using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace BigSock.UI {

	/**
	 * One ability element for UI, has a cooldown bar, rcharge bar and sprite.
	 */ 
	public class AbilityElement : MonoBehaviour {
		private Slider chargeSlider, cooldownSlider;
		private Image _sprite;

		/**
		 * WithCharge allows for setting Charge bar.
		 */
		public AbilityElement WithCharge(float? value = null, float? maxValue = null) {
			if (value != null) chargeSlider.value = value.Value;

			if (maxValue != null) chargeSlider.maxValue = maxValue.Value;
			return this;
		}

		/**
		 * WithCooldown allows for setting cooldown bar.
		 */
		public AbilityElement WithCooldown(float? value = null, float? maxValue = null) {
			if (value != null) cooldownSlider.value = value.Value;

			if (maxValue != null) cooldownSlider.maxValue = maxValue.Value;
			return this;
		}

		/**
		 * WithAbility allows for setting icon of ability.
		 */
		public AbilityElement WithAbility(AbilityEntity ability) {
			_sprite ??= transform.Find("Sprite").GetComponent<UnityEngine.UI.Image>();

			if (_sprite != null)
				_sprite.overrideSprite = ability.Ability.Icon ?? _sprite.overrideSprite;
			return this;
		}

		/**
		 * Start is called before the first frame update.
		 */
		void Start() {
			chargeSlider = transform.Find("ChargeSlider").GetComponent<Slider>();
			cooldownSlider = transform.Find("ReloadSlider").GetComponent<Slider>();
			_sprite ??= transform.Find("Sprite").GetComponent<UnityEngine.UI.Image>();
		}
	}
}
