using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigSock.UI {

	/**
	 * Simple UI element to show control scheme.
	 */ 
	public class ControlScheme : MonoBehaviour {

		float originalTime;

		// Sets time to 0 while viweing element.
		public void Start() {
			originalTime = Time.timeScale;
			Time.timeScale = 0;
		}

		// Sets time back to original when closing element.
		public void OnDestroy() {
			Time.timeScale = originalTime;
		}

		// Used to remove element.
		public void ExitMenu() {
			Destroy(gameObject);

		}
	}
}

