using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndScreenDed : MonoBehaviour {
	public string mainMenu;
	public void backToMain() {
		SceneManager.LoadScene(mainMenu);
	}

	public void QuitGame() {
		Application.Quit();
	}
}
