using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigSock.UI {

	public class HPBar : MonoBehaviour {

		public Slider slider;

		public void SetHealth(int health) {
			slider.value = health;
		}

		public void SetMaxHealth(int health) {
			slider.maxValue = health;
		}
	}
}


