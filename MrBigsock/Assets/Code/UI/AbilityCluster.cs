using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BigSock.Service;

namespace BigSock.UI {

	/**
	 * Keeps track of all bilities in the cluster.
	 */ 
	public class AbilityCluster : MonoBehaviour {
		private const string ABILITYELEMENT = "UI/Ability";
		private List<AbilityElement> abilityList;

		/**
		 * Sets ho many elements are in the ability cluster and adds referene to abilityList.
		 */ 
		public void SetElements(int elementAmount) {
			abilityList = new List<AbilityElement>();
			ResetElements();
			for (int i = 0; i < elementAmount; ++i) {
				abilityList.Add(PrefabService.SINGLETON.Instance(ABILITYELEMENT, transform).GetComponent<AbilityElement>());
			}
		}

		/**
		 * Clears all elements in ability cluster.
		 */
		public void ResetElements() {
			abilityList.Clear();
			var layout = this.GetComponent<HorizontalLayoutGroup>();
			for (int i = 0; i < layout.transform.childCount; i++) {
				Destroy(layout.transform.GetChild(i).gameObject);
			}
		}

		/**
		 * Sets the cooldown of a given element in the abilityList
		 */ 
		public void SetCooldown(int index, float amount) {
			abilityList[index].WithCooldown(amount);
		}

		/**
		 * Sets the charge of a given element in the abilityList
		 */
		public void SetCharge(int index, float amount) {
			abilityList[index].WithCharge(amount);
		}

		/**
		 * Sets the sprite of a given element in the abilityList
		 */
		public void SetAbility(int index, AbilityEntity ability) {
			abilityList[index].WithAbility(ability);
		}

	}
}
