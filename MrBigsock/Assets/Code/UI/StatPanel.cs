using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BigSock.UI {

	/**
	 * Controlls UI element for changing player statistics. 
	 */ 
	public class StatPanel : MonoBehaviour {


		//Variables for game objects.
		public PlayerController player;
		public Slider hpSlider, mpSlider, spSlider, dmgSlider, speedSlider, luckSlider, concSlider;
		public Button hpUp, hpDown, mpUp, mpDown, spUp, spDown, dmgUp, dmgDown, speedUp, speedDown, luckUp, luckDown, concUp, concDown, save;
		public GameObject pointIndicator;

		//Holds text component of text mesh pro object.
		private TextMeshProUGUI pointIndicatorText;

		//Used to show the player how many points they have left.
		public int xpPoints;


		private CharacterStats stats;
		private Skills skills;
		private Skills skillsTemp;
		private Skills maxSkill;
		private CharacterStats increase;

		private void Start() {
			//Sets point indicator
			pointIndicatorText = pointIndicator.GetComponent<TextMeshProUGUI>();
			GetNewStats();



			hpUp.onClick.AddListener(() => {
				if (xpPoints > 0 && skills.HP + skillsTemp.HP < maxSkill.HP) {
					xpPoints--;
					skillsTemp.HP++;
					stats.Apply(increase, Skill.HP, 1);
					hpSlider.value = skills.HP + skillsTemp.HP;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});

			hpDown.onClick.AddListener(() => {
				if (skillsTemp.HP > 0) {
					xpPoints++;
					skillsTemp.HP--;
					stats.Apply(increase, Skill.HP, -1);
					hpSlider.value = skillsTemp.HP + skills.HP;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});
			mpUp.onClick.AddListener(() => {
				if (xpPoints > 0 && skills.MP + skillsTemp.MP < maxSkill.MP) {
					xpPoints--;
					skillsTemp.MP++;
					stats.Apply(increase, Skill.MP, 1);
					mpSlider.value = skills.MP + skillsTemp.MP;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});

			mpDown.onClick.AddListener(() => {
				if (skillsTemp.MP > 0) {
					xpPoints++;
					skillsTemp.MP--;
					stats.Apply(increase, Skill.MP, -1);
					mpSlider.value = skillsTemp.MP + skills.MP;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});
			spUp.onClick.AddListener(() => {
				if (xpPoints > 0 && skills.SP + skillsTemp.SP < maxSkill.SP) {
					xpPoints--;
					skillsTemp.SP++;
					stats.Apply(increase, Skill.SP, 1);
					spSlider.value = skills.SP + skillsTemp.SP;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});

			spDown.onClick.AddListener(() => {
				if (skillsTemp.SP > 0) {
					xpPoints++;
					skillsTemp.SP--;
					stats.Apply(increase, Skill.SP, -1);
					spSlider.value = skillsTemp.SP + skills.SP;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});
			dmgUp.onClick.AddListener(() => {
				if (xpPoints > 0 && skills.Damage + skillsTemp.Damage < maxSkill.Damage) {
					xpPoints--;
					skillsTemp.Damage++;
					stats.Apply(increase, Skill.Damage, 1);
					dmgSlider.value = skills.Damage + skillsTemp.Damage;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});

			dmgDown.onClick.AddListener(() => {
				if (skillsTemp.Damage > 0) {
					xpPoints++;
					skillsTemp.Damage--;
					stats.Apply(increase, Skill.Damage, -1);
					dmgSlider.value = skillsTemp.Damage + skills.Damage;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});
			speedUp.onClick.AddListener(() => {
				if (xpPoints > 0 && skills.Speed + skillsTemp.Speed < maxSkill.Speed) {
					xpPoints--;
					skillsTemp.Speed++;
					stats.Apply(increase, Skill.Speed, 1);
					speedSlider.value = skills.Speed + skillsTemp.Speed;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});

			speedDown.onClick.AddListener(() => {
				if (skillsTemp.Speed > 0) {
					xpPoints++;
					skillsTemp.Speed--;
					stats.Apply(increase, Skill.Speed, -1);
					speedSlider.value = skillsTemp.Speed + skills.Speed;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});
			luckUp.onClick.AddListener(() => {
				if (xpPoints > 0 && skills.Luck + skillsTemp.Luck < maxSkill.Luck) {
					xpPoints--;
					skillsTemp.Luck++;
					stats.Apply(increase, Skill.Luck, 1);
					luckSlider.value = skills.Luck + skillsTemp.Luck;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});

			luckDown.onClick.AddListener(() => {
				if (skillsTemp.Luck > 0) {
					xpPoints++;
					skillsTemp.Luck--;
					stats.Apply(increase, Skill.Luck, -1);
					luckSlider.value = skillsTemp.Luck + skills.Luck;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});
			concUp.onClick.AddListener(() => {
				if (xpPoints > 0 && skills.Concentration + skillsTemp.Concentration < maxSkill.Concentration) {
					xpPoints--;
					skillsTemp.Concentration++;
					stats.Apply(increase, Skill.Concentration, 1);
					concSlider.value = skills.Concentration + skillsTemp.Concentration;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});

			concDown.onClick.AddListener(() => {
				if (skillsTemp.Concentration > 0) {
					xpPoints++;
					skillsTemp.Concentration--;
					stats.Apply(increase, Skill.Concentration, -1);
					concSlider.value = skillsTemp.Concentration + skills.Concentration;
					pointIndicatorText.text = xpPoints.ToString();
				}
			});


			save.onClick.AddListener(Save);
		}

		/**
		 * Saves player statistics. 
		 */ 
		private void Save() {
			player.Skills = skills.Increase(skillsTemp);
			player.BaseStats = stats;
			player.SkillPoints = xpPoints;
			player.UpdateModifiers();
			GetNewStats();
		}

		/**
		 * Gets stats from player obejct. 
		 */
		private void GetNewStats() {
			xpPoints = player.SkillPoints;
			skills = player.Skills;
			skillsTemp = new Skills();
			maxSkill = player.MaxSkills;
			increase = player.StatIncreasePrSkillLevel;
			stats = (CharacterStats)player.BaseStats.Add(new CharacterStats());

			hpSlider.maxValue = maxSkill.HP;
			spSlider.maxValue = maxSkill.SP;
			mpSlider.maxValue = maxSkill.MP;
			dmgSlider.maxValue = maxSkill.Damage;
			speedSlider.maxValue = maxSkill.Speed;
			luckSlider.maxValue = maxSkill.Luck;
			concSlider.maxValue = maxSkill.Concentration;

			hpSlider.value = skills.HP;
			spSlider.value = skills.SP;
			mpSlider.value = skills.MP;
			dmgSlider.value = skills.Damage;
			speedSlider.value = skills.Speed;
			luckSlider.value = skills.Luck;
			concSlider.value = skills.Concentration;

			pointIndicatorText.text = xpPoints.ToString();
		}
	}
}

