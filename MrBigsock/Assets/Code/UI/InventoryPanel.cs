using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


using BigSock.Item;
using BigSock.Service;


namespace BigSock.UI {

	/**
	 * Used for controlling the different aspects of the inventory UI element. 
	 */ 
	public class InventoryPanel : MonoBehaviour {
		public PlayerController player;
		public const string INVSLOT = "UI/Inventoryslot";
		public const string ITEM = "UI/Item";

		public GridLayoutGroup gridBackPack;
		public GridLayoutGroup gridTools;
		public GridLayoutGroup gridAccessory;
		public GridLayoutGroup gridEquipment;

		public TextMeshProUGUI toolTip;


		public void Start() {
			GenerateInv();

			// Get the tooltip child-component if it's not set in the prefab.
			toolTip ??= transform.Find("ToolTip")?.GetComponent<TextMeshProUGUI>();
		}

		/*
			Generates the GUI elements for the player's inventory.
		*/
		public void GenerateInv() {
			if (player == null) return;
			var inventory = player.Inventory;

			var trash = transform.Find("Trash")?.GetComponent<InvTrash>();
			if (trash != null) {
				trash.inventory = inventory;
			}

			for (int i = 0; i < inventory.Backpack.Count; ++i) {
				var invSlot = PrefabService.SINGLETON.Instance(INVSLOT, gridBackPack.transform);
				var invScript = invSlot.GetComponent<ItemSlot>();
				invScript.inventory = inventory;
				invScript.inventoryType = InventoryType.Backpack;
				invScript.position = i;
				var item = inventory.Backpack[i];
				if (item != null) {
					var itemPref = PrefabService.SINGLETON.Instance(ITEM, transform);
					var itemScript = itemPref.GetComponent<ItemPref>();
					itemScript.item = item;
					itemScript.Place(invScript);
				}
			}

			for (int i = 0; i < inventory.Tools.Count; ++i) {
				var invSlot = PrefabService.SINGLETON.Instance(INVSLOT, gridTools.transform);
				var invScript = invSlot.GetComponent<ItemSlot>();
				invScript.inventory = inventory;
				invScript.inventoryType = InventoryType.Tool;
				invScript.position = i;
				var item = inventory.Tools[i];
				if (item != null) {
					var itemPref = PrefabService.SINGLETON.Instance(ITEM, transform);
					var itemScript = itemPref.GetComponent<ItemPref>();
					itemScript.item = item;
					itemScript.Place(invScript);
				}
			}

			for (int i = 0; i < inventory.Equipment.Count; ++i) {
				var invSlot = PrefabService.SINGLETON.Instance(INVSLOT, gridEquipment.transform);
				var invScript = invSlot.GetComponent<ItemSlot>();
				invScript.inventory = inventory;
				invScript.inventoryType = InventoryType.Equipment;
				invScript.position = i;
				var item = inventory.Equipment[i];
				if (item != null) {
					var itemPref = PrefabService.SINGLETON.Instance(ITEM, transform);
					var itemScript = itemPref.GetComponent<ItemPref>();
					itemScript.item = item;
					itemScript.Place(invScript);
				}
			}

			for (int i = 0; i < inventory.Accessories.Count; ++i) {
				var invSlot = PrefabService.SINGLETON.Instance(INVSLOT, gridAccessory.transform);
				var invScript = invSlot.GetComponent<ItemSlot>();
				invScript.inventory = inventory;
				invScript.inventoryType = InventoryType.Accessory;
				invScript.position = i;
				var item = inventory.Accessories[i];
				if (item != null) {
					var itemPref = PrefabService.SINGLETON.Instance(ITEM, transform);
					var itemScript = itemPref.GetComponent<ItemPref>();
					itemScript.item = item;
					itemScript.Place(invScript);
				}
			}

		}

		/*
			Sets the text of the tooltip.
				(Current version is just a basic test of the concept)
		*/
		public void SetToolTip(string text) {
			toolTip?.SetText(text);

			/*
				Planned changes:
					- Take in position.
					- Give it a background.
					- Place it where we indicate.
					- Way to remove it when we move.
				Vision:
					- When player hovers over/clicks on the item, tooltip shows up.
						- If we use hover: have delay and tooltip goes away when mouse moves away.
						- If we use click: have it go away if user clicks elsewhere or again.
					- Have tooltip either spawn based on cursor, or like a dropdown for the slot.
				Ambitious:
					- Have toolbox fade based on cursor distance from source.
						- When cursor moves away from the item that caused it to appear, have it fade for a certain distance before it is removed.
						+ When cursor leaves the item slot, for a radius of 1x the slot size, fade from 100% to 0%, then remove if further.
					- Have a way to color/bold part of the text.
						- Bold the name.
						- Use effects (color, italic, underline, ...) on key points of text to highlight.
			*/
		}
	}
}

