using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using BigSock.Service;
using BigSock.UI;
using BigSock.Item;
using BigSock.Interact;

using UnityEditor;

namespace BigSock.UI {
	public class ChestDisplay : MonoBehaviour {
		// UI elements
		public Button closeButton;
		public List<Button> buttons;

		// Gameobjects
		public GameObject other;
		public GameObject chest;
		PlayerController player;

		// Item data
		public List<IItem> items;

		void Start() {
			// Gets the parent of the itembuttons
			var itemButtonLocation = transform.Find("ItemBox").Find("ItemBoxBackground");

			// Creates a new list of buttons
			buttons = new List<Button>();

			// Gets button gameobjects to add onClick listener and add them to the list of buttons
			for (int i = 0; i < 3; i++) {
				var button = itemButtonLocation.transform.Find("ButtonItem" + (i + 1).ToString()).GetComponent<Button>();
				int j = i;
				button.onClick.AddListener(delegate { ItemPicked(j); });
				buttons.Add(button);
			}

			// Button to call manuallyCloseWindow
			closeButton = transform.Find("CloseButton").GetComponent<Button>();
			closeButton.onClick.AddListener(manuallyCloseWindow);

			// Gets the Items stored in the chest you open
			items = chest.transform.GetComponent<Chest>().GetChestItems();

			// Sets Item icon and name
			for (int i = 0; i < buttons.Count; i++) {
				buttons[i].gameObject.transform.Find("Item").GetComponent<Image>().sprite = ItemService.SINGLETON.Get(items[i].Id).Icon;
				buttons[i].gameObject.transform.Find("ItemName").GetComponent<TMPro.TMP_Text>().text = ItemService.SINGLETON.Get(items[i].Id).Name;
			}

			// Finds the gameobject with the player tag and gets the playercontroller
			this.other = GameObject.FindWithTag("Player");
			player = other.GetComponent<PlayerController>();

			// Tries to add null item, to check if the inventory is full
			if (player.Inventory.AddItem(null) == -1) {
				for (int i = 0; i < buttons.Count; i++) {
					// Turns of interactable on the buttons
					buttons[i].interactable = false;
				}
				// Adds the text that inventory is full to the display
				transform.Find("Banner").Find("BannerBackground").Find("BannerExtraText").
					GetComponent<TMPro.TMP_Text>().text = "Inventory is full, free up some space to select an item";
			}
		}

		// Shows the Item description
		public void ShowItemDescription(GameObject button) {
			string[] buttonNumber = button.name.Split("ButtonItem");
			// Using the Item method GetToolTip to show item info
			button.transform.Find("ItemDescription").GetComponent<TMPro.TMP_Text>().text = items[int.Parse(buttonNumber[1]) - 1].GetToolTip();
			// Sets the button to clear, to hide image
			button.transform.Find("Item").GetComponent<Image>().color = Color.clear;
		}

		// Hides item description
		public void HideItemDescription(GameObject button) {
			// Clears the text of the description
			button.transform.Find("ItemDescription").GetComponent<TMPro.TMP_Text>().text = "";
			// Sets image color to white, which is default
			button.transform.Find("Item").GetComponent<Image>().color = Color.white;
		}

		private void Update() {
			// Calls manuallyCloseWindow when escape is pressed on keyboard
			if (Input.GetKeyDown(KeyCode.Escape)) {
				manuallyCloseWindow();
			}
		}

		private void manuallyCloseWindow() {
			// Calls the ItemNotPicked from Chest
			chest.transform.GetComponent<Chest>().ItemNotPicked();
			// Destroys the UI element
			Destroy(this.gameObject);
		}

		// Gets the chest element the display belongs to
		public void setChestObject(GameObject chestInstance) {
			chest = chestInstance;
		}

		// Method called on the button Onclick listeners
		void ItemPicked(int number) {
			// Pick up the item clicked on with the number being from 0-2.
			player.TryPickUpItem(ItemService.SINGLETON.Get(items[number].Id));
			// Informs chest that item has been picked
			chest.transform.GetComponent<Chest>().ItemPicked();
			// Destroys the UI element
			Destroy(this.gameObject);
		}
	}
}
