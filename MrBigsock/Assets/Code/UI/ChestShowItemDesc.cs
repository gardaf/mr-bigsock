using UnityEngine.EventSystems;

namespace BigSock.UI {
	public class ChestShowItemDesc : EventTrigger {

		public override void OnPointerEnter(PointerEventData data) {
			// Calls the showItemDescription in ChestDisplay, which passes this gameobject
			gameObject.transform.parent.gameObject.transform.parent.transform.parent.
				gameObject.transform.GetComponent<ChestDisplay>().ShowItemDescription(this.gameObject);
		}

		public override void OnPointerExit(PointerEventData data) {
			// Calls the hideItemDescription in ChestDisplay, which passes this gameobject
			gameObject.transform.parent.gameObject.transform.parent.transform.parent.
				gameObject.transform.GetComponent<ChestDisplay>().HideItemDescription(this.gameObject);
		}
	}
}
