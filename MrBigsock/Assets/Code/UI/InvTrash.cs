﻿using BigSock.Item;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BigSock.UI {

	/**
	 * Used for the strashcan in the inventory panel. 
	 */ 
	public class InvTrash : MonoBehaviour, IDropHandler {
		public Inventory inventory;

		/**
		 * When dropping item on trashcan, remove from earlier slot and delete item.
		 */ 
		public void OnDrop(PointerEventData eventData) {
			// Get the item prefab that was dropped.
			var itemPref = eventData?.pointerDrag?.GetComponent<ItemPref>();

			// Cancel if we didn't move an item prefab, or it was empty.
			if (itemPref?.item == null) return;

			// Try to move the item in the inventory.
			var didMove = inventory.DropItem(
				itemPref.item, itemPref.itemSlot.inventoryType,
				itemPref.itemSlot.position);

			// If we successfully moved the item in the inventory: update the gui.
			if (didMove) itemPref.itemSlot.item = null;

			Destroy(itemPref.gameObject);		
		}
	}
}