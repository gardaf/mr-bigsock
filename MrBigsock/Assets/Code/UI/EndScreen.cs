using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BigSock.UI {

	/**
	 * Used when player has died, gives option to go back to main meny or quit app. 
	 */ 
	public class EndScreen : MonoBehaviour {

		/**
		 * When screen starts, stop time and remove player menu if active. 
		 */
		public void Start() {
			Time.timeScale = 0;
			GameObject menu = GameObject.Find("PlayerMenu(Clone)");
			if (menu != null) {
				Destroy(menu.gameObject);
			}
		}

		/**
		 * When object is removed sets time back to 1. 
		 */ 
		public void OnDestroy() {
			Time.timeScale = 1;
		}

		/**
		 * Used to quit app, when pressing quit button. 
		 */ 
		public void GiveUp() {
			Application.Quit();
		}

		/**
		 * Send player back to main menu. 
		 */ 
		public void TryAgain() {
			Destroy(gameObject);
			SceneManager.LoadScene("MainMenu");
		}
	}
}
