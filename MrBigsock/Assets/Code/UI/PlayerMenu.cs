using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigSock.UI {

	/**
	 * For controlling the player menu.
	 */ 
	public class PlayerMenu : MonoBehaviour {

		public void Start() {

			Time.timeScale = 0; //Ensures game pauses
		}

		public void ExitMenu() {
			Destroy(gameObject);
		}

		public void OnDestroy() {
			Time.timeScale = 1; //Ensures the game gets un-paused
		}

		/**
		 * For killing player, to eiter reset or end the game. 
		 */ 
		public void Die() {
			GameObject player = GameObject.Find("BigSock");
			Destroy(player.gameObject);
		}
	}
}

