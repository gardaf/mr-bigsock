using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using BigSock.Service;


namespace BigSock {

	public partial class Enemy_orc_range : EnemyController {
		/*
			The state 
		*/
		public State State { get; protected set; } = State.Idle;

		public GameObject attack;
		protected IAttack _testAttack;
		/*
			The location of the target when attack start. 
		*/
		public Vector3 TargetLocation { get; protected set; }

		/*
			The next time the state can change.
		*/
		public DateTime NextTimeStateCanChange { get; private set; } = DateTime.Now;

		/*
			Minimum idle time.
		*/
		protected static readonly TimeSpan IDLE_WAIT_TIME = new TimeSpan(0, 0, 0, 0, 400);
		/*
			Minimum animation time.
		*/
		protected static readonly TimeSpan CHARGE_WAIT_TIME = new TimeSpan(0, 0, 0, 0, 400);
		/*
			Maximum time the slime should attack before it can idle.
		*/
		protected static readonly TimeSpan ATTACK_WAIT_TIME = new TimeSpan(0, 0, 0, 1, 0);

		protected Animator m_Animator_bow;

		protected override void Start() {
			base.Start();
			_testAttack = (IAttack)AbilityService.SINGLETON.Get(103);
			m_Animator_bow = gameObject.transform.GetChild(2).GetComponent<Animator>();

		}


		protected override void Update() {
			if (State == State.Idle) {
				// If it has a target and has idled long enough.
				if (target != null && DateTime.Now >= NextTimeStateCanChange && isInMelee) {
					// Store target location.
					TargetLocation = target.position;

					// Update the state.
					State = State.Charging;
					NextTimeStateCanChange = DateTime.Now + CHARGE_WAIT_TIME;
					m_Animator.SetTrigger("walk");
				}
				if (target != null && !isInMelee) {
					TryMove((new Vector2(target.position.x, target.position.y) - rb.position).normalized);
				}
			} else if (State == State.Charging) {
				// If it has charged long enough.
				m_Animator_bow.SetTrigger("attack");
				if (DateTime.Now >= NextTimeStateCanChange && target != null) {
					m_Animator_bow.SetTrigger("none");


					// Object w/ parameters for abilities.
					var par = GetAbilityParam(target.position);
					//var par = new AbilityParam{
					//	Actor = this,
					//	TargetPos = target.position,
					//	MovementDir = moveDir,
					//};

					_testAttack.Use(par);

					State = State.Attacking;
					NextTimeStateCanChange = DateTime.Now + ATTACK_WAIT_TIME;
				}
				if (target != null && !isInMelee) {
					TryMove((new Vector2(target.position.x, target.position.y) - rb.position).normalized);
				}
			} else if (State == State.Attacking) {
				// If it has charged long enough.
				if (DateTime.Now >= NextTimeStateCanChange || rb.velocity == Vector2.zero) {
					// Update the state.

					State = State.Idle;
					NextTimeStateCanChange = DateTime.Now + IDLE_WAIT_TIME;
					m_Animator.SetTrigger("idle");

				}
			}

		}

	}

	/*
		Movement
	*/
	public partial class Enemy_orc_range {

		protected override void Move_OnColliderEnter2D(Collider2D other) {
			if (other.gameObject.tag == "Player") {
				//m_Animator.SetTrigger("walk");
				target = other.transform;
			}
		}


		protected override void Move_OnColliderExit2D(Collider2D other) {
			if (other.gameObject.tag == "Player") {
				//m_Animator.SetTrigger("idle");
				target = null;
			}
		}

	}
	/*
		Attack
	*/
	public partial class Enemy_orc_range {

		protected override void Attack_OnColliderEnter2D(Collider2D other) {
			if (other.gameObject.tag == "Player") {
				//m_Animator.SetTrigger("walk");
				isInMelee = true;
				target = other.transform;
			}
		}

		protected override void Attack_OnColliderStay2D(Collider2D other) {

		}


		protected override void Attack_OnColliderExit2D(Collider2D other) {
			if (other.gameObject.tag == "Player") {
				//m_Animator.SetTrigger("idle");
				isInMelee = false;
			}
		}

	}

	/*
		The different states the slime can be in.
	*/
	public enum State {
		Idle, Charging, Attacking
	}
}
