using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;


namespace BigSock {

	public partial class EnemyController : Character {
		public float collisionOffset = 0.05f;

		public ContactFilter2D movementFilter;
		protected List<RaycastHit2D> castCollisions = new List<RaycastHit2D>();
		protected Transform target;
		protected Animator m_Animator;
		protected float distance;

		protected float canAttack;

		protected EmptyCollider followCollider;
		protected EmptyCollider attackCollider;

		protected bool isInMelee = false;


		//Rigidbody2D rb;

		// private Collider2D_Proxy secondCollider;

		protected override void Start() {
			base.Start();

			m_Animator = gameObject.GetComponent<Animator>();

			followCollider = transform.Find("followCollider").GetComponent<EmptyCollider>();
			followCollider.OnColliderEnter2D_Action += Move_OnColliderEnter2D;
			followCollider.OnColliderStay2D_Action += Move_OnColliderStay2D;
			followCollider.OnColliderExit2D_Action += Move_OnColliderExit2D;

			attackCollider = transform.Find("MeleeCollider").GetComponent<EmptyCollider>();
			attackCollider.OnColliderEnter2D_Action += Attack_OnColliderEnter2D;
			attackCollider.OnColliderStay2D_Action += Attack_OnColliderStay2D;
			attackCollider.OnColliderExit2D_Action += Attack_OnColliderExit2D;
		}







		protected virtual void RotateAnimation(Vector2 direction) {
			if (direction.x > 0.01f) {
				gameObject.GetComponent<SpriteRenderer>().flipX = false;
			} else if (direction.x < -0.01f) {
				gameObject.GetComponent<SpriteRenderer>().flipX = true;
			}
		}

		protected virtual void Update() {
			Regenerate();

			if (target != null && !isInMelee) {

				/* //walk
				float step = speed * Time.deltaTime;
				transform.position = Vector2.MoveTowards(transform.position, target.position, step);
				
				//distance = Vector3.Distance (transform.position, target.position);
				//roter
				RotateAnimation();
				*/

				TryMove((new Vector2(target.position.x, target.position.y) - rb.position).normalized);
			} else {

			}
		}
		public override bool TakeDamage(AttackStats attack) {
			var d = base.TakeDamage(attack);

			if (HpBarEnemy == null) print("Error feil funk ikke");
			HpBarEnemy?.SetMaxHpEnemy(this.MaxHP);
			HpBarEnemy?.SetHPEnemy(this.HP);

			return d;
		}


	}


	/*
		Attack
	*/
	public partial class EnemyController {

		protected virtual void Attack_OnColliderEnter2D(Collider2D other) {
			if (other.gameObject.tag == "Player") isInMelee = true;
		}



		protected virtual void Attack_OnColliderStay2D(Collider2D other) {
			var player = other.gameObject.GetComponent<PlayerController>();
			if (player != null) {
				// Create attack object.
				var attack = (AttackStats)new AttackStats {
					Damage = 1,
					Knockback = 1,
					Range = 1,
					AttackSpeed = 1,
					Source = transform.position,
					Actor = this,
				}.Calculate(Stats);

				// Get the player to take the damage.
				if (player.TakeDamage(attack)) {
					//rb position - player position
					//Vector2 difference = (other.transform.position - transform.position).normalized;
					//player.KnockBack(KnockbackForce, difference);
					//animer nå ?
				}
			}
		}
		protected virtual void Attack_OnColliderExit2D(Collider2D other) {
			if (other.gameObject.tag == "Player")
				isInMelee = false;
		}

	}

	/*
		Move
	*/
	public partial class EnemyController {

		protected virtual void Move_OnColliderEnter2D(Collider2D other) {
			//Debug.Log("enter");
			if (other.gameObject.tag == "Player") {
				//Debug.Log("enter if");

				m_Animator.SetTrigger("walk");
				target = other.transform;
			}
		}

		protected virtual void Move_OnColliderStay2D(Collider2D other) {
			if (other.gameObject.tag == "Player") {
				m_Animator.SetTrigger("walk");
				target = other.transform;
			}
		}

		protected virtual void Move_OnColliderExit2D(Collider2D other) {
			if (other.gameObject.tag == "Player") {
				m_Animator.SetTrigger("idle");
				target = other.transform;
				target = null;
			}
		}

	}

}
