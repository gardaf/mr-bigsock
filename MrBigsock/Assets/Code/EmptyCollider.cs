using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace BigSock {
	public class EmptyCollider : MonoBehaviour {
		public event Action<Collider2D> OnColliderEnter2D_Action;
		public event Action<Collider2D> OnColliderStay2D_Action;
		public event Action<Collider2D> OnColliderExit2D_Action;

		private void OnTriggerEnter2D(Collider2D other) {
			//Debug.Log("enter");
			OnColliderEnter2D_Action?.Invoke(other);
		}

		private void OnTriggerStay2D(Collider2D other) {
			OnColliderStay2D_Action?.Invoke(other);
		}

		private void OnTriggerExit2D(Collider2D other) {
			//Debug.Log("exit");
			OnColliderExit2D_Action?.Invoke(other);
		}
	}
}