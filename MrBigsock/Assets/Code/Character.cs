using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using BigSock.Item;
using BigSock.UI;

namespace BigSock {

	/*
		Common class for all characters.
	*/
	public partial class Character : Entity {

		public const float MOVE_MULTIPLIER = 0.7f;
		/*
			Attack speed of the character.
		*/
		public float AttackSpeed => Stats.AttackSpeed;
		public float baseAttackSpeed = 1;

		public List<AudioSource> source = new List<AudioSource>();
		public AudioClip TakeDamageAudio;

		/*
			Cooldown between attacks.
		*/
		public float AttackCooldown => 1.0f / AttackSpeed;

		/*
			Movement speed of the character.
		*/
		public float MovementSpeed => Stats.MoveSpeed;
		public float baseMovementSpeed = 1;


		/*
			Damage of the character.
		*/
		public float Damage => Stats.Damage;
		public float baseDamage = 1;

		/*
			Knockback force
		*/
		public float KnockbackForce => Stats.Knockback;
		public float knockbackForce = 150;


		/*
			Hit points of the character.
		*/
		public float HP {
			get => baseHP;
			set => baseHP = value;
		}
		public float baseHP = 10;

		/*
			Mana of the character.
		*/
		public float Mana { get; set; }

		/*
			Stamina of the character.
		*/
		public float Stamina { get; set; }

		/*
			Maximum hit points of the character.
		*/
		public float MaxHP => Stats.MaxHP;
		public float baseMaxHP = 10;

		/*
			Xp....
		*/
		public float dropXP;
		public float xp;
		public float maxXp;
		public int level;


		public HpBarEnemy HpBarEnemy;

		protected Rigidbody2D rb;

		// The direction the character last moved.
		protected Vector2 moveDir;

		// trigger for in knockback.
		protected bool inKnockBack = false;


		/*
			The inventory of the character.
		*/
		public Inventory Inventory { get; protected set; }

		/*
			The base stats of the character.
		*/
		public ICharacterStats BaseStats { get; set; } = new CharacterStats();

		/*
			The final stats of the character after applying modifiers.
		*/
		public ICharacterStats Stats { get; protected set; } = new CharacterStats();

		public Character() {
			Inventory = new Inventory(this);
			Inventory.BackpackCap = 6;
			Inventory.AccessoriesCap = 3;
			Inventory.EquipmentCap = 3;
			Inventory.ToolsCap = 3;
		}

		protected virtual void Start() {
			rb = GetComponent<Rigidbody2D>();
			HpBarEnemy = GameObject.Find("HpBarEnemy")?.GetComponent<HpBarEnemy>();
			//Inventory = new Inventory(this);

			// Set the base stats.
			BaseStats = new CharacterStats {
				MaxHP = baseMaxHP,
				MaxMana = 20,
				MaxStamina = 20,
				RegenMana = 2,
				RegenStamina = 1,

				Damage = baseDamage,
				MoveSpeed = baseMovementSpeed,
				Knockback = knockbackForce,
				Range = 1,
				AttackSpeed = baseAttackSpeed,
				CritChance = 1,
				CritDamageModifier = 1,
				ProjectileSpeed = 1,
				Accuracy = 1,
			};

			// Add audio sources.
			foreach (var s in Camera.main.gameObject.GetComponents<AudioSource>())
				if (s != null)
					source.Add(s);

			var src = GetComponent<AudioSource>();
			if (src != null) source.Add(src);


			UpdateModifiers();

			Mana = Stats.MaxMana;
			Stamina = Stats.MaxStamina;
		}

		/*
			Regenerates mana and stamina.
		*/
		protected virtual void Regenerate() {
			Mana = Math.Min(Stats.MaxMana, Mana + Time.fixedDeltaTime * Stats.RegenMana);
			Stamina = Math.Min(Stats.MaxStamina, Stamina + Time.fixedDeltaTime * Stats.RegenStamina);
		}

		/*
			Updates the modifiers to the character's stats.
		*/
		public virtual void UpdateModifiers(ICharacterStats modifiers = null) {
			modifiers ??= Inventory.Modifier;
			Stats = BaseStats.Modify(modifiers);
		}


		/*
			Gets the AbilityParams we want.
		*/
		public virtual IAbilityParam GetAbilityParam(Vector2? targetPos) {
			return new AbilityParam {
				Actor = this,
				TargetPos = targetPos,
				MovementDir = moveDir,
			};
		}
		/*
			Using a courutine to set trigger in knockback.
		*/
		IEnumerator knockBackTimer() {
			inKnockBack = true;
			yield return new WaitForSeconds(0.15F);
			rb.velocity = Vector2.zero;
			inKnockBack = false;
		}

		/*
			Add Knockback. (used only for dash ?)
		*/
		public void KnockBack(float force, Vector2 difference) {
			rb.AddForce(difference * force, ForceMode2D.Impulse);
			StartCoroutine(knockBackTimer());

			//rb.MovePosition((Vector2)transform.position + (difference * force * Time.fixedDeltaTime));
		}
		public void KnockBack(IAttackStats attack) {
			Vector2 difference = ((Vector2)transform.position - attack.Source).normalized;
			rb.AddForce(difference * attack.Knockback, ForceMode2D.Impulse);
			StartCoroutine(knockBackTimer());
		}


		/*
			Try to move in a given direction.
		*/
		protected virtual bool TryMove(Vector2 direction) {

			moveDir = direction;
			if (direction != Vector2.zero && !inKnockBack) {
				//Using movePosition to get a "snappy" movement. 
				//rb.AddForce(direction * (float) MovementSpeed * Time.fixedDeltaTime, ForceMode2D.Impulse);
				rb.MovePosition((Vector2)transform.position + (direction * (float)MovementSpeed * Time.fixedDeltaTime * MOVE_MULTIPLIER));
				//rb.velocity = (direction * (float) MovementSpeed);
				return true;
			}
			return false;
		}

		/*
			Adds damage to the character if they don't have IFrames.
		*/
		public virtual bool TakeDamage(AttackStats attack) {
			if (attack == null) throw new ArgumentNullException(nameof(attack));
			if (!attack.IsCalculated) throw new ArgumentException("Attack needs to be calculated.", nameof(attack));

			// Check if player has IFrames
			if (HasIFrames)
				return false;

			// Start new IFrames
			AddIFrames(IFrameDuration);

			// Trigger the event for taking damage.
			OnTakeDamage?.Invoke(this, attack.Actor, attack);

			if (HpBarEnemy == null) print("Error feil funk ikke");
			HpBarEnemy?.SetMaxHpEnemy(this.MaxHP);
			HpBarEnemy?.SetHPEnemy(this.HP);

			print(this.HP + "  ___ " + this.MaxHP);


			if (TakeDamageAudio != null && source?.Count > 0 && source[0] != null) {
				source[0].clip = TakeDamageAudio;
				source[0].Play();
			}

			// Inform the attacker they hit us.
			if (attack.Actor != null) {
				attack.Actor.TargetHit(this, attack);


			}


			// Add damage
			HP -= attack.Damage;
			AfterDamage(attack);

			TryKill(attack);

			return true;
		}


		/*
			Try to kill the character.
		*/
		public bool TryKill(AttackStats attack) {
			if (Alive && HP <= 0) {


				// Trigger the event for us dying.
				OnDeath?.Invoke(this, attack.Actor, attack);

				//== PUT CODE HERE TO HANDLE IF WE DODGED DEATH (In case we had an item to revieve or cheat death)

				Alive = false;

				// Inform the attacker killed us.
				if (attack.Actor != null) {
					attack.Actor.TargetKilled(this, attack);
					if (attack.Actor is PlayerController player)
						player.GainXp(dropXP);
				}

				AfterDeath();

				return true;
			}
			return false;
		}


		/*
			Method for healing the character.
		*/
		public bool TryHeal(float amount) {
			// Can't heal if full.
			if (HP >= MaxHP) {
				print($"[Character.TryHeal()] Already Full! ({HP:N1} >= {MaxHP:N1})");
				return false;
			}

			print($"[Character.TryHeal()] {HP:N1} + {amount:N1} = {HP + amount:N1}");
			OnHeal?.Invoke(this, amount);

			// Heal the character.
			var res = HP + amount;
			if (res > MaxHP) res = MaxHP;
			HP = res;
			AfterHeal();
			return true;
		}

		/*
			Method for what to do when the character takes damage.
		*/
		protected virtual void AfterDamage(IAttackStats attack) {
			print($"[Character.AfterDamage()] {HP + attack.Damage:N1} - {attack.Damage:N1} = {HP:N1} {(attack.IsCrit ? "[CRIT]" : "")}");
			KnockBack(attack);
		}


		protected virtual void AfterHeal() {

		}
		/*
			Method for what to do when the character dies.
		*/
		protected virtual void AfterDeath() {
			print($"[Character.AfterDeath()] start. | {HP}, {Alive}");
			Destroy(gameObject);
		}
	}





	/*
		Items
	*/
	public partial class Character {

		/*
			Adds a listener for a conditional item to this character's event.
		*/
		public void AddItemListener(IConditionalItem item) {
			switch (item.Trigger) {
				case TriggerType.Fire: OnFire += ((OnFireItemBase)item).Handler; break;
				case TriggerType.Hit: OnHit += ((OnHitItemBase)item).Handler; break;
				case TriggerType.Kill: OnKill += ((OnKillItemBase)item).Handler; break;

				case TriggerType.TakeDamage: OnTakeDamage += ((OnTakeDamageItemBase)item).Handler; break;
				case TriggerType.Heal: OnHeal += ((OnHealItemBase)item).Handler; break;
				case TriggerType.Death: OnDeath += ((OnDeathItemBase)item).Handler; break;

				default: print($"[Character.AddItemListener()] No handler for {item.Trigger}!"); break;
			}
		}

		/*
			Remove a listener for a conditional item from this character's event.
		*/
		public void RemoveItemListener(IConditionalItem item) {
			switch (item.Trigger) {
				case TriggerType.Fire: OnFire -= ((OnFireItemBase)item).Handler; break;
				case TriggerType.Hit: OnHit -= ((OnHitItemBase)item).Handler; break;
				case TriggerType.Kill: OnKill -= ((OnKillItemBase)item).Handler; break;

				case TriggerType.TakeDamage: OnTakeDamage -= ((OnTakeDamageItemBase)item).Handler; break;
				case TriggerType.Heal: OnHeal -= ((OnHealItemBase)item).Handler; break;
				case TriggerType.Death: OnDeath -= ((OnDeathItemBase)item).Handler; break;

				default: print($"[Character.RemoveItemListener()] No handler for {item.Trigger}!"); break;
			}
		}

		/*
			Try to pick up an item.
		*/
		public bool TryPickUpItem(IItem item) {
			if (Inventory.AddItem(item) != -1) {
				//UpdateModifiers();
				print($"[Character.TryPickUpItem()] {item.Name} picked up. ({Inventory.Backpack.Count}/{Inventory.BackpackCap})");
				return true;
			}
			print($"[Character.TryPickUpItem()] {item.Name} NOT picked up. ({Inventory.Backpack.Count}/{Inventory.BackpackCap})");
			return false;
		}

		/*
			Try to drop an item.
			!! Depricated, no use this method.
		*/
		public bool TryDropItem(IItem item) {
			throw new NotImplementedException();
			// if(Inventory.RemoveItem(item)) {
			// 	//UpdateModifiers();
			// 	print($"[Character.TryDropItem()] {item.Name} dropped. ({Inventory.Items.Count}/{Inventory.Cap})");
			// 	return true;
			// }
			// print($"[Character.TryDropItem()] {item.Name} NOT dropped. ({Inventory.Items.Count}/{Inventory.Cap})");
			// return false;
		}
	}

	/*
		Events
	*/
	public partial class Character {

		/*
			Trigers the OnHit event.
		*/
		public void TargetHit(Character target, AttackStats attack) {
			OnHit?.Invoke(this, target, attack);
		}

		/*
			Trigers the OnKill event.
		*/
		public void TargetKilled(Character target, AttackStats attack) {
			OnKill?.Invoke(this, target, attack);
		}


		public void GiveXp(float xp) {
			this.xp += xp;
			OnGainedXP?.Invoke(this, xp);
		}

		/*
			Triggers when character uses an attack.
			Params: actor, attack stats.
		*/
		public event Action<Character, AttackStats> OnFire;

		/*
			Triggers when character hits an enemy.
			Params: actor, target, attack stats.
		*/
		public event Action<Character, Character, AttackStats> OnHit;

		/*
			Triggers when character kills an enemy.
			Params: actor, target, attack stats.
		*/
		public event Action<Character, Character, AttackStats> OnKill;



		/*
			Triggers when character takes damage.
			Params: actor, target, attack stats.
				(Target here is the character that dealt damage to this one)
		*/
		public event Action<Character, Character, AttackStats> OnTakeDamage;

		/*
			Triggers when character heals back health.
			Params: actor, amount.
				(Add heal source later on)
		*/
		public event Action<Character, float> OnHeal;

		/*
			Triggers when character has taken fatal damage. 
				(This is before the character is killed, so can be used to prevent death)
			Params: actor, target, attack stats.
				(Target here is the character that dealt damage to this one)
		*/
		public event Action<Character, Character, AttackStats> OnDeath;



		/*
			Triggers when character picked up an item.
			Params: actor, item.
		*/
		public event Action<Character, IItem> OnPickedUpItem;

		/*
			Triggers when character gains xp.
			Params: sorce, amount.
		*/
		public event Action<Character, float> OnGainedXP;

		/*
			Triggers when character levels up.
			Params: actor.
		*/
		public event Action<Character> OnLevelUp;



		/*
			Triggers when character enters a new room.
			Params: actor.
				(Add room object later)
		*/
		public event Action<Character> OnEnterNewRoom;

		/*
			Triggers when character clears a room.
			Params: actor.
				(Add room object later)
		*/
		public event Action<Character> OnClearedRoom;

		/*
			Triggers when character enters a new level.
			Params: actor.
				(Add level object later)
		*/
		public event Action<Character> OnEnterNewStage;


	}



	/*
		Status effects.
	*/
	public partial class Character {

		/*
			Indicates whether or not the characer is alive.
		*/
		public bool Alive { get; private set; } = true;

		/*
			How long between each time the character can take damage.
		*/
		public TimeSpan IFrameDuration {
			get => _iFrameDuration;
			protected set => _iFrameDuration = value;
		}
		public TimeSpan _iFrameDuration = new TimeSpan(0, 0, 0, 0, 0);



		/*
			Is the character stunned.
		*/
		public bool IsStunned => NextTimeNotStunned > DateTime.Now;

		/*
			Stores the next time the character can isn't stunned.
		*/
		public DateTime NextTimeNotStunned { get; private set; } = DateTime.Now;



		/*
			Is the character visible.
		*/
		public bool IsInvisible => NextTimeVisible > DateTime.Now;

		/*
			Stores the next time the character can recieve damage.
		*/
		public DateTime NextTimeVisible { get; private set; } = DateTime.Now;



		/*
			Is the character invincible.
		*/
		public bool HasIFrames => NextTimeCanTakeDamage > DateTime.Now;

		/*
			Stores the next time the character can recieve damage.
		*/
		public DateTime NextTimeCanTakeDamage { get; private set; } = DateTime.Now;



		/*
			Adds a status effect to the character.
		*/
		public bool AddStatusEffect(StatusEffectType statusType, TimeSpan amount) {
			switch (statusType) {
				case StatusEffectType.Invincible: return AddIFrames(amount);
				case StatusEffectType.Invisible: return AddInvisibility(amount);
				case StatusEffectType.Stun: return AddStun(amount);
				default: return false;
			}
		}



		/*
			Adds Stun for the character.
		*/
		public bool AddStun(TimeSpan amount) {
			// Get when the status effect would expire.
			var wouldExpire = DateTime.Now + IFrameDuration;

			// Only if that's later than current.
			if (wouldExpire > NextTimeNotStunned) {
				// If character currently doesn't have the status effect.
				if (NextTimeNotStunned < DateTime.Now) {
					ApplyStun();
				}

				// Set new time.
				NextTimeNotStunned = wouldExpire;
				return true;
			}
			return false;
		}
		/*
			Appies Stun to the character.
		*/
		private void ApplyStun() {

		}
		/*
			Removes Stun for the character.
		*/
		private void RemoveStun() {

		}



		/*
			Adds invisibility for the character.
		*/
		public bool AddInvisibility(TimeSpan amount) {
			// Get when the status effect would expire.
			var wouldExpire = DateTime.Now + IFrameDuration;

			// Only if that's later than current.
			if (wouldExpire > NextTimeVisible) {
				// If character currently doesn't have the status effect.
				if (NextTimeVisible < DateTime.Now) {
					ApplyInvisibility();
				}

				// Set new time.
				NextTimeVisible = wouldExpire;
				return true;
			}
			return false;
		}
		/*
			Appies invisibility to the character.
		*/
		private void ApplyInvisibility() {

		}
		/*
			Removes invisibility for the character.
		*/
		private void RemoveInvisibility() {

		}



		/*
			Adds IFrames for the character.
		*/
		public bool AddIFrames(TimeSpan amount) {
			// Get when the IFrames would expire.
			var nextCanTakeDamage = DateTime.Now + amount;

			// Only if that's later than current.
			if (nextCanTakeDamage > NextTimeCanTakeDamage) {
				// If character currently doesn't have the status effect.
				if (NextTimeCanTakeDamage < DateTime.Now) {
					ApplyIFrames();
				}

				// Set new time.
				NextTimeCanTakeDamage = nextCanTakeDamage;
				return true;
			}
			return false;
		}
		/*
			Appies IFrames to the character.
		*/
		private void ApplyIFrames() {

		}
		/*
			Removes IFrames for the character.
		*/
		private void RemoveIFrames() {

		}

	}
}
