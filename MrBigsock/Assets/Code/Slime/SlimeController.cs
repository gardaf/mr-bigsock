using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;


namespace BigSock {

	public partial class SlimeController : EnemyController {
		//public float collisionOffset = 0.05f;
		//public ContactFilter2D movementFilter;
		//List<RaycastHit2D> castCollisions = new List<RaycastHit2D>();

		//private Transform target;
		//Animator m_Animator;
		//private float distance; 

		//private float canAttack;

		//private EmptyCollider followCollider;
		//private EmptyCollider attackCollider;

		//private bool isInMelee = false;


		//Rigidbody2D rb;

		// private Collider2D_Proxy secondCollider;

		/*
			The state the slime is in.
		*/
		public SlimeState State { get; protected set; } = SlimeState.Idle;


		/*
			The location of the target when charging started.
		*/
		public Vector3 TargetLocation { get; protected set; }

		/*
			The next time the slime can change its state.
		*/
		public DateTime NextTimeStateCanChange { get; private set; } = DateTime.Now;

		/*
			Minimum time the slime should idle before it can charge.
		*/
		protected static readonly TimeSpan IDLE_WAIT_TIME = new TimeSpan(0, 0, 0, 0, 5);
		/*
			Minimum time the slime should charge before it can leap.
		*/
		protected static readonly TimeSpan CHARGE_WAIT_TIME = new TimeSpan(0, 0, 0, 1, 0);
		/*
			Maximum time the slime should leap before it can idle.
		*/
		protected static readonly TimeSpan LEAP_WAIT_TIME = new TimeSpan(0, 0, 0, 2, 0);

		/*
			The force the slime leaps at.
		*/
		public double LeapForce => MovementSpeed * 10;


		//void Start() {
		//	rb = GetComponent<Rigidbody2D>();
		// m_Animator = gameObject.GetComponent<Animator>();
		//	followCollider = transform.Find("followCollider").GetComponent<EmptyCollider>();
		//	followCollider.OnColliderEnter2D_Action += Move_OnColliderEnter2D;
		//	followCollider.OnColliderStay2D_Action += Move_OnColliderStay2D;
		//	followCollider.OnColliderExit2D_Action += Move_OnColliderExit2D;
		//	attackCollider = transform.Find("MeleeCollider").GetComponent<EmptyCollider>();
		//	attackCollider.OnColliderEnter2D_Action += Attack_OnColliderEnter2D;
		//	attackCollider.OnColliderStay2D_Action += Attack_OnColliderStay2D;
		//	attackCollider.OnColliderExit2D_Action += Attack_OnColliderExit2D;
		//}





		protected override void Update() {
			if (State == SlimeState.Idle) {
				// If it has a target and has idled long enough.
				if (target != null && DateTime.Now >= NextTimeStateCanChange) {
					// Store target location.
					TargetLocation = target.position;

					// Update the state.
					State = SlimeState.Charging;
					NextTimeStateCanChange = DateTime.Now + CHARGE_WAIT_TIME;
					m_Animator.SetTrigger("idle");
				}
			} else if (State == SlimeState.Charging) {
				// If it has charged long enough.
				if (DateTime.Now >= NextTimeStateCanChange) {
					// Set momentum
					var pos = TargetLocation;//target.position;
					var temp = (pos - rb.transform.position);
					temp.z = 0;
					var direction = temp.normalized;
					//rb.AddForce((float) LeapForce * direction, ForceMode2D.Impulse);
					TryMove(direction);

					// Update the state.
					State = SlimeState.Leaping;
					NextTimeStateCanChange = DateTime.Now + LEAP_WAIT_TIME;
				}
			} else if (State == SlimeState.Leaping) {
				// If it has charged long enough.
				if (DateTime.Now >= NextTimeStateCanChange || rb.velocity == Vector2.zero) {
					// Update the state.
					State = SlimeState.Idle;
					NextTimeStateCanChange = DateTime.Now + IDLE_WAIT_TIME;
					m_Animator.SetTrigger("walk");

				}
			}

		}


		/*
			Try to move in a given direction.
		*/
		protected override bool TryMove(Vector2 direction) {
			if (direction != Vector2.zero) {
				rb.AddForce((float)LeapForce * direction, ForceMode2D.Impulse);
				return true;
			}
			return false;
		}

	}


	/*
		Attack
	*/
	public partial class SlimeController {

		//private void Attack_OnColliderEnter2D(Collider2D other) {
		//	if (other.gameObject.tag == "Player")
		//		isInMelee = true;
		//}


		//private void Attack_OnColliderStay2D(Collider2D other) {
		//	var player = other.gameObject.GetComponent<PlayerController>();
		//	if (player != null) {
		//		// Create attack object.
		//		var attack = new AttackStats{
		//			Damage = Damage,
		//			Knockback = KnockbackForce,
		//			Range = 0,
		//			AttackSpeed = AttackSpeed,
		//			Source = transform.position,
		//		};
		//		// Get the player to take the damage.
		//		if(player.TakeDamage(attack)) {
		//				//knockback ? 
		//				//animer nå ?
		//				
		//		}
		//	} 
		//}

		//private void Attack_OnColliderExit2D(Collider2D other){
		//	if (other.gameObject.tag == "Player")
		//		isInMelee = false;
		//}

	}

	/*
		Movement
	*/
	public partial class SlimeController {

		protected override void Move_OnColliderEnter2D(Collider2D other) {
			if (other.gameObject.tag == "Player") {
				//m_Animator.SetTrigger("walk");
				target = other.transform;
			}
		}


		protected override void Move_OnColliderExit2D(Collider2D other) {
			if (other.gameObject.tag == "Player") {
				//m_Animator.SetTrigger("idle");
				target = null;
			}
		}

	}

	/*
		The different states the slime can be in.
	*/
	public enum SlimeState {
		Idle, Charging, Leaping
	}
}
