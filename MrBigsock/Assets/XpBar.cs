using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace BigSock.UI{

    public class XpBar : MonoBehaviour
{

    public Slider slider;
    public TextMeshPro xpText;

    protected void Start(){
        slider = GetComponent<Slider>();
       // xpText = GetComponent<TextMeshPro>();
    }
    
    public void SetXp(float xp){
        slider.value = xp;
        xpText.SetText($"{xp:N0}/{slider.maxValue:N0}");
    }

    public void SetMaxXp(float xp){
        slider.maxValue = xp;
    }
}
}
